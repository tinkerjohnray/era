'use strict';

(async function init(){

    const
        express = require('express'),
        app = express(),
        config = require('./config/database'),
        http = require('http').Server(app),
        api = require('./@api'),
        mongoose = require('mongoose'),
        lib = require('./library')['i'](),
        port = 3000;
   
        try{

            (async function initServers(){

                await lib.options(express, app)();
            
                await http.listen(port, () => {
                    console.log("Express server listening on port %d", port);
                }).setMaxListeners(0);
                
                await mongoose.connect(config.database, config.options, () => {
                    switch(mongoose.connection.readyState){
                        case 0:
                            console.log("Mongoose not connected", config.database);
                            process.exit(0); break;
                        case 1:
                            console.log("Mongoose connected %s", config.database); break;
                    }
                });
                
                await app.use(lib.logs().start());
                
                await app.use('/api', await api.start());

                await lib.socket(http, port).start();

            })();

        }catch(e){
            console.log("Server Error %s", e);
        }

})();