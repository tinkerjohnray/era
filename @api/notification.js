'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _get('getNotification', async (req, res) => {
            await model.getNotification(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllNotifications', async (req, res) => {
            await model.getAllNotifications(req.body, response => {
                res.json(response);
            });
        });

        _get('getNotificationLength', async (req, res) => {
            await model.getNotificationLength(req.body, response => {
                res.json(response);
            });
        });

        _put('updateNotificationRead', async (req, res) => {
            await model.updateNotificationRead(req.body, response => {
                res.json(response);
            });
        });
        
        _put('updateNotificationDelete', async (req, res) => {
            await model.updateNotificationDelete(req.body, response => {
                res.json(response);
            });
        });

        // _post('newNotification', (req, res) => {
        //     res.json({ success:true, message:'test post' }) 
        // });   

    module.exports = lib.ROUTES(base);
    
 })();