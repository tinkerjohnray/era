'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _post('createForm', async (req, res) => {
            await model.createForm(req.body, response => {
                res.json(response)
            });
        });   


        _get('getApplicants', async (req, res) => {
            await model.getApplicants((err, applicants) => {
                if (err) {
                    res.json({success: false, message:'Failed'})
                } else {
                    res.json({success: true, message: 'success', data: applicants})
                }
            })
        });
        
        // _post('createBrand', async (req, res) => {
        //     await model.createBrand(req.body, response => {
        //         res.json(response)
        //     });
        // });   




        _put('deleteApplicants', async (req, res) => { 
            await model.deleteApplicants(req.body, response => {
                res.json(response)
            });
        }); 

        _put('editApplicants', async (req, res) => { 
            await model.editApplicants(req.body, response => {
                res.json(response)
            });
        }); 

        // _get('getMobile', async (req, res) => {
        //     await model.getMobile((err, mobile) => {
        //         if (err) {
        //             res.json({success: false, message:'Failed'})
        //         } else {
        //             res.json({success: true, message: 'success', data: mobile})
        //         }
        //     })
        // });



    module.exports = lib.ROUTES(base);
    
 })();