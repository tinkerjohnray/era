'use strict';

(async function init() {

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET, _post = lib.POST, _put = lib.PUT;

    let filesystem = require('fs');

    _get('test', (req, res) => {
        res.json({ success: true, message: 'test get' })
    }, {
            private: false
        });

    _get('getSGSGallery', async (req, res) => {
        await model.getSGSGallery(req.body, response => {
            res.json(response);
        });
    });

    _get('getSGSGalleryFileTypes', async (req, res) => {
        await model.getSGSGalleryFileTypes(req.body, response => {
            res.json(response);
        });
    });

    _post('test', (req, res) => {
        res.json({ success: true, message: 'test post' })
    });

    _post('galleryImages', async (req, res) => {
        req.body.useFor = 'image-gallery';
        req.body.uploadPath = 'image-gallery';
        await model.addFile(req, response => {
            res.json(response);
        });
    });

    _post('productImage', async (req, res) => {
        req.body.useFor = 'product-image';
        await model.addFile(req, response => {
            res.json(response);
        });
    });

    _post('receiptImage', async (req, res) => {
        req.body.useFor = 'receipt-image';
        await model.addReceiptFile(req, response => {
            res.json(response);
        });
    });

    _post('avatars', async (req, res) => {
        req.body.useFor = 'avatar-image';
        await model.uploadAvatar(req, response => {
            res.json(response);
        });
    });

    _post('deleteReciept', async (req, res) => {
        await model.deleteReciept(req, response => {
            res.json(response);
        });
    });

    _post('hmedUploadImage', async (req, res) => {
        await model.hmed_ImageUpload(req, response => {
            res.json(response);
        });
    });

    _post('uploadBrochure', async (req, res) => {
        req.body.useFor = 'brochure-image';
        await model.uploadBrochure(req, response => {
            res.json(response);
        });
    });

    _post('medrepAvatar', async (req, res) => {
        req.body.useFor = 'medrep-avatar-image';
        await model.medrepAvatar(req, response => {
            res.json(response);
        });
    });

    _get('uploadedImageList', (req, res) => {
        let imagesDir = './uploads/images_hmed_attendance';

        // check directory for HMED images
        if (filesystem.existsSync(imagesDir) == false) {

            console.warn("HMED Attendance directory for images doesn't exist! Creating one...");
            filesystem.mkdirSync(imagesDir);
        }

        //list all files into an array
        filesystem.readdir(imagesDir, (err, images) => {
            res.json({ fileList: images });
        })
    });

    _put('updateImage', async (req, res) => {
        await model.updateImage(req.body, response => {
            res.json(response);
        });
    });

    _put('test', (req, res) => {
        res.json({ success: true, message: 'test put' })
    });

    _post('signatureUpload', async (req, res) => {
        await model.signatureUpload(req, response => {
            res.json(response);
        });
    });

    
    module.exports = lib.ROUTES(base);

})();