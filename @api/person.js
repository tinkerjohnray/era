'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        // GET
        _get('getAllPeople', async (req, res) => {
            await model.getAllPeople('empty', response => {
                res.json(response)
            });
        });

        _get('getAllPeopleNameId', async (req, res) => {
            await model.getAllPeopleNameId(req.body, response => {
                res.json(response)
            });
        });
        
        _get('getOnePersonChildren', async (req, res) => {
            await model.getOnePersonChildren(req.body, response => {
                res.json(response)
            });
        });
        
        //POST
        _post('createperson', async (req, res) => { 
            await model.createperson(req.body, response => {
                res.json(response)
            });
        });


        //PUT
        _put('updatePerson', async (req, res) => {
            await model.updatePerson(req.body, response => {
                res.json(response)
            });
        });
        
        _put('updatePersonStatus', async (req, res) => {
            await model.updatePersonStatus(req.body, response => {
                res.json(response)
            });
        });
        
        _put('addChild', async (req, res) => {
            await model.addChild(req.body, response => {
                res.json(response)
            });
        });
        
        _put('UpdateChild', async (req, res) => {
            await model.UpdateChild(req.body, response => {
                res.json(response)
            });
        });

    module.exports = lib.ROUTES(base);

 })();