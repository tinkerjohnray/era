'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _post('addMobile', async (req, res) => {
            await model.addMobile(req.body, response => {
                res.json(response)
            });
        });   

        
        _post('createBrand', async (req, res) => {
            await model.createBrand(req.body, response => {
                res.json(response)
            });
        });   




        _put('deleteBtn', async (req, res) => { 
            await model.deleteBtn(req.body, response => {
                res.json(response)
            });
        }); 

        _put('edited', async (req, res) => { 
            console.log(req.body)
            await model.edited(req.body, response => {
                res.json(response)
            });
        }); 

        _get('getMobile', async (req, res) => {
            await model.getMobile((err, mobile) => {
                if (err) {
                    res.json({success: false, message:'Failed'})
                } else {
                    res.json({success: true, message: 'success', data: mobile})
                }
            })
        });



    module.exports = lib.ROUTES(base);
    
 })();