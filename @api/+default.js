'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _get('test', (req, res) => {
            res.json({ success:true, message:'test get' }) 
        });

        _post('test', (req, res) => {
            res.json({ success:true, message:'test post' }) 
        });   

        _put('test', (req, res) => {
            res.json({ success:true, message:'test put' }) 
        });

    module.exports = lib.ROUTES(base);
    
 })();