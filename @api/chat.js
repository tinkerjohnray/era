'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _get('checkThreadIfEsist', async (req, res) => {
            await model.checkThreadIfEsist(req.body, response => {
                res.json(response);
            });
        });

        //get first 10 latest convo
        _get('getMessages', async (req, res) => {
            await model.getMessages(req.body, response => {
                res.json(response);
            });
        });

        _get('getGroupMessages', async (req, res) => {
            await model.getGroupMessages(req.body, response => {
                res.json(response);
            });
        });

        _get('getGroupInfo', async (req, res) => {
            await model.getGroupInfo(req.body, response => {
                res.json(response);
            });
        });

        _get('getGroupThread', async (req, res) => {
            await model.getGroupThread(req.body, response => {
                res.json(response);
            });
        });

        _get('getThreadMembers', async (req, res) => {
            await model.getThreadMembers(req.body, response => {
                res.json(response);
            });
        });

        _post('createMessageThread', async (req, res) => {
            await model.createMessageThread(req.body, response => {
                res.json(response);
            });
        });        

        _post('sendMessageUpdate', async (req, res) => {
            await model.sendMessageUpdate(req.body, response => {
                res.json(response);
            });
        });

        _post('createMessageGroupThread', async (req, res) => {
            await model.createMessageGroupThread(req.body, response => {
                res.json(response);
            });
        });

        _post('updateGroupMembers', async (req, res) => {
            await model.updateGroupMembers(req.body, response => {
                res.json(response);
            });
        });

        _get('countUnreadGroupMessages', async (req, res) => {
            await model.countUnreadGroupMessages(req.body, response => {
                res.json(response);
            });
        });

        _get('countUnreadMessages', async (req, res) => {
            await model.countUnreadMessages(req.body, response => {
                res.json(response);
            });
        });

        _post('updateLastSeen', async (req, res) => {
            await model.updateLastSeen(req.body, response=> {
                res.json(response);
            });
        });


        

    module.exports = lib.ROUTES(base);

 })();
