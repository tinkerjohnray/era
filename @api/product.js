'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        // GET

        _get('getAllProducts', async (req, res) => {
            await model.getAllProducts('empty', response => {
                res.json(response)
            });
        });
        
        _get('getAllProductsSO', async (req, res) => {
            await model.getAllProductsSO('empty', response => {
                res.json(response)
            });
        });
        
        _get('getAllProductsWithPrice', async (req, res) => {
            await model.getAllProductsWithPrice('empty', response => {
                res.json(response)
            });
        });
        
        _get('getProductById', async (req, res) => {
            await model.getProductById(req.body, response => {
                res.json(response)
            });
        });
        
        _get('getAllInventory', async (req, res) => {
            await model.getAllInventory('empty', response => {
                res.json(response)
            });
        }, { allow: ['admin', 'inventory'] });
        
        _get('getMedrepAllInventory', async (req, res) => {
            await model.getMedrepAllInventory('empty', response => {
                res.json(response)
            });
        }, { allow: ['medrep'] });

        //POST

        _post('addProduct', async (req, res) => {
            await model.addProduct(req.body, response => {
                res.json(response)
            });
        });

        //PUT
        
        _put('disableProduct', async (req, res) => {
            await model.disableProduct(req.body, response => {
                res.json(response)
            });
        }, { allow: ['admin', 'inventory'] });
        
        _put('updateProductById', async (req, res) => {
            await model.updateProductById(req.body, response => {
                res.json(response)
            });
        }, { allow: ['admin', 'inventory'] });

    module.exports = lib.ROUTES(base);

 })();