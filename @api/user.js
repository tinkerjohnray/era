'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _get('getServerDate', async (req, res) => {
            res.json({ success:true, data: new Date})
        });

        // lib.createSocket((client, io) => {
        //     return client.on('localnotif', (data) => {
        //         console.log('emit receive');
        //         io.emit('localnotif', { success: true, data: data })
        //     });
        // });

        // _get('test', async(req, res) => {
        //     lib.createEmit('localnotif', { data:1, uid:8});
        //     res.json({ yeah:1 });
        // }, { private:false });

        _get('getAgentCustomers', async (req, res) => {
            await model.getAgentCustomers(req.body, response => {
                res.json(response)
            });
        });

        _get('getAllAgentCustomers', async (req, res) => {
            await model.getAllAgentCustomers(req.body, response => {
                res.json(response)
            });
        });

        _get('getAllAgentBrochure', async (req, res) => {
            await model.getAllAgentBrochure(req.body, response => {
                res.json(response);
             });
        });

        _put('updateAgentBrochures', async (req, res) => {
            await model.updateAgentBrochures(req.body, response => {
                res.json(response);
             });
        });
        
        _get('allUsers', async (req, res) => {
            await model.allUsers(req.body, response => {
                res.json(response)
            });
        });

        _get('viewScreenshot', async (req, res) => {
            await model.viewScreenshot(req.body, response => {
                res.json(response)
            });
        });

        _get('viewSignature', async (req, res) => {
            await model.viewSignature(req.body, response => {
                res.json(response)
            });
        });

        _get('getAllAgentProducts_mobile', async (req, res) => {
            await model.getAllAgentProducts_mobile(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllActivitiesSuper', async (req, res) => {
            await model.getAllActivitiesSuper(req.body, response => {
                res.json(response);
            });
        });

        _post('createUser', async (req, res) => {
            await model.createUser(req.body, response => {
                res.json(response)
            });
        });

        _post('authenticate', async (req, res) => {
            await model.authenticate(req, response => {
                res.json(response)
            });
        }, { private:false });

        // _post('mobileAuthenticate', async (req, res) => {
        //     await model.mobileAuthenticate(req, response => {
        //         res.json(response)
        //     });
        // }, { private:false });

        _get('getAllUsers', (req, res) => {
            model.getAllUsers(response => {
                res.json(response);
            });
        });

        _get('getAllAgents', (req, res) => {
            model.getAllAgents(req.body, response => {
                res.json(response);
            });
        });

        _get('getUser', (req, res) => {
            model.getUser(req.body, response => {
                res.json(response)
            });
        });

        _get('getUserById', (req, res) => {
            model.getUserById(req.body, response => {
                res.json(response);
            });
        });

        _get('getUserById_mobile', (req, res) => {
            model.getUserById_mobile(req.body, response => {
                res.json(response)
            });
        });

        _get('getAllActivitiesCustomWeek', (req, res) => {
            model.getAllActivitiesCustomWeek(req.body, response => {
                res.json(response)
            });
        });

        _post('updateUser', (req, res) => {
            model.updateUser(req.body, response => {
                res.json(response);
            });
        });

        _post('deactivateUser', (req, res) => {
            model.deactivateUser(req.body, response => {
                res.json(response);
            });
        });

        _post('enableUser', (req, res) => {
            model.enableUser(req.body, response => {
                res.json(response);
            });
        });

        _post('updateUserEmployee', (req, res) => {
            model.updateUserEmployee(req.body, response => {
                res.json(response);
            });
        });

        _get('getSecuredUser', (req, res) => {
            model.getSecuredUser(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllSecuredUser', (req, res) => {
            model.getAllSecuredUser(req.body, response => {
                res.json(response);
            });
        });

        _get('getChatableUser', (req, res) => {
            model.getChatableUser(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllChatableUser', (req, res) => {
            model.getAllChatableUser(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllEmployee', (req, res) => {
            model.getAllEmployee(req.body, response => {
                res.json(response);
            });
        });

        
        _get('getActivities', (req, res) => {
            model.getActivities(req.body, response => {
                res.json(response);
            });
        });

        _get('getActivity', (req, res) => {
            model.getActivity(req.body, response => {
                res.json(response);
            });
        });

        _post('createItinerary', (req, res) => {
            model.createItinerary(req.body, response => {
                res.json(response);
            });
        });

        _post('SignatureUpload', (req, res) => {
            model.SignatureUpload(req.body, response => {
                res.json(response);
            });
        });

        _get('getItineraries', (req, res) => {
            model.getItineraries(req.body, response => {
                res.json(response);
            });
        });

        _post('updateSignature', (req, res) => {
            model.updateSignature(req.body, response => {
                res.json(response);
            });
        });

        _get('getCurrentItinerary', (req, res) => {
            model.getCurrentItinerary(req.body, response => {
                res.json(response);
            });
        });

        _post('updateItinerary', (req, res) => {
            model.updateItinerary(req.body, response => {
                res.json(response);
            });
        });


        
        _post('addActivity', (req, res) => {
            model.addActivity(req.body, response => {
                res.json(response);
            });
        });

        
        _get('getActivities', (req, res) => {
            model.getActivities(req.body, response => {
                res.json(response);
            });
        });

        _get('getActivity', (req, res) => {
            model.getActivity(req.body, response => {
                res.json(response);
            });
        });

        _get('getMonthActivities', async (req, res) => {
            await model.getMonthActivities(req.body, response => {
                res.json(response);
            });
        });

        _get('getDayActivities', async (req, res) => {
            await model.getDayActivities(req.body, response => {
                res.json(response);
            });
        });
       
        _post('createItinerary', (req, res) => {
            model.createItinerary(req.body, response => {
                res.json(response);
            });
        });

        _post('SignatureUpload', (req, res) => {
            model.SignatureUpload(req.body, response => {
                res.json(response);
            });
        });

        _get('getItineraries', (req, res) => {
            model.getItineraries(req.body, response => {
                res.json(response);
            });
        });

        _post('updateSignature', (req, res) => {
            model.updateSignature(req.body, response => {
                res.json(response);
            });
        });

        _post('updateScreenshot', (req, res) => {
            model.updateScreenshot(req.body, response => {
                res.json(response);
            });
        });

        _get('getCurrentItinerary', (req, res) => {
            model.getCurrentItinerary(req.body, response => {
                res.json(response);
            });
        });

        _post('updateItinerary', (req, res) => {
            model.updateItinerary(req.body, response => {
                res.json(response);
            });
        });

        _put('changeStatus', (req, res) => {
            model.changeStatus(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllUsersWithProfile', (req, res) => {
            model.getAllUsersWithProfile(req.body, response => {
                res.json(response);
            });
        });

        _put('updateAgentCustomers', (req, res) => {
            model.updateAgentCustomers(req.body, response => {
                res.json(response);
            });
        });

        _get('getAllPayrolledUsers', (req, res) => {
            model.getAllPayrolledUsers(req.body, response => {
                res.json(response);
            });
        });
        
    
    module.exports = lib.ROUTES(base);

 })();