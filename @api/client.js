'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _post('createClient', async (req, res) => {
            await model.createClient(req.body, response => {
                res.json(response)
            });
        });   

        
        // _post('createBrand', async (req, res) => {
        //     await model.createBrand(req.body, response => {
        //         res.json(response)
        //     });
        // });   




        _put('deleteClient', async (req, res) => { 
            await model.deleteClient(req.body, response => {
                res.json(response)
            });
        }); 

        // _put('edited', async (req, res) => { 
        //     await model.edited(req.body, response => {
        //         res.json(response)
        //     });
        // }); 

        _get('getClient', async (req, res) => {
            await model.getClient((err, client) => {
                if (err) {
                    res.json({success: false, message:'Failed'})
                } else {
                    res.json({success: true, message: 'success', data: client})
                }
            })
        });



    module.exports = lib.ROUTES(base);
    
 })();