'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _post('tasks', async (req, res) => {
            await model.tasks(req.body, response => {
                res.json(response)
            });
        });   

        
        // _post('createBrand', async (req, res) => {
        //     await model.createBrand(req.body, response => {
        //         res.json(response)
        //     });
        // });   




        _put('deleteTask', async (req, res) => { 
            await model.deleteTask(req.body, response => {
                res.json(response)
            });
        }); 

        _put('editedTask', async (req, res) => { 
            await model.editedTask(req.body, response => {
                res.json(response)
            });
        }); 

        _get('getTasks', async (req, res) => {
            await model.getTasks((err, task) => {
                if (err) {
                    res.json({success: false, message:'Failed'})
                } else {
                    res.json({success: true, message: 'success', data: task})
                }
            })
        });



    module.exports = lib.ROUTES(base);
    
 })();