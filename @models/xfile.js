'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    lib = require('../library'),

    Xfile = module.exports = mongoose.model(base, mongoose.Schema({
        id:                     { type:Number, maxlength:7, required:true },
        source:                 { type:String, maxlength:50, required:true },
        for:                    { type:String, maxlength:50, required:true },
        extension:              { type:String, maxlength:50, required:true },
        name:                   { type:String, maxlength:100, required:false },
        category:               { type:String, maxlength:50, required:false },
        tags:[{
            name:               { type:String, maxlength:50, required:false }
        }],

        date_added:             { type: Date, required: true, required: true },
        status:                 { type: Number, maxlength: 1, required: true, default: 1 },
        added_by:               { type: Number, maxlength: 7, required: true },
        lastUpdateBy:           { type: Number, maxlength: 7, required: true },
        lastUpdateDate:         { type: Date, required: true },
    }, { strict: true })),

    fn = lib.invocation().init(module.exports).fn;
    

        fn('getSGSGallery', (data, cb) => {
            
            data.type = data.type !== undefined && data.type.length ? data.type.map(e => e.extension ) : [];

            Xfile.aggregate([
                {
                    $match: {
                        $and: [
                            { status: 1 },
                            { name: { $regex: data.query || '' , $options: 'i' } },
                            { extension:  data.type.length ? { $in: data.type } : { $regex: '' , $options: 'i' } },
                        ]
                    }
                },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "added_by",
                        foreignField: "id",
                        as: "addedBy"
                    }
                },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "lastUpdateBy",
                        foreignField: "id",
                        as: "lastUpdateBy"
                    }
                },
                { $unwind: "$addedBy" },
                { $unwind: "$lastUpdateBy" },
                {
                    $group: {
                        _id: 1,
                        images: {
                            $addToSet: {
                                id: "$id",
                                source: "$source",
                                name: "$name",
                                extension: "$extension",
                                
                                date_added: "$date_added",
                                status: "$status",
                                added_by: { $concat: [
                                    { $ifNull: [ "$addedBy.lname", "" ] }, ", ",
                                    { $ifNull: [ "$addedBy.fname", "" ] }, " ",
                                    { $ifNull: [ "$addedBy.mname", "" ] } ]
                                },
                                lastUpdateDate: "$lastUpdateDate",
                                lastUpdateBy: { $concat: [
                                    { $ifNull: [ "$lastUpdateBy.lname", "" ] }, ", ",
                                    { $ifNull: [ "$lastUpdateBy.fname", "" ] }, " ",
                                    { $ifNull: [ "$lastUpdateBy.mname", "" ] } ]
                                },
                            }
                        },
                        count: { $sum: 1 }
                    }
                },
                { $unwind: "$images" },
                { $sort: { "images.id": -1 } },
                { $skip: data.skip || 0 },
                { $limit: data.limit || 2 },
                {
                    $group: {
                        _id: 1,
                        images: { $push: "$images" },
                        count: { $first: "$count" }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        images: "$images",
                        total: "$count",
                    }
                },
            ], (err, getSGSGallery)=>{
                // console.log(getSGSGallery);
                if( err ) return cb({ sucess:false, message: err.message });
                if(getSGSGallery.length){
                    return cb({ success:true, data:getSGSGallery[0] });
                }else{
                    return cb({ success:false, message: "No images found!" });
                }
            });
        });

        fn('getSGSGalleryFileTypes', (data, cb) => {
            Xfile.aggregate([
                { $match: { status: 1 } },
                {
                    $group: {
                        _id: "$extension",
                        extension: { $first: "$extension" },
                    }
                },
                {
                    $project: {
                        _id: [],
                        extension: "$extension",
                        type: {
                            $switch: {
                                branches: [
                                    { case: { $eq: [ "$extension", ".jpg" ] }, then: "Image" },
                                    { case: { $eq: [ "$extension", ".jpeg" ] }, then: "Image" },
                                    { case: { $eq: [ "$extension", ".png" ] }, then: "Image" },
                                    { case: { $eq: [ "$extension", ".gif" ] }, then: "Image" },
                                    { case: { $eq: [ "$extension", ".pdf" ] }, then: "Document" },
                                    { case: { $eq: [ "$extension", ".doc" ] }, then: "Document" },
                                    { case: { $eq: [ "$extension", ".txt" ] }, then: "Document" },
                                    { case: { $eq: [ "$extension", ".docx" ] }, then: "Document" },
                                    { case: { $eq: [ "$extension", ".mp4" ] }, then: "Video" },
                                    { case: { $eq: [ "$extension", ".mp3" ] }, then: "Audio" },
                                    { case: { $eq: [ "$extension", ".tar" ] }, then: "Compressed File" },
                                    { case: { $eq: [ "$extension", ".gz" ] }, then: "Compressed File" },
                                    { case: { $eq: [ "$extension", ".zip" ] }, then: "Compressed File" },
                                    { case: { $eq: [ "$extension", ".7z" ] }, then: "Compressed File" },
                                    { case: { $eq: [ "$extension", ".rar" ] }, then: "Compressed File" },
                                ],
                            }
                        },
                        __img__: {
                            $switch: {
                                branches: [
                                    { case: { $eq: [ "$extension", ".jpg" ] }, then: "../assets/images/jpg.png" },
                                    { case: { $eq: [ "$extension", ".jpeg" ] }, then: "../assets/images/jpg.png" },
                                    { case: { $eq: [ "$extension", ".png" ] }, then: "../assets/images/png.png" },
                                    { case: { $eq: [ "$extension", ".gif" ] }, then: "../assets/images/gif.png" },
                                    { case: { $eq: [ "$extension", ".pdf" ] }, then: "../assets/images/pdf.png" },
                                    { case: { $eq: [ "$extension", ".doc" ] }, then: "../assets/images/doc.png" },
                                    { case: { $eq: [ "$extension", ".docx" ] }, then: "../assets/images/doc.png" },
                                    { case: { $eq: [ "$extension", ".txt" ] }, then: "../assets/images/doc.png" },
                                    { case: { $eq: [ "$extension", ".mp4" ] }, then: "../assets/images/mp4.png" },
                                    { case: { $eq: [ "$extension", ".mp3" ] }, then: "../assets/images/mp3.png" },
                                    { case: { $eq: [ "$extension", ".tar" ] }, then: "../assets/images/file.png" },
                                    { case: { $eq: [ "$extension", ".gz" ] }, then: "../assets/images/file.png" },
                                    { case: { $eq: [ "$extension", ".zip" ] }, then: "../assets/images/file.png" },
                                    { case: { $eq: [ "$extension", ".7z" ] }, then: "../assets/images/file.png" },
                                    { case: { $eq: [ "$extension", ".rar" ] }, then: "../assets/images/file.png" },
                                ],
                            }
                        }
                    }
                }
            ], (err, getSGSGalleryFileTypes)=>{
                // console.log(getSGSGalleryFileTypes);
                if( err ) return cb({ sucess:false, message: err.message });
                if(getSGSGalleryFileTypes.length){
                    return cb({ success:true, data:getSGSGalleryFileTypes });
                }else{
                    return cb({ success:false, message: "No file type found!" });
                }
            });
        });

        fn('addFile', (data, cb) => {

            let useFor = data.body.useFor;
            let username = data.body.auth.username;
            let formidable = require('formidable');
            let fs = require('fs');
            let path = require('path');
            let md5 = require('md5');

            var form = new formidable.IncomingForm();
             form.uploadDir = `${__dirname}/../uploads/${data.body.uploadPath}/`;

             //auto image creator
             if(!fs.existsSync(path.join(form.uploadDir))){
                fs.mkdirSync(path.join(form.uploadDir));
             }

             form.on('file', async (field, file) => {
                 
                 let newFileName = [
                    username,
                     Math.random(),
                     Math.random(),
                     Math.random(),
                     new Date,
                ];

                    let rawFileName = file.name.split('.')[0];
                    let extension = file.name.split('.').pop();
                    let validFiles = [".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc",  ".txt", ".docx", ".mp4", ".mp3", ".tar", ".gz", ".zip", ".7z", ".rar"];
                    
                    newFileName = `${md5(newFileName.join(''))}.${extension}`;

                    if( validFiles.includes(`.${extension}`) ){
                        if (fs.existsSync(file.path)) {
                            fs.rename(file.path, path.join(form.uploadDir, newFileName), (err) => {
                                if (err) {
                                    return cb({ success:false, message:err.name + " " + err.message }) 
                                }else{
    
                                    let cdata = {
                                        source: newFileName,
                                        for: useFor,
                                        extension: `.${extension}`,
                                        name: rawFileName,
    
                                        date_added: new Date,
                                        status: 1,
                                        added_by: data.body.auth.id,
                                        lastUpdateBy: data.body.auth.id,
                                        lastUpdateDate: new Date,
                                    };
    
                                    lib.create(cdata, (response) => {
                                        if(response.success){
                                            lib.createEmit('xfile', cdata.id);
                                            return cb({ success:true, message:'Image uploaded', data: { name:newFileName, rawFileName:rawFileName, id:cdata.id } }) 
                                        }else{
                                            return cb(response);
                                        }
                                    }, base, 1);
                                }
                            });
                        }else{
                            return cb({ success:false, message: `Something went wrong please re-upload your image. ${err.message}` }) 
                        }
                    }else{
                        return cb({ success:false, message: `File of this type/format cannot be uploaded` }) 
                    }

                    
             });

              form.on('error', function(err) {
                // console.log('An error has occured: \n' + err);
                
                return cb({ success:false, message: `Something went wrong please re-upload your image. ${err.message}` }) 
              });
              form.on('end', function() {
                //   console.log('hey');
              });
              form.parse(data);

        });

        fn('updateImage', (data, cb) => {
            // console.log(data);
            Xfile.updateOne(
                { id: data.id },
                {
                    $set: {
                        name: data.name
                    }
                }, (err, updateImage) => {
                    if (err) return cb({ success: false, message: err.message });
                    if (updateImage.nModified || updateImage.ok) {
                        lib.createEmit('xfile', data.id);
                        return cb({ success: true, message: "Image successfully added." });
                    } else {
                        return cb({ success: false, message: "Unable to update image" })
                    }
                }
            )
        });
        
        fn('deleteFile', (data, cb) => {
            Xfile.deleteOne({ id:data.id }, (err) => {
                if (err){
                    return cb({ success:false, message: err.message });
                }else{
                    return cb({ success:true, message: "File successfully deleted." });
                }
            });
        });


    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

    /**
     * @param {Object} data object and values to check
     * @return {Object} User object data
     */
    
    lib.createSocket( (client, io) => {
        return client.on('xfile', (data) => {
            console.log(`'xfile' emit received at`, __filename, `with data "${data}"`);
            io.emit('xfile', { success:true, data:data })
        });
    });

    lib.createSocket( (client, io) => {
        return client.on('medrepavatar', (data) => {
            console.log(`'medrepavatar' emit received at`, __filename, `with data "${data}"`);
            io.emit('medrepavatar', { success:true, data:data })
        });
    });

})();