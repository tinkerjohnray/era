'use strict';

(async function init() {

    const
        base = require('path').basename(__filename).split('.')[0],
        mongoose = require('mongoose'),
        lib = require('../library'),
        Id = require('./id'),

        User = module.exports = mongoose.model(base, mongoose.Schema({
            id:         { type: Number, maxlength: 7, required: true },
            role:       { type: String, maxlength: 10, required: true },
            fname:      { type: String, maxlength: 40 },
            mname:      { type: String, maxlength: 40 },
            lname:      { type: String, maxlength: 40 },
            username:   { type: String, maxlength: 14 },
            password:   { type: String, maxlength: 100 },
            email:      { type: String, maxlength: 100 },
            avatar:     { type: String },
            session_id: { type: String, maxlength: 100, required: true },
            security: {
                lastlogin: {
                    list: [{
                        ua:         { type: String, maxlength: 100 },
                        ip:         { type: String, maxlength: 20 },
                        location:   { type: String, maxlength: 100 },
                        date:       { type: Date },
                    }]
                },
                loginAttempt: {
                    count:          { type: Number, maxlength: 7 },
                    lock:           { type: Boolean, maxlength: 5, default: false },
                    list: [{
                        ua:         { type: String, maxlength: 100 },
                        ip:         { type: String, maxlength: 20 },
                        location:   { type: String, maxlength: 100 },
                    }]
                }
            },
            activity: [{
                id: { type: Number, maxlength: 7, required: true },
                title: { type: String, maxlength: 200, required: true },
                cid: { type: Number, maxlength: 7, required: true }, //Customer ID
                location: { type: String, maxlength: 200, required: true },
                signature: { type: String, maxlength: 10000, required: true },
                screenshot: { type: String, maxlength: 10000, required: true },
                timeto: { type: Date, required: true },
                timefrom: { type: Date, required: true },
                date: { type: Date, required: true },
                date_added: { type: Date, required: true },
            }],
            itinerary: [{
                id:                 { type: Number, maxlength: 7, required: true },
                territory:          { type: String, maxlength: 200, required: false },
                activities: [{
                    aid:            { type: Number, maxlength: 200, required: true }, //Activity ID
                }],
                date:               { type: Date, required: false },
                date_added:         { type: Date, required: true },
                month:              { type: Number, required: true },
            }],
            customers: [{
                id:                 { type: Number, maxlength: 7, required: true },
                added_by:           { type: Number, maxlength: 7, required: true },
                date_added:         { type: Date, required: true },
            }],
            brochures: [{
                id:                 { type: Number, maxlength: 7, required: true },
                added_by:           { type: Number, maxlength: 7, required: true },
                date_added:         { type: Date, required: true },
            }],
            expenses: [{
                id:                 { type: Number, maxlength: 7, required: true },
                particulars:        { type: String, maxlength: 200, required: true },
                type:               { type: String, maxlength: 50, required: true },
                ornumber:           { type: String, maxlength: 50 },
                amount:             { type: Number, maxlength: 7, required: true },
                aid:                { type: Date, required: true }, //Activity ID
                date:               { type: Date, required: true },
                date_added:         { type: Date, required: true },
            }],
         
            date_added:             { type: Date, required: true, required: true },
            status:                 { type: Number, maxlength: 1, required: true, default: 1 },
            added_by:               { type: Number, maxlength: 7, required: true },
            lastUpdateBy:           { type: Number, maxlength: 7, required: true },
            lastUpdateDate:         { type: Date, required: true },
            isOnline:               { type: Boolean, default: false },
        }, { strict: true })),

        fn = lib.invocation().init(module.exports).fn;


    fn('allUsers', (data, callback) => {
        User.find({}, (err, findResult) => {
            if (err) { return callback({ sucess: false, message: err.message }); } else {
                return callback({ success: true, data: findResult });
            }
        })
    });

    fn('getAllAgentBrochure', (data, cb) => {
        
        data.auth.role === 'medrep' && (data.id = data.auth.id);
        
        User.aggregate([
            {
                $match: { id: data.id }
            },
            {
                $lookup:
                {
                    from: "brochures",
                    foreignField: "id",
                    localField: "brochures.id",
                    as: "bro"
                }
            },
            {
                $project: {
                    _id: 1,
                    name: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                    brochures: {
                        $map: {
                            input: "$bro",
                            as: "brochure",
                            in: {
                                id: "$$brochure.id",
                                title: "$$brochure.title",
                                description: "$$brochure.description",
                                brochures: "$$brochure.brochures",
                                count: { $size: "$$brochure.brochures" },
                            }
                        }
                    }
                }
            },    
        ], (err, getAllAgentBrochure) => {
            // console.log(data.id, getAllAgentBrochure);
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAllAgentBrochure.length){
                return cb({ success:true, data:getAllAgentBrochure[0] });
            }else{
                return cb({ success:false, message: "No brochures found!" });
            }
        }) 
    });

    fn('getAgentCustomers', (data, cb) => {
        User.aggregate([
            {
                $match: { id: data.id }
            },
            {
                $lookup:
                {
                    from: "customers",
                    foreignField: "id",
                    localField: "customers.id",
                    as: "cus"
                }
            },
            {
                $project: {
                    _id: 1,
                    name: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                    customers: {
                        $map: {
                            input: "$cus",
                            as: "customer",
                            in: {
                                cid: "$$customer.id",
                                company: "$$customer.name",
                                name: {
                                    $concat: [
                                        { $ifNull: ["$$customer.fname", ""] }, " ",
                                        { $ifNull: ["$$customer.lname", ""] }]
                                }
                            }
                        }
                    }
                }
            }
        ], (err, getAgentCustomers) => {
            // console.log(getAgentCustomers);
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAgentCustomers.length){
                return cb({ success:true, data:getAgentCustomers[0] });
            }else{
                return cb({ success:false, message: "No agent found!" });
            }
        })
    });

    
    fn('getAllAgentCustomers', (data, cb) => {
        User.aggregate([
            {
                $match: { id: data.auth.id }
            },
            {
                $lookup:
                {
                    from: "customers",
                    foreignField: "id",
                    localField: "customers.id",
                    as: "cus"
                }
            },
            {
                $lookup:
                {
                    from: "customers",
                    foreignField: "added_by",
                    localField: "id",
                    as: "agentcustomers"
                }
            },
            {
                $project: {
                    _id: 1,
                    agentcustomers: {
                        $map: {
                            input: "$agentcustomers",
                            as: "customer",
                            in: {
                                id: "$$customer.id",
                                company: "$$customer.name",
                                name: {
                                    $concat: [
                                        { $ifNull: ["$$customer.salutation", ""] }, " ",
                                        { $ifNull: ["$$customer.fname", ""] }, " ",
                                        { $ifNull: ["$$customer.lname", ""] }]
                                },
                                medical_specialty: "$$customer.medical_specialty",
                                sub_specialty: "$$customer.sub_specialty",
                                billing_address: "$$customer.billing_address",
                                shipping_address: "$$customer.shipping_address",
                                tin: "$$customer.tin",
                                email: "$$customer.email",
                                phone: "$$customer.phone",
                                mobile: "$$customer.mobile",
                                note: "$$customer.note",
                                contactPerson: "$$customer.contactPerson",
                            }
                        }
                    },
                    customers: {
                        $map: {
                            input: "$cus",
                            as: "customer",
                            in: {
                                id: "$$customer.id",
                                company: "$$customer.name",
                                name: {
                                    $concat: [
                                        { $ifNull: ["$$customer.salutation", ""] }, " ",
                                        { $ifNull: ["$$customer.fname", ""] }, " ",
                                        { $ifNull: ["$$customer.lname", ""] }]
                                },
                                medical_specialty: "$$customer.medical_specialty",
                                sub_specialty: "$$customer.sub_specialty",
                                billing_address: "$$customer.billing_address",
                                shipping_address: "$$customer.shipping_address",
                                tin: "$$customer.tin",
                                email: "$$customer.email",
                                phone: "$$customer.phone",
                                mobile: "$$customer.mobile",
                                note: "$$customer.note",
                                contactPerson: "$$customer.contactPerson",
                            }
                        }
                    }
                }
            },
            {
                $project:{
                    _id:1,
                    customers: {
                        $setUnion: ["$agentcustomers", "$customers"]
                    }
                }
            }
        ], (err, getAllAgentCustomers) => {
            // console.log(getAllAgentCustomers);
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAllAgentCustomers.length){
                return cb({ success:true, data:getAllAgentCustomers[0] });
            }else{
                return cb({ success:false, message: "No agent found!" });
            }
        })
    });

    fn('updateAgentBrochures', (data, cb) => {

        data.customers = (() => {
            return data.brochures.map(e => {
                return {
                    id: e.id,
                    date_added: new Date,
                    added_by: data.auth.id,
                }
            })
        })();

        User.updateOne(
            {
                id: data.id
            },
            {
                $set: {
                    brochures: data.brochures
                }
            },
            (err, updateAgentBrochures) => {
                if (err) return cb({ success: false, message: err.message });
                if (updateAgentBrochures.nModified) {
                    lib.createEmit("agentbrochures", data.id);
                    return cb({ success: true, message: "Brochures successfully added" });
                } else {
                    return cb({ success: false, data: updateAgentBrochures, message: "Unable to update brochure" });
                }
            }
        );
    });

    fn('updateAgentCustomers', (data, cb) => {

        data.customers = (() => {
            return data.customers.map(e => {
                return {
                    id: e.id,
                    date_added: new Date,
                    added_by: data.auth.id,
                }
            })
        })();

        User.updateOne(
            {
                id: data.id
            },
            {
                $set: {
                    customers: data.customers
                }
            },
            (err, updateUserCustomers) => {
                if (err) return cb({ success: false, message: err.message });
                if (updateUserCustomers.nModified) {
                    lib.createEmit("agentcustomers", data.id);
                    return cb({ success: true, message: "Customers successfully added" });
                } else {
                    return cb({ success: false, data: updateUserCustomers, message: "Unable to update customer" });
                }
            }
        );
    });

    fn('getUser', (data, cb) => {
        User.findOne({ _id: data.id }, (err, reponse) => {
            if (err) return cb({ success: false, message: err.message });
            if (reponse.length) {
                return cb({ success: true, message: "User found!", data: reponse });
            } else {
                return cb({ success: true, message: "No user found!", data: reponse });
            }
        })
    });

    fn('getUserById', (data, cb) => {
        User.findOne({id:  +data.id}, (err, reponse) => {
            if( err ) return cb({ success:false, message:err.message });
            if( reponse.length ){
                return cb({ success:true, message: "User found!", data:reponse });
            }else{
                return cb({ success:true, message: "No user found!", data:reponse });
            }
        });
    });

    fn('getUserById_mobile', (data, cb) => {
        User.findOne({ id: data.id }, (err, reponse) => {
            if (err) return cb({ success: false, message: err.message });
            if (reponse) {
                return cb({ success: true, message: "User found!", data: reponse });
            } else {
                return cb({ success: false, message: "No user found!", data: reponse });
            }
        });
    })

    fn('createUser', async (data, callback) => {
        data.username = data.username.toLowerCase();

        if (await lib.isUser({ username: data.username })) {
            return callback({ success: false, message: 'Username not available' });
        } else {
            data.role = (() => {
                switch (data.role) {
                    case 'superadmin': return 'superadmin';
                    case 'admin': return 'admin';
                    case 'accounting': return 'accounting';
                    case 'hr': return 'hr';
                    case 'inventory': return 'inventory';
                    case 'stockler': return 'stockler';
                    case 'sales': return 'sales';
                    case 'medrep': return 'medrep';
                    case 'marketing': return 'marketing';
                    default: return undefined;
                }
            })();

            data.added_by = data.auth.id,
            data.lastUpdateBy = data.auth.id,
            data.lastUpdateDate = new Date,
            data.date_added = new Date,
            data.status = 1;
            data.password = lib.createHash(data.password);
            data.session_id = lib.createHash([Math.random(), Math.random(), Math.random()].join(''));
            lib.create(data, (response) => {
                if (response.success) {
                    lib.createEmit('users');
                    return callback(response);
                } else {
                    return callback(response);
                }
            }, base, 10001);

        }
    });

    fn('getAllAgentProducts_mobile', async (data, cb) => {
        User.aggregate([
            {
                $match: {
                    id: data.auth.id
                }
            },
            {
                $lookup:
                {
                    from: "employees",
                    foreignField: "uid",
                    localField: "id",
                    as: "employee"
                }
            },
            {
                $unwind: "$employee"
            },
            {
                $project:{
                    _id: 1,
                    initial_principal: {
                        $filter: {
                           input: "$employee.principal",
                           as: "principal",
                           cond: {
                               $eq: [ "$$principal.status", 1 ]
                            }
                        }
                    },
                    initial_products: {
                        $filter: {
                           input: "$employee.products",
                           as: "product",
                           cond: {
                               $eq: [ "$$product.status", 1 ]
                            }
                        }
                    },
                }
            },
            {
                $lookup:
                {
                    from: "principals",
                    foreignField: "pid",
                    localField: "initial_principal.id",
                    as: "principal"
                }
            },
            {
                $project: {
                    initial_products: "$initial_products",
                    principal: {
                        $filter: {
                            input: {
                                $reduce: {
                                    input: "$principal.products",
                                    initialValue: [],
                                    in: { $concatArrays: [ "$$value", "$$this" ] }
                                }
                            },
                            as: "principal",
                            cond: {
                                $eq: [ "$$principal.status", 1 ]
                            }
                        }
                    }
                },
            },
            {
                $project: {
                    products: {
                       $setUnion: [
                        {
                            $map: {
                                input: {
                                     $setUnion: ["$initial_products", "$principal"]
                                },
                                as: "final",
                                in: { pid: "$$final.pid" }
                            }
                        }
                       ]
                    }
                }
            },
            {
                $lookup:
                {
                    from: "products",
                    foreignField: "id",
                    localField: "products.pid",
                    as: "products"
                }
            },
            {
                $project: {
                    products: {
                        $filter: {
                            input: {
                                $map: {
                                    input: "$products",
                                    as: "product",
                                    in: {
                                        pid: "$$product.id",
                                        name: "$$product.name",
                                        price: "$$product.price",
                                        genericname: "$$product.genericname",
                                        image: "$$product.image",
                                        unit: "$$product.unit",
                                        status: "$$product.status",
                                        quantity: 0,
                                    }
                                }
                            },
                            as: "product",
                            cond: {
                                $and:[
                                    { $eq: [ "$$product.status", 1 ] },
                                    { $gt: [ "$$product.price", 0 ] }
                                ]
                            }
                        }
                    }
                }
            }
        ], (err, getAllAgentProducts_mobile) => {
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAllAgentProducts_mobile.length){
                return cb({ success:true, data:getAllAgentProducts_mobile[0] });
            }else{
                return cb({ success:false, message: "No products found!" });
            }
        });
    });    

    fn('updateUser', async (data, cb) => {
        let isPasswordChange = false;
        let newData = {};
            if(data.data.password || data.data.password != ""){
                newData.role = data.data.role;
                newData.username = data.data.username;
                newData.lname = data.data.lname;
                newData.fname = data.data.fname;
                newData.mname = data.data.mname;
                newData.avatar = data.avatar;
                newData.password = lib.createHash(data.data.password);
                isPasswordChange = true;
            }
            else{
                newData.role = data.data.role;
                newData.username = data.data.username;
                newData.lname = data.data.lname;
                newData.fname = data.data.fname;
                newData.mname = data.data.mname;
                newData.avatar = data.avatar;
                isPasswordChange = false;
            }
            User.findOneAndUpdate({_id: data.data.uid}, newData, {upsert: true}, (err, response) =>{
                if( err ) return cb({ success:false, message:err.message });
                if( response ){
                    lib.createEmit('users');
                    return cb({ success:true, message: "User Information has been updated!", data: response, isPasswordChange: isPasswordChange  });
                }else{
                    return cb({ success:true, message: "No User has been modified!", data: response});
                }
            },);
    });

    fn('updateUserEmployee', async (data, cb) => {
        data.fname = data.form.fname;
        data.lname = data.form.lname;
        data.mname = data.form.mname;
        data.email = data.form.email;
        User.findOneAndUpdate({ id: data.form.uid }, data, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('users');
                return cb({ success: true, message: "User Information has been updated!", data: response });
            } else {
                return cb({ success: true, message: "No User has been modified!", data: response });
            }
        });
    });

    fn('deactivateUser', async (data, cb) => {
        User.findOneAndUpdate({ _id: data.id }, { "status": 2 }, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('users');
                return cb({ success: true, message: "User has been disabled", data: response });
            } else {
                return cb({ success: true, message: "No user has been modified!", data: response });
            }
        });
    });

    fn('enableUser', async (data, cb) => {
        User.findOneAndUpdate({ _id: data.id }, { "status": 1 }, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('users');
                return cb({ success: true, message: "User has been enabled", data: response });
            } else {
                return cb({ success: true, message: "No user has been modified!", data: response });
            }
        }, base);
    });

    fn('authenticate', async (req, callback) => {
        let data = req.body;
        let userObj = { username: data.username.toLowerCase() };

        if (await lib.isUser(userObj)) {
            userObj.status = 1;
            let user = await getUser(userObj);
            if (user) {
                if (lib.compareHash(data.password, user.password)) {
                    if ((data.type !== undefined && data.type === 'mobile' && user.role === 'medrep') || (data.type === undefined && user.role !== 'medrep')) {
                        let signInData = {
                            _id: user._id,
                            id: user.id,
                            username: user.username,
                            role: user.role,
                            status: user.status,
                            session_id: user.session_id,
                            fname: user.fname,
                            mname: user.mname,
                            lname: user.lname,
                            avatar: user.avatar
                        };

                        lib.sign(signInData, response => {
                            if (response.success) {
                                User.updateOne(
                                    {
                                        $and: [
                                            { id: user.id },
                                        ]
                                    },
                                    {
                                        $push: {
                                            "security.lastlogin.list": {
                                                ua: lib.UA(req),
                                                ip: lib.IP(req),
                                                location: Math.random(),
                                                date: new Date,
                                            }
                                        },
                                    },
                                    err => {
                                        console.log(`${user.username}.${user.role} connected.`);
                                        if (err) return callback({ success: false, message: err.message });
                                        if( data.type !== undefined && data.type === 'mobile' ){
                                            setTimeout(() => {
                                                return callback({ success: true, data: response.message });
                                            }, 2000);
                                        }else{
                                            return callback({ success: true, data: response.message });
                                        }
                                    });
                                // return callback({ success:true, data: response.message });
                            } else {
                                return callback(response);
                            }
                        });
                    } else {
                        return callback({ success: false, message: 'Your account doesn\'t have enough access permission.' });
                    }
                } else {
                    return callback({ success: false, message: 'Password incorrect' });
                }
            } else {
                return callback({ success: false, message: 'Account has been disabled' });
            }
        } else {
            return callback({ success: false, message: 'User not found' });
        }
    });

    fn('getAllUsers', (callback) => {
        User.find({}, (err, findResult) => {
            if (err) { return callback({ sucess: false, message: err.message }); } else {
                return callback({ success: true, data: findResult });
            }
        });
    });

    fn('getSecuredUser', (data, cb) => {
        User.aggregate([
            { $match: { id: +data.uid } },
            {
                $project: {
                    uid: "$id",
                    fname: "$fname",
                    lname: "$lname",
                    mname: "$mname",
                    role: "$role"
                }
            }
        ], (err, res) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: res });
            }
        });
    });

    fn('getAllSecuredUser', (data, cb) => {
        User.aggregate([
            {
                $project: {
                    uid: "$id",
                    fname: "$fname",
                    lname: "$lname",
                    mname: "$mname",
                    role: "$role",
                    fullName: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                }
            }
        ], (err, findResult) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: findResult });
            }
        });
    });

    fn('getAllAgents', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    role: 'medrep'
                }
            },
            {
                $lookup: {
                    from: "employees",
                    localField: "id",
                    foreignField: "uid",
                    as: "emp"
                }
            },
            { $unwind: "$emp" },
            {
                $project: {
                    _id: "$_id",
                    uid: "$id",
                    fname: "$fname",
                    lname: "$lname",
                    mname: "$mname",
                    role: "$role",
                    status: '$status',
                    email: "$email",
                    phone: "$emp.phone",
                    mobile: "$emp.mobile",
                    principal: { $ifNull: ["$emp.principal", []] },
                    fullName: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                }
            }
        ], (err, res) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: res });
            }
        });
    });

    fn('addActivity', (data, cb) => {
        Id.generate('medrep-activity', async (response) => {
            if (response.success) {
                User.updateOne(
                    { id: data.auth.id },
                    {
                        $push: {
                            activity: {
                                id: response.id,
                                title: data.title,
                                cid: data.cid,
                                location: data.location,
                                signature: data.signature,
                                screenshot: "",
                                timefrom: data.timefrom,
                                timeto: data.timeto,
                                date: data.date,
                                date_added: new Date()
                            }
                        }
                    }, (err, addActivity) => {
                        // console.log(addActivity)
                        if (err) return cb({ success: false, message: err.message });
                        if (addActivity.nModified) {
                            lib.createEmit('newactivity', {
                                uid: data.auth.id,
                                aid: response.id
                            });
                            return cb({ success: true, message: "Activity successfully added" });
                        } else {
                            return cb({ success: false, data: addActivity, message: "Unable to add new activity: " + err })
                        }
                    }
                )
            }
            else
            {
                return cb(response);
            }
        });
    });

    fn('getActivities', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    id: data.auth.id
                }
            },
            { $unwind: "$activity" },
            {
                $lookup: {
                    from: "customers",
                    localField: "activity.cid",
                    foreignField: "id",
                    as: "act"
                }
            },
            { $unwind: "$act" },
            {
                $project: {
                    name: {
                        $concat: [
                            { $ifNull: [ "$act.salutation", "" ] }, " ",
                            { $ifNull: [ "$act.fname", "" ] }, " ",
                            { $ifNull: [ "$act.lname", "" ] } ]
                    },
                    location: "$activity.location",
                    timefrom: "$activity.timefrom",
                    timeto: "$activity.timeto",
                    date: "$activity.date",
                    activityId: "$activity.id",
                    customerId: "$act.id",
                    date_added: "$act.date_added",
                    title: {
                        $ifNull: ["$activity.title", "No Title"]
                    }
                }
            }

        ], (err, res) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: res });
            }
        });
    });

    fn('getActivity', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    id: data.auth.id
                }
            },
            { $unwind: "$activity" },
            {
                $match: {
                    "activity.id": data.activityId
                }
            },
            {
                $lookup: {
                    from: "customers",
                    localField: "activity.cid",
                    foreignField: "id",
                    as: "act"
                }
            },
            {
                $project: {
                    salutation: "$act.salutation",
                    fname: "$act.fname",
                    lname: "$act.lname",
                    customerId: "$act.id",
                    medical_specialty: "$act.medical_specialty",
                    sub_specialty: "$act.sub_specialty",
                    title: "$activity.title",
                    location: "$activity.location",
                    timefrom: "$activity.timefrom",
                    timeto: "$activity.timeto",
                    date: "$activity.date",
                    Activityid: "$activity.id",
                    customerId: "$act.id",
                    // signature: { $ifNull: ["$activity.signature", ""] }
                    signature: {
                        $cond:{
                            if: { $eq:["$activity.signature", ""] },
                            then: false,
                            else: true
                        }
                    },
                }
            }
        ], (err, res) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: res });
            }
        });
    });

    fn('getMonthActivities', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    id: data.auth.id,
                    "itinerary.month": data.month,
                }
            },
            {
                $lookup: {
                    from: "customers",
                    localField: "*",
                    foreignField: "*",
                    as: "customers"
                }
            },
            {
                $project: {
                    customers: "$customers",
                    activities:  {
                        $cond:
                            {
                                if: { $isArray: "$activity" },
                                then: "$activity",
                                else: []
                            }
                        },
                    currentMonth: {
                        $cond:
                        {
                            if: { $gt: [ { $size: "$itinerary" } ,0 ] },
                            then: {
                                $cond:
                                {
                                    if: { $gte: [{ $indexOfArray: ["$itinerary.month",  data.month ]}, 0] },
                                    then: {
                                        $arrayElemAt: [ "$itinerary",  { $indexOfArray: ["$itinerary.month", data.month ] } ]
                                    },
                                    else: {}
                                }
                            },
                            else: {}
                        }
                    }
                }
            },
            {
                $project: {
                    activities: {
                        $map: {
                            input: "$currentMonth.activities",
                            as: "activity",
                            in: {
                                $let: {
                                    vars: {
                                        activityData: { $arrayElemAt: [ "$activities",  { $indexOfArray: ["$activities.id",  "$$activity.aid" ] } ] },
                                    },
                                    in: {
                                        date_added: "$$activityData.date_added",
                                        location: "$$activityData.location",
                                        timefrom: "$$activityData.timefrom",
                                        timeto: "$$activityData.timeto",
                                        title: "$$activityData.title",
                                        date: "$$activityData.date",
                                        itineraryId: "$currentMonth.id",
                                        signature: {
                                            $cond:{
                                                if: { $eq:["$$activityData.signature", ""] },
                                                then: false,
                                                else: true
                                            }
                                        },
                                        month: "$$activity.date",
                                        aid: "$$activityData.id",
                                        cid: "$$activityData.cid",
                                        customer: {
                                            $let: {
                                                vars: {
                                                    customer: {
                                                        $cond:
                                                        {
                                                            if: { $gt: [ { $size: "$customers" }, 0 ] },
                                                            then: {
                                                                $cond:
                                                                {
                                                                    if: { $gte: [{ $indexOfArray: ["$customers.id",  "$$activityData.cid"  ]}, 0] },
                                                                    then: {
                                                                        $arrayElemAt: [ "$customers",  { $indexOfArray: ["$customers.id", "$$activityData.cid"  ] } ]
                                                                    },
                                                                    else: {}
                                                                }
                                                            },
                                                            else: {}
                                                        }
                                                    }
                                                },
                                                in: {
                                                    id: "$$customer.id",
                                                    company: "$$customer.name",
                                                    name: {
                                                        $concat: [
                                                            { $ifNull: ["$$customer.fname", ""] }, " ",
                                                            { $ifNull: ["$$customer.lname", ""] }
                                                        ]
                                                    },
                                                }
                                            }
                                        }
                                    }
                                },
                            }
                        }
                    }
                }
            }

        ], (err, getMonthActivities)=>{
            if( err ) return cb({ sucess:false, message: err.message });
            if(getMonthActivities.length){
                return cb({ success:true, data:getMonthActivities[0] });
            }else{
                return cb({ success:false, message: "Itineraries not found!" });
            }
        })
    });

    fn('viewSignature', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    id: data.uid
                }
            },
            {
                $project: {
                    signature: {
                        $let: {
                            vars: {
                                act: {
                                    $cond:
                                        {
                                            if: { $gt: [ { $size: "$activity" }, 0 ] },
                                            then: {
                                                $cond:
                                                {
                                                    if: { $gte: [{ $indexOfArray: ["$activity.id",  data.aid  ]}, 0] },
                                                    then: {
                                                        $arrayElemAt: [ "$activity",  { $indexOfArray: ["$activity.id", data.aid  ] } ]
                                                    },
                                                    else: ""
                                                }
                                            },
                                            else: ""
                                        }
                                }
                            },
                            in: "$$act.signature"
                        }
                    }
                }
            }
        ], (err, viewSignature)=>{
            if( err ) return cb({ sucess:false, message: err.message });
            if(viewSignature.length){
                return cb({ success:true, data:viewSignature[0] });
            }else{
                return cb({ success:false, message: "No signature found!" });
            }
        })
    });

    fn('viewScreenshot', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    id: data.uid
                }
            },
            {
                $project: {
                    screenshot: {
                        $let: {
                            vars: {
                                act: {
                                    $cond:
                                        {
                                            if: { $gt: [ { $size: "$activity" }, 0 ] },
                                            then: {
                                                $cond:
                                                {
                                                    if: { $gte: [{ $indexOfArray: ["$activity.id",  data.aid  ]}, 0] },
                                                    then: {
                                                        $arrayElemAt: [ "$activity",  { $indexOfArray: ["$activity.id", data.aid  ] } ]
                                                    },
                                                    else: ""
                                                }
                                            },
                                            else: ""
                                        }
                                }
                            },
                            in: "$$act.screenshot"
                        }
                    }
                }
            }
        ], (err, viewSignature)=>{
            if( err ) return cb({ sucess:false, message: err.message });
            if(viewSignature.length){
                return cb({ success:true, data:viewSignature[0] });
            }else{
                return cb({ success:false, message: "No signature found!" });
            }
        })
    });

    fn('getAllActivitiesSuper', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    role: "medrep"
                }
            },
            {
                $lookup: {
                    from: "customers",
                    localField: "activity.cid",
                    foreignField: "id",
                    as: "customers"
                }
            },
            {
                $project: {
                    uid: "$id",
                    agent: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                    activity: "$activity",
                    customers: "$customers",
                    itineraries: {
                        $reduce: {
                            input: "$itinerary.activities",
                            initialValue: [],
                            in: { $concatArrays: [ "$$value", "$$this" ] }
                        }
                    }
                }
            },
            {
                $project: {
                    activities: {
                        $map: {
                            input: "$itineraries",
                            as: "it",
                            in: {
                                $let: {
                                    vars: {
                                        act: { $arrayElemAt: [ "$activity",  { $indexOfArray: ["$activity.id", "$$it.aid"  ] } ] }
                                    },
                                    in: {
                                        id: "$$act.id",
                                        title: "$$act.title",
                                        date: "$$act.date",
                                        date_added: "$$act.date_added",
                                        uid: "$uid",
                                        agent: "$agent",
                                        signature: {
                                            $cond:{
                                                if: { $eq:["$$act.signature", ""] },
                                                then: false,
                                                else: true
                                            }
                                        },
                                        screenshot: {
                                            $cond:{
                                                if: { $eq:["$$act.screenshot", ""] },
                                                then: false,
                                                else: true
                                            }
                                        },
                                        customer: {
                                            $let: {
                                                vars: {
                                                    cus: {
                                                        $cond:
                                                        {
                                                            if: { $gt: [ { $size: "$customers" }, 0 ] },
                                                            then: {
                                                                $cond:
                                                                {
                                                                    if: { $gte: [{ $indexOfArray: ["$customers.id",  "$$act.cid"  ]}, 0] },
                                                                    then: {
                                                                        $arrayElemAt: [ "$customers",  { $indexOfArray: ["$customers.id", "$$act.cid"  ] } ]
                                                                    },
                                                                    else: {}
                                                                }
                                                            },
                                                            else: {}
                                                        }
                                                    }
                                                },
                                                in: {
                                                    company: "$$cus.name",
                                                    name: {
                                                        $concat: [
                                                            { $ifNull: ["$$cus.fname", ""] }, " ",
                                                            { $ifNull: ["$$cus.lname", ""] }]
                                                    },
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $group:{
                    _id: 1,
                    activities: {
                        $addToSet: "$activities"
                     },
                }
            },
            {
                $project: {
                    activities: {
                        $reduce: {
                            input: "$activities",
                            initialValue: [],
                            in: { $concatArrays: [ "$$value", "$$this" ] }
                        },
                    },
                }
            },
        ], (err, getAllActivitiesSuper) => {
            // console.log(getAllActivitiesSuper);
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAllActivitiesSuper.length){
                return cb({ success:true, data:getAllActivitiesSuper[0] });
            }else{
                return cb({ success:false, message: "No activities found!" });
            }
        });
    });

    fn('getAllActivitiesCustomWeek', (data, cb) => {
        User.aggregate([
            {
                $match: {
                    role: "medrep"
                }
            },
            {
                $lookup: {
                    from: "customers",
                    localField: "activity.cid",
                    foreignField: "id",
                    as: "customers"
                }
            },
            {
                $project: {
                    uid: "$id",
                    agent: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                    activity: "$activity",
                    customers: "$customers",
                    itineraries: {
                        $reduce: {
                            input: "$itinerary.activities",
                            initialValue: [],
                            in: { $concatArrays: [ "$$value", "$$this" ] }
                        }
                    }
                }
            },
            {
                $project: {
                    activities: {
                        $map: {
                            input: "$itineraries",
                            as: "it",
                            in: {
                                $let: {
                                    vars: {
                                        act: { $arrayElemAt: [ "$activity",  { $indexOfArray: ["$activity.id", "$$it.aid"  ] } ] }
                                    },
                                    in: {
                                        id: "$$act.id",
                                        title: "$$act.title",
                                        date: "$$act.date",
                                        date_added: "$$act.date_added",
                                        uid: "$uid",
                                        agent: "$agent",
                                        signature: {
                                            $cond:{
                                                if: { $eq:["$$act.signature", ""] },
                                                then: false,
                                                else: true
                                            }
                                        },
                                        screenshot: {
                                            $cond:{
                                                if: { $eq:["$$act.screenshot", ""] },
                                                then: false,
                                                else: true
                                            }
                                        },
                                        customer: {
                                            $let: {
                                                vars: {
                                                    cus: {
                                                        $cond:
                                                        {
                                                            if: { $gt: [ { $size: "$customers" }, 0 ] },
                                                            then: {
                                                                $cond:
                                                                {
                                                                    if: { $gte: [{ $indexOfArray: ["$customers.id",  "$$act.cid"  ]}, 0] },
                                                                    then: {
                                                                        $arrayElemAt: [ "$customers",  { $indexOfArray: ["$customers.id", "$$act.cid"  ] } ]
                                                                    },
                                                                    else: {}
                                                                }
                                                            },
                                                            else: {}
                                                        }
                                                    }
                                                },
                                                in: {
                                                    company: "$$cus.name",
                                                    name: {
                                                        $concat: [
                                                            { $ifNull: ["$$cus.fname", ""] }, " ",
                                                            { $ifNull: ["$$cus.lname", ""] }]
                                                    },
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $group:{
                    _id: 1,
                    activities: {
                        $addToSet: "$activities"
                     },
                }
            },
            {
                $project: {
                    activities: {
                        $filter: {
                            input: {
                                $reduce: {
                                    input: "$activities",
                                    initialValue: [],
                                    in: { $concatArrays: [ "$$value", "$$this" ] }
                                } 
                            },
                            as: "activity",
                            cond: {
                                $and: [
                                    { $gte: ["$$activity.date", new Date(data.from)] },
                                    { $lte: ["$$activity.date", new Date(data.to)] }
                                ]
                            }
                        }
                    },
                }
            },
        ], (err, getAllActivitiesCustomWeek) => {
            // console.log(data.from, data.to);
            // console.log(getAllActivitiesCustomWeek);
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAllActivitiesCustomWeek.length){
                return cb({ success:true, data:getAllActivitiesCustomWeek[0] });
            }else{
                return cb({ success:false, message: "No activities found!" });
            }
        });
    });    

    fn('getDayActivities', (data, cb) => {
        let datematch = new Date(`${data.year}-${('0'+(data.month+1)).slice(-2)}-${('0'+data.day).slice(-2)} 00:00:00.000Z`);
        // console.log(datematch);
        User.aggregate([
            {
                $match: {
                    id: data.auth.id,
                    "itinerary.month": data.month,
                }
            },
            {
                $lookup: {
                    from: "customers",
                    localField: "*",
                    foreignField: "*",
                    as: "customers"
                }
            },
            {
                $project: {
                    customers: "$customers",
                    activities:  {
                        $cond:
                            {
                                if: { $isArray: "$activity" },
                                then: "$activity",
                                else: []
                            }
                        },
                    currentMonth: {
                        $cond:
                        {
                            if: { $gt: [ { $size: "$itinerary" } ,0 ] },
                            then: {
                                $cond:
                                {
                                    if: { $gte: [{ $indexOfArray: ["$itinerary.month",  data.month ]}, 0] },
                                    then: {
                                        $arrayElemAt: [ "$itinerary",  { $indexOfArray: ["$itinerary.month", data.month ] } ]
                                    },
                                    else: {}
                                }
                            },
                            else: {}
                        }
                    }
                }
            },
            {
                $project: {
                    activities: {
                        $filter: {
                            input: {
                                $map: {
                                    input: "$currentMonth.activities",
                                    as: "activity",
                                    in: {
                                        $let: {
                                            vars: {
                                                activityData: { $arrayElemAt: [ "$activities",  { $indexOfArray: ["$activities.id",  "$$activity.aid" ] } ] },
                                            },
                                            in: {
                                                date_added: "$$activityData.date_added",
                                                location: "$$activityData.location",
                                                timefrom: "$$activityData.timefrom",
                                                timeto: "$$activityData.timeto",
                                                title: "$$activityData.title",
                                                date: "$$activityData.date",
                                                itineraryId: "$currentMonth.id",
                                                signature: {
                                                    $cond:{
                                                        if: { $eq:["$$activityData.signature", ""] },
                                                        then: false,
                                                        else: true
                                                    }
                                                },
                                                month: "$$activity.date",
                                                aid: "$$activityData.id",
                                                customer: {
                                                    $let: {
                                                        vars: {
                                                            customer: {
                                                                $cond:
                                                                {
                                                                    if: { $gt: [ { $size: "$customers" }, 0 ] },
                                                                    then: {
                                                                        $cond:
                                                                        {
                                                                            if: { $gte: [{ $indexOfArray: ["$customers.id",  "$$activityData.cid"  ]}, 0] },
                                                                            then: {
                                                                                $arrayElemAt: [ "$customers",  { $indexOfArray: ["$customers.id", "$$activityData.cid"  ] } ]
                                                                            },
                                                                            else: {}
                                                                        }
                                                                    },
                                                                    else: {}
                                                                }
                                                            }
                                                        },
                                                        in: {
                                                            id: "$$customer.id",
                                                            company: "$$customer.name",
                                                            name: {
                                                                $concat: [
                                                                    { $ifNull: ["$$customer.fname", ""] }, " ",
                                                                    { $ifNull: ["$$customer.lname", ""] }
                                                                ]
                                                            },
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                    }
                                }
                            },
                            as: "activity",
                            cond: {
                                $eq: ["$$activity.date", datematch]
                             }
                         }
                    }
                }
            }

        ], (err, getDayActivities)=>{
            // console.log(getDayActivities);
            if( err ) return cb({ sucess:false, message: err.message });
            if(getDayActivities.length){
                return cb({ success:true, data:getDayActivities[0] });
            }else{
                return cb({ success:false, message: "No activities found!" });
            }
        })
    });

    fn('createItinerary', (data, cb) => {
        Id.generate('medrep-itinerary', async (response) => {
            // console.log(data.aid);
            User.updateOne(
                {
                    $and: [
                        { id: data.auth.id },
                        { "itinerary.activities.aid": { $ne: data.aid } }
                    ]
                },
                {
                    $push: {
                        itinerary: {
                            id: response.id,
                            activities: [{
                                aid: data.aid
                            }],
                            date_added: new Date(),
                            date: data.date,
                            month: new Date(data.date).getMonth()
                        }
                    }
                }, (err, createItinerary) => {
                    if (err) return cb({ success: false, message: err.message });
                    if (createItinerary.nModified) {
                        lib.createEmit('users');
                        return cb({ success: true, message: "Itinerary successfully added" });
                    } else {
                        return cb({ success: false, data: createItinerary, message: "Unable to add new itinerary: " + err })
                    }
                }
            )

        }, 200000)

    })



    fn('updateItinerary', (data, cb) => {
        if( data.month !== undefined){

            User.aggregate([
                {
                    $match: {
                        id: data.auth.id,
                        "itinerary.month": data.month,
                    }
                }
            ],
            (err, findItinerary)=>{
                if( err ) return cb({ sucess:false, message: err.message });
                if(findItinerary.length){
                    if(new Date(data.date).getMonth() !== data.month){
                        return cb({ success:false, message: "Activity date doesn't match with your Itinerary month." });
                    }else{
                        User.updateOne(
                            {
                                $and: [
                                    { id: data.auth.id },
                                    { "itinerary.month": data.month },
                                    { "itinerary.activities.aid": { $ne: data.aid } },
                                ]
                            },
                            {
                                $addToSet: {
                                    "itinerary.$.activities": {
                                        aid: data.aid
                                    }
                                }
                            },
                            (err, updateItinerary) => {
                                if (err) return cb({ success: false, message: err.message });
                                if (updateItinerary.nModified) {
                                    lib.createEmit('newactivity', {
                                        uid: data.auth.id,
                                        aid: data.aid
                                    });
                                    return cb({ success:true, message: "Itinerary Successfully Added" });
                                }else{
                                    return cb({ success:false, message: "Cannot add the same activity in your Itinerary" });
                                }   
                            }
                        );
                    }
                }else{
                    if(data.date !== data.month){
                        return cb({ success:false, message: "Activity date doesn't match with your Itinerary month." });
                    }else{
                        Id.generate('medrep-itinerary', async (response) => {
                            User.updateOne(
                                {
                                    $and: [
                                        { id: data.auth.id },
                                        {
                                            $and: [
                                                { "itinerary.month": { $ne: data.month } },
                                            ]
                                        }
                                    ]
                                },
                                {
                                    $push: {
                                        itinerary: {
                                            id: response.id,
                                            activities: [{
                                                aid: data.aid
                                            }],
                                            date_added: new Date(),
                                            date: data.date,
                                            month: data.month
                                        }
                                    }
                                }, (err, createItinerary) => {
                                    if (err) return cb({ success: false, message: err.message });
                                    if (createItinerary.nModified) {
                                        lib.createEmit('newactivity', {
                                            uid: data.auth.id,
                                            aid: data.aid
                                        });
                                        return cb({ success: true, message: "Itinerary successfully added" });
                                    } else {
                                        return cb({ success: false, message: "Unable to add new itinerary" })
                                    }
                                }
                            )
                        }, 200000);
                    }
                }
            });
        }else{
            return cb({ success: false, message: "Itinerary month not found!" })
        }
      });
    
      fn('getChatableUser', (data, cb) => {
        User.aggregate([
            {
                $match: {id: {$ne: data.auth.id}}
            },
            {
                $project: {
                    _id: 1,
                    uid: "$id",
                    fname: "$fname",
                    lname: "$lname",
                    mname: "$mname",
                    role: "$role",
                    avatar: "$avatar",
                    isOnline: "$isOnline",
                    fullName: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                }
            }
        ], (err, res) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: res });
            }
        });
    });

    fn('getAllChatableUser', (data, cb) => {
        User.aggregate([
            {
                $project: {
                    uid: "$id",
                    fname: "$fname",
                    lname: "$lname",
                    mname: "$mname",
                    role: "$role",
                    avatar: "$avatar",
                    isOnline: "$isOnline",
                    fullName: {
                        $concat: [
                            { $ifNull: ["$fname", ""] }, " ",
                            { $ifNull: ["$mname", ""] }, " ",
                            { $ifNull: ["$lname", ""] }]
                    },
                }
            }
        ], (err, res) => {
            if (err) { return cb({ sucess: false, message: err.message }); } else {
                return cb({ success: true, data: res });
            }
        });
    })

    fn('updateSignature', (data, cb) => {
        if (data.aid) {
            User.updateOne(
                {
                    $and: [

                        { id: data.auth.id },
                        { "activity.id": data.aid },
                    ]
                },
                {
                    $set: {

                        "activity.$.signature": data.signature
                    }
                },
                (err, updateSignature) => {
                    if (err) return cb({ success: false, message: err.message });
                    if (updateSignature.nModified) {
                        lib.createEmit('newactivity', {
                            uid: data.auth.id,
                            aid: data.aid
                        });
                        return cb({ success: true, message: updateSignature });
                    } else {
                        return cb({ success: false, message: "Unable to update signature" });
                    }
                });
        } else {
            return cb({ success: false, message: "activity not found" });
        }
    });

    fn('updateScreenshot', (data, cb) => {
        if (data.aid) {
            User.updateOne(
                {
                    $and: [

                        { id: data.auth.id },
                        { "activity.id": data.aid },
                    ]
                },
                {
                    $set: {

                        "activity.$.screenshot": data.screenshot
                    }
                },
                (err, updateScreenshot) => {
                    if (err) return cb({ success: false, message: err.message });
                    if (updateScreenshot.nModified) {
                        lib.createEmit('newactivity', data.aid);
                        return cb({ success: true, message: updateScreenshot });
                    } else {
                        return cb({ success: false, message: "Unable to update screenshot" });
                    }
                });
        } else {
            return cb({ success: false, message: "activity not found" });
        }
    });

    fn('getCurrentItinerary', (data,cb) => {    
        User.aggregate([
            {
                $match: {
                    id: data.auth.id,
                }
            },
            {
                $project: {
                    activity:  {
                        $cond:
                            {
                                if: { $isArray: "$activity" },
                                then: "$activity",
                                else: []
                            }
                        },
                    currentMonth: {
                        $cond:
                        {
                            if: { $gt: [ { $size: "$activity" } ,0 ] },
                            then: {
                                $cond:
                                {
                                    if: { $gte: [{ $indexOfArray: ["$itinerary.month",  data.newdate ]}, 0] },
                                    then: {
                                        $arrayElemAt: [ "$itinerary",  { $indexOfArray: ["$itinerary.month", data.newdate ] } ]
                                    },
                                    else: {}
                                }
                            },
                            else: {}
                        }
                    }
                }
            },
            {
                $project: {
                    activities: {
                        $map: {
                            input: "$currentMonth.activities",
                            as: "activity",
                            in: {
                                $let: {
                                    vars: {
                                        activityData: { $arrayElemAt: [ "$activity",  { $indexOfArray: ["$activity.id",  "$$activity.aid" ] } ] },
                                    },
                                    in: {
                                        date_added: "$$activityData.date_added",
                                        location: "$$activityData.location",
                                        timefrom: "$$activityData.timefrom",
                                        timeto: "$$activityData.timeto",
                                        title: "$$activityData.title",
                                        date: "$$activityData.date",
                                        itineraryId: "$currentMonth.id",
                                        month: "$$activity.date",
                                        aid: "$$activityData.id",
                                        cid: "$$activityData.cid"
                                    }
                                },
                            }
                        }
                    }
                }
            }

        ], (err, getItineraries)=>{
            if( err ) return cb({ sucess:false, message: err.message });
            if(getItineraries.length){
                return cb({ success:true, data:getItineraries[0] });
            }else{
                return cb({ success:false, message: "Itineraries not found!" });
            }
        })
    });

    fn('changeStatus', (data, cb) => {
        data.isOnline = data.status;
        User.updateOne(
            { id: +data.id},
            {
                $set:{
                    "isOnline" : data.status
                }
            },
            (err, res) => {
            if( err ) return cb({ success:false, message:err.message });
            if(res.nModified){
                lib.createEmit('userStatus', data);
                return cb({ success:true, message:res });
            }else{
                return cb({ success:false, message: "Unable to update data", toaster: 'off'});
            }
        });
    });

    fn('getAllUsersWithProfile', (data, cb) => {
        User.aggregate([
            {
                $match: { role: {$ne: "medrep"} }
            },
            {
                $lookup:{
                    from: "employees",
                    foreignField: "uid",
                    localField: "id",
                    as: "employee"
                }
            },            
            {
                $unwind: "$employee" 
            },
            {
                $unwind: "$employee.employment"
            },
            {
                $match: {"employee.employment.status": true}
            },
            {
                $project: {
                    _id: 1,
                    id:         "$id",
                    name:       { $concat: ["$lname", ", ", "$fname", ' ', "$mname" ] },
                    position:   "$employee.employment.position"
                }
            }
        ], (err, res) => {
            if( err ) return cb({ sucess:false, message: err.message });
            
            if(res.length){
                return cb({ success:true, data: res });
            }else{
                return cb({ success:false, message: "data not found!" });
            }
        });
    });

    fn('getAllPayrolledUsers', (data, cb) => {
        User.aggregate([
            {
                $match: { 
                    role: {$ne: "medrep"},
                    id: { $nin: data.data}
                }
            },
            {
                $project: {
                    _id: 1,
                    uid:            "$id",
                    name:           { $concat: ["$lname", ", ", "$fname", ' ', "$mname" ] },
                    position:       "$employee.employment.position",
                    totalHours:     '0',
                    totalMinutes:   '0',
                    total:          '0',
                    in:             '0',
                    out:            '0', 
                    status:         '0',
                    attendance:     [
                        {
                            'in': '0'
                        }
                    ],
                }
            }
        ], (err, res) => {
            if( err ) return cb({ success:false, message: err.message });
            
            if(res.length){
                return cb({ success:true, data: res });
            }else{
                return cb({ success:false, message: "data not found!" });
            }
        });
    });


    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

    /**
     * @param {Object} data object and values to check
     * @return {Object} User object data
     */

    function getUser(data) {
        // console.log(data);
        return new Promise((resolve) => {
            if (Object.keys(data).length) {
                User.findOne(data, (err, findOneResult) => {
                    if (err) resolve(false);
                    resolve(findOneResult);
                });
            } else {
                resolve(false);
            }
        });
    }

    lib.createSocket((client, io) => {
        return client.on('newactivity', (data) => {
            console.log(`'newactivity' emit received at`, __filename, `with data "${data}"`);
            io.emit('newactivity', { success: true, data: data })
        });
    });

    lib.createSocket((client, io) => {
        return client.on('users', (data) => {
            console.log(`'users' emit received at`, __filename, `with data "${data}"`);
            io.emit('users', { success: true, data: data })
        });
    });

    lib.createSocket((client, io) => {
        return client.on('newuseritinerary', (data) => {
            console.log(`'newuseritinerary' emit received at`, __filename, `with data "${data}"`);
            io.emit('newuseritinerary', { success: true, data: data })
        });
    });

    lib.createSocket((client, io) => {
        return client.on('userStatus', (data) => {
            io.emit('userStatus', { success: true, data: data })
        });
    });

    lib.createSocket((client, io) => {
        return client.on('agentcustomers', (data) => {
            console.log(`'agentcustomers' emit received at`, __filename, `with data "${data}"`);
            io.emit('agentcustomers', { success: true, data: data })
        });
    });
    
    lib.createSocket((client, io) => {
        return client.on('agentbrochures', (data) => {
            console.log(`'agentbrochures' emit received at`, __filename, `with data "${data}"`);
            io.emit('agentbrochures', { success: true, data: data })
        });
    });

})();