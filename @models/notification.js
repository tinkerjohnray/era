'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    lib = require('../library'),

    /**
     * @see Function/method/class relied on
     * @link https://mongoosejs.com/docs/guide.html
     */
    Notify = module.exports = mongoose.model(base, mongoose.Schema({
        id:                     { type:Number, maxlength:7, required:true },
        role_module:            { type:String, maxlength:100, required:false },
        document_id:            { type:Number, maxlength:7, required:true },
        message:                { type:String, maxlength:100, required:true },
        from:                   { type:String, maxlength:50, required:true },
        type:                   { type:String, maxlength:50, required:true },
        url:                    { type:String, maxlength:50 },
        for:[{
            role:               { type:String, maxlength:50, required:true },
        }],
        readby:[{
            uid:                { type:Number, maxlength:7, required:true },
        }],
        deletedby:[{
            uid:                { type:Number, maxlength:7, required:true },
        }],
        date_added:             { type:Date, required:true, required:true },
        status:                 { type:Number, maxlength:1, required:true, default:1 },
        added_by:               { type:Number, maxlength:7, required:true },
    }, { strict: true })),

    fn = lib.invocation().init(module.exports).fn;
    
    fn('getAllNotifications', (data, cb) => {
        Notify.aggregate([
            {
                $match: {
                    $and: [
                        { "for.role": data.auth.role },
                        { "deletedby.uid": { $ne: data.auth.id } }
                    ]
                }
            },
            {
                $lookup:
                {
                    from: "users",
                    localField: "added_by",
                    foreignField: "id",
                    as: "addedBy"
                }
            },
            { $unwind: "$addedBy" },
            {
                $project: {
                    _id: 1,
                    id: "$id",
                    message: "$message",
                    date_added: "$date_added",
                    added_by: { $concat: [
                        { $ifNull: [ "$addedBy.fname", "" ] }, " ",
                        { $ifNull: [ "$addedBy.mname", "" ] }, " ",
                        { $ifNull: [ "$addedBy.lname", "" ] } ]
                    },
                }
            },
            { $sort: { id: -1 } },
        ], (err, getAllNotifications) => {
            if( err ) return cb({ sucess:false, message: err.message });
            if(getAllNotifications.length){
                return cb({ success:true, data:getAllNotifications });
            }else{
                return cb({ success:false, message: "No notification found!" });
            }
        });
    });

    fn('getNotification', (data, cb) => {
        Notify.aggregate([
            {
                $match: {
                    $and: [
                        { "for.role": data.auth.role },
                        { "deletedby.uid": { $ne: data.auth.id } }
                    ]
                }
            },
            {
                $lookup:
                {
                    from: "users",
                    localField: "added_by",
                    foreignField: "id",
                    as: "addedBy"
                }
            },
            { $unwind: "$addedBy" },
            {
                $project: {
                    _id: 1,
                    id: "$id",
                    role_module: "$role_module",
                    document_id: "$document_id",
                    for: "$for",
                    readby: "$readby",
                    url: "$url",
                    message: "$message",
                    date_added: "$date_added",
                    added_by: { $concat: [
                        { $ifNull: [ "$addedBy.fname", "" ] }, " ",
                        { $ifNull: [ "$addedBy.mname", "" ] }, " ",
                        { $ifNull: [ "$addedBy.lname", "" ] } ]
                    },
                }
            },
            { $sort: { id: -1 } },
            { $limit : 20 }
        ], (err, getNotification) => {
            if( err ) return cb({ sucess:false, message: err.message });
            if(getNotification.length){
                return cb({ success:true, data:getNotification });
            }else{
                return cb({ success:false, message: "No notification found!" });
            }
        });
    });

    fn('getNotificationLength', (data, cb) => {
        Notify.aggregate([
            {
                $match: {
                    $and: [
                        { "for.role": data.auth.role },
                        { "readby.uid": { $ne: data.auth.id } },
                        { "deletedby.uid": { $ne: data.auth.id } }
                    ]
                } 
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            }
        ], (err, getNotificationLength) => {
            if( err ) return cb({ sucess:false, message: err.message });
            if(getNotificationLength.length){
                return cb({ success:true, data:getNotificationLength[0] });
            }else{
                return cb({ success:false, message: "No notification found!" });
            }
        });
    });
    
    fn('updateNotificationRead', (data, cb) => {
        Notify.updateOne(
            {
                $and: [
                    { id: data.nid },
                ]
            },
            {
                $addToSet:{
                    "readby": {
                        uid: data.auth.id
                    }
                }
            },
            (err, updateNotificationRead) => {
            if( err ) return cb({ success:false, message:err.message });
            if(updateNotificationRead.nModified){
                lib.createEmit('notifications', { uid:data.auth.id, readonly:true });
                return cb({ success:true, message:updateNotificationRead });
            }else{
                return cb({ success:false, message: "Unable to update notification" });
            }
        });
    });

    fn('updateNotificationDelete', (data, cb) => {
        Notify.updateOne(
            {
                $and: [
                    { id: data.nid },
                ]
            },
            {
                $addToSet:{
                    "deletedby": {
                        uid: data.auth.id
                    }
                }
            },
            (err, updateNotificationDelete) => {
            if( err ) return cb({ success:false, message:err.message });
            if(updateNotificationDelete.nModified){
                lib.createEmit('notifications', { uid:data.auth.id, readonly:true });
                return cb({ success:true, message:updateNotificationDelete });
            }else{
                return cb({ success:false, message: "Unable to update notification" });
            }
        });
    });

    /**
     * @description Private model functions;
     * only accessible on this model file; 
     * @value for.status: 1 = active/unread/default
     * @value for.status: 0 - inactive/read
     * @IMPORTANT never execute in frontend
     */
    fn('newNotification', (data, cb) => {
        data.date_added = new Date;
        data.status = 1;
        data.for.map( (result, loc) => { data.for[loc] = { role:result } });
        data.deletedby = [
            { uid: data.added_by }
        ];
        lib.create(data, (response) => {
            if(response.success){
                lib.createEmit('notifications', { uid:data.added_by, readonly:false, for:data.for });
                // return cb(response);
            }else{
                // return cb(response);
            }
        }, base, 800);
    });

    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */
     
    lib.createSocket( (client, io) => {
        return client.on('notifications', (data) => {
            console.log(`'notifications' emit received at`, __filename, `with data "${data}"`);
            io.emit('notifications', { success:true, data:data })
        });
    });


})();