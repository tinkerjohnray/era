'use strict';

(async function init() {

    const
        base = require('path').basename(__filename).split('.')[0],
        mongoose = require('mongoose'),
        Id = require('./id'),
        lib = require('../library'),

        /**
         * @see Function/method/class relied on
         * @link https://mongoosejs.com/docs/guide.html
         */
        Benefit = module.exports = mongoose.model(base, mongoose.Schema({
            id: { type: Number, maxLength: 7, required: true },
            name: { 
                sss: { type: String, maxLength: 100, required: true}, 
                bir: { type: String, maxLength: 100, required: true}, 
                pagibig: { type: String, maxLength: 100, required: true}
            },
            forms: [{
                formName: { type: String, maxLength: 50, required: true },
         
            }],
          
            status: { type: Number, required: true, default: 1 },
            date_added: { type: Date, required: true, required: true },
            status: { type: Number, maxlength: 1, required: true, default: 1 },
            added_by: { type: Number, maxlength: 7, required: true },
            lastUpdateBy: { type: Number, maxlength: 7, required: true },
            lastUpdateDate: { type: Date, required: true },
        }, { strict: true })),

        fn = lib.invocation().init(module.exports).fn;




    // fn('edited', (data, cb) => {
    //     data.date_added = new Date;
    //     data.status = 1;
    //     data.added_by = data.auth.id;
    //     data.lastUpdateBy = data.auth.id;
    //     data.lastUpdateDate = new Date;
    //     Brand.updateOne(
    //         {
    //             id: data.id
    //         },
    //         {
    //             $set: {
    //                 brand: data.brand,
    //                 model: data.model,
    //                 specs: {
    //                     chipset: data.specs.chipset,
    //                     operatingsystem: data.specs.operatingsystem,
    //                     processor: data.specs.processor,
    //                     touch: data.specs.touch,
    //                 },
    //             }
    //         },
    //         (err, edited) => {
    //             if (err) return cb({ success: false, message: err });
    //             if (edited.nModified) {
    //                 return cb({ success: true, message: `User has been edited!` });
    //             }
    //         }
    //     )
    // })



    fn('addForm', async (data, cb) => {
        data.date_added = new Date;
        data.status = 1;
        data.added_by = data.auth.id;
        data.lastUpdateBy = data.auth.id;
        data.lastUpdateDate = new Date;
        lib.create(data, (response) => {
            if (response.success) {
                lib.createEmit('forms');
                return cb(response);
            } else {
                return cb(response);
            }
        }, base, 600);
    });




    // fn('deleteBtn', (data, cb) => {
    //     data.date_added = new Date;
    //     data.status = 1;
    //     data.added_by = data.auth.id;
    //     data.lastUpdateBy = data.auth.id;
    //     data.lastUpdateDate = new Date;
    //     Brand.updateOne(
    //         {
    //             id: data.id
    //         },
    //         {
    //             $set: {
    //                 status: 0,
    //             }
    //         },
    //         (err, deleteBtn) => {
    //             if (err) return cb({ success: false, message: err });
    //             if (deleteBtn.nModified) {
    //                 return cb({ success: true, message: `User has been deleted!` });
    //             }
    //         }
    //     )
    // })

    // fn('getMobile', (cb) => {
    //     Brand.aggregate([
    //         {
    //             $match: {
    //                 status: 1
    //             }   
    //         },
    //         {
    //             $project: {
    //                 id: "$id",
    //                 _id: "$_id",
    //                 // brand: {
    //                 //     $concat: ["$brand", "$specs.chipset"]
    //                 // },
    //                 brand: "$brand",
    //                 specs: "$specs",
    //                 model: "$model",
    //             },
    //         },
    //         {
    //             $sort: { id: -1 }
    //         }

    //     ], cb)
    // })




    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */
})();