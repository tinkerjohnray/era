'use strict';

(async function init() {

    const
        base = require('path').basename(__filename).split('.')[0],
        mongoose = require('mongoose'),
        Id = require('./id'),
        lib = require('../library'),

        /**
         * @see Function/method/class relied on
         * @link https://mongoosejs.com/docs/guide.html
         */
        Sss = module.exports = mongoose.model(base, mongoose.Schema({
            id: { type: Number, maxLength: 7, required: true },
            fname: { type: String, maxLength: 100, required: true },
            mname: { type: String, maxLength: 100, required: true },
            lname: { type: String, maxLength: 100, required: true },
            sex: { type: String, maxLength: 100, required: true },
            basic: {
                age: { type: Number, maxLength: 50, required: true },
                address: { type: String, maxLength: 100, required: true, },
                company: { type: String, maxLength: 100, required: true, },
            },
            status: { type: Number, required: true, default: 1 },


            date_added: { type: Date, required: true, required: true },
            status: { type: Number, maxlength: 1, required: true, default: 1 },
            added_by: { type: Number, maxlength: 7, required: true },
            lastUpdateBy: { type: Number, maxlength: 7, required: true },
            lastUpdateDate: { type: Date, required: true },
        }, { strict: true })),

        fn = lib.invocation().init(module.exports).fn;




    fn('editApplicants', (data, cb) => {
        data.date_added = new Date;
        data.status = 1;
        data.added_by = data.auth.id;
        data.lastUpdateBy = data.auth.id;
        data.lastUpdateDate = new Date;
        Sss.updateOne(
            {
                id: data.id
            },
            {
                $set: {
                    sex: data.sex,
                    fname: data.fname,
                    mname: data.mname,
                    lname: data.lname,
                    basic: {
                        age: data.basic.age,
                        address: data.basic.address,
                        company: data.basic.company,
                    },
                }
            },
            (err, editApplicants) => {
                if (err) return cb({ success: false, message: err });
                if (editApplicants.nModified) {
                    return cb({ success: true, message: `Applicant has been edited!` });
                }
            }
        )
    })



    fn('createForm', async (data, cb) => {
        data.date_added = new Date;
        data.status = 1;
        data.added_by = data.auth.id;
        data.lastUpdateBy = data.auth.id;
        data.lastUpdateDate = new Date;
        lib.create(data, (response) => {
            if (response.success) {
                lib.createEmit('forms');
                return cb(response);
            } else {
                return cb(response);
            }
        }, base, 100);
    });




    fn('deleteApplicants', (data, cb) => {
        data.date_added = new Date;
        data.status = 1;
        data.added_by = data.auth.id;
        data.lastUpdateBy = data.auth.id;
        data.lastUpdateDate = new Date;
        Sss.updateOne(
            {
                id: data.id
            },
            {
                $set: {
                    status: 0,
                }
            },
            (err, deleteApplicants) => {
                if (err) return cb({ success: false, message: err });
                if (deleteApplicants.nModified) {
                    return cb({ success: true, message:  `Applicant has been deleted!` });
                }
            }
        )
    })

    fn('getApplicants', (cb) => {
        Sss.aggregate([
            {
                $match: {
                    status: 1
                }
            },
            {
                $project: {
                    id: "$id",
                    _id: "$_id",
                    fname: "$fname",
                    lname: "$lname",
                    mname: "$mname",
                    basic: "$basic",
                    sex: "$sex"
                },
            },
            {
                $sort: { id: -1 }
            }

        ], cb)
    })




    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */
})();