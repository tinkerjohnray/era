'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    Notify = require('./notification'),
    lib = require('../library'),

    Product = module.exports = mongoose.model(base, mongoose.Schema({
        id:                 { type:Number, maxlength:7, required:true },
        name:               { type:String, maxlength:50, required:true },
        genericname:        { type:String, maxlength:50 },
        brandname:          { type:String, maxlength:50 },
        manufacturer:       { type:String, maxlength:50 },
        unit:               { type:String, maxlength:50 },
        barcode:            { type:String, maxlength:50 },
        sku:                { type:String, maxlength:50 },
        description:        { type:String, maxlength:200, required:true },
        image:              { type:String, maxlength:100 },
        price:              { type:Number, maxlength:11, required:true },
        stocklevel:{
            critical_stock:     { type:Number, maxlength:11, required:true },
            low_stock:          { type:Number, maxlength:11, required:true },
            balance_stock:      { type:Number, maxlength:11, required:true },
            good_stock:         { type:Number, maxlength:11, required:true },
            over_stock:         { type:Number, maxlength:11, required:true },
        },
        date_added:         { type:Date, required:true, required:true },
        status:             { type:Number, maxlength:1, required:true, default:1 },
        added_by:           { type:Number, maxlength:7, required:true },
        lastUpdateBy:       { type:Number, maxlength:7, required:true },
        lastUpdateDate:     { type:Date, required:true },
    }, { strict: true })),

    fn = lib.invocation().init(module.exports).fn;

        fn('addProduct', (data, cb) => {
            data.price = data.price || 0;
            data.date_added = new Date;
            data.status = 1;
            data.added_by = data.auth.id;
            data.lastUpdateBy = data.auth.id;
            data.lastUpdateDate = new Date;
            lib.create(data, (response) => {
                if(response.success){
                    lib.createEmit('products');

                    // if(data.auth.role !== 'admin'){
                        Notify.newNotification({
                            message: `New Product #${response.data.id} has been added and need to set a price.`,
                            role_module: 'inventory',
                            document_id: response.data.id,
                            from: data.auth.role,
                            type: "notify",
                            url: "/products/all-products",
                            for: ['admin'],
                            added_by: data.auth.id,
                        }, () => {}); 
                    // }                   

                    return cb(response);
                }else{
                    return cb(response);
                }
            }, base, 800);
        });

        fn('getAllProducts', (data, callback) => {
            Product.aggregate([
                { $match: { status: 1 } },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "added_by",
                        foreignField: "id",
                        as: "addedBy"
                    }
                },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "lastUpdateBy",
                        foreignField: "id",
                        as: "lastUpdateBy"
                    }
                },
                { $unwind: "$addedBy" },
                { $unwind: "$lastUpdateBy" },
                {
                    $project: {
                        _id: 1,
                        id: "$id",
                        name: "$name",
                        genericname: "$genericname",
                        brandname: "$brandname",
                        manufacturer: "$manufacturer",

                        unit: "$unit",
                        price: "$price",
                        supplier_id: "$supplier_id",
                        barcode: "$barcode",
                        sku: "$sku",
                        description: "$description",
                        image: "$image",
                        supplier_price: "$supplier_price",

                        date_added: "$date_added",
                        status: "$status",
                        added_by: { $concat: [
                            { $ifNull: [ "$addedBy.lname", "" ] }, ", ",
                            { $ifNull: [ "$addedBy.fname", "" ] }, " ",
                            { $ifNull: [ "$addedBy.mname", "" ] } ]
                        },
                        lastUpdateDate: "$lastUpdateDate",
                        lastUpdateBy: { $concat: [
                            { $ifNull: [ "$lastUpdateBy.lname", "" ] }, ", ",
                            { $ifNull: [ "$lastUpdateBy.fname", "" ] }, " ",
                            { $ifNull: [ "$lastUpdateBy.mname", "" ] } ]
                        },
                    }
                }
            ], (err, getAllProductsResult)=>{
                if( err ) return callback({ sucess:false, message: err.message });
                if(getAllProductsResult.length){
                    return callback({ success:true, data:getAllProductsResult });
                }else{
                    return callback({ success:false, message: "No products found!", toaster: 'off'  });
                }
            });
        });

        fn('getProductById', (data, cb) => {
            Product.aggregate([
                { $match: { id: data.id, status: 1 } },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "added_by",
                        foreignField: "id",
                        as: "fromUsers"
                    }
                },
                { $unwind: "$fromUsers" },
                {
                    $project: {
                        _id: 1,
                        id: "$id",
                        pid: "$id",
                        name: "$name",
                        price: "$price",
                        genericname: "$genericname",
                        brandname: "$brandname",
                        manufacturer: "$manufacturer",

                        unit: "$unit",
                        supplier_id: "$supplier_id",
                        barcode: "$barcode",
                        sku: "$sku",
                        description: "$description",
                        image: "$image",
                        supplier_price: "$supplier_price",
                        stocklevel: "$stocklevel",
                    }
                }
            ], (err, findResult)=>{
                if( err ) return cb({ sucess:false, message: err.message });
                if(findResult.length){
                    return cb({ success:true, data:findResult[0] });
                }else{
                    return cb({ success:false, message: "Product not found!", toaster: 'off'   });
                }
            });
        });

        //Inventory
        ///#/inventory/inventory-manager/all-inventory
        fn('getAllInventory', (data, cb) => {
            Product.aggregate([
                {
                    $match: {
                        status: 1
                    }
                },
                {
                    $lookup:
                    {
                        from: "warehouses",
                        localField: "*",
                        foreignField: "*",
                        as: "warehouses"
                    }
                },
                { $unwind: "$warehouses"},
                { $unwind : { path: "$warehouses", preserveNullAndEmptyArrays: true } },
                { $match: { "warehouses.status": 1 } },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "*",
                        foreignField: "*",
                        as: "lastUpdateBy"
                    }
                },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "*",
                        foreignField: "*",
                        as: "addedBy"
                    }
                },
                {
                    $project: {
                        _id: 1,
                        others: {
                            $let: {
                                vars: {
                                   theProduct: { 
                                    $arrayElemAt: [ "$warehouses.inventory",  {
                                        $indexOfArray: ["$warehouses.inventory.pid",  "$id" ]
                                    } ] 
                                   }
                                },
                                in: {
                                    $cond:
                                    {
                                        if: { $gte: [{ $indexOfArray: ["$warehouses.inventory.pid",  "$id" ]}, 0] },
                                        then: {
                                            total: "$$theProduct.total",
                                            entry: {
                                                $let: {
                                                    vars: {
                                                        lastUpdateBy: {
                                                            $arrayElemAt: ["$lastUpdateBy",  {
                                                                $indexOfArray: ["$lastUpdateBy.id",  "$$theProduct.lastUpdateBy" ]
                                                            }]
                                                        },
                                                        addedBy: {
                                                            $arrayElemAt: ["$addedBy",  {
                                                                $indexOfArray: ["$addedBy.id",  "$$theProduct.added_by" ]
                                                            }]
                                                        },
                                                    },
                                                    in: {
                                                        date_added: "$$theProduct.date_added",
                                                        lastUpdateDate: "$$theProduct.lastUpdateDate",
                                                        lastUpdateBy: { $concat: [
                                                            { $ifNull: [ "$$lastUpdateBy.lname", "" ] }, ", ",
                                                            { $ifNull: [ "$$lastUpdateBy.fname", "" ] }, " ",
                                                            { $ifNull: [ "$$lastUpdateBy.mname", "" ] } ]
                                                        },
                                                        addedBy: { $concat: [
                                                            { $ifNull: [ "$$addedBy.lname", "" ] }, ", ",
                                                            { $ifNull: [ "$$addedBy.fname", "" ] }, " ",
                                                            { $ifNull: [ "$$addedBy.mname", "" ] } ]
                                                        },
                                                    }
                                                }
                                            }
                                        },
                                        else: {
                                            total: 0
                                        }
                                    }
                                },
                             }
                        },
                        pid: "$id",
                        pname: "$name",
                        wname: "$warehouses.name",
                        genericname: "$genericname",
                        unit: "$unit",
                        stocklevel: "$stocklevel",
                        date_added: "$date_added",
                        productstatus: "$product.status",
                        // status: "$status",
                    }
                },
                {
                    $project:{
                        _id: 1,
                        pid: "$pid",
                        pname: "$pname",
                        wname: "$wname",
                        genericname: "$genericname",
                        unit: "$unit",
                        stocklevel: "$stocklevel",
                        date_added: "$date_added",
                        productstatus: "$product.status",
                        status: "$status",
                        quantity: "$others.total",
                        lastUpdateBy: "$others.entry.lastUpdateBy",
                        added_by: "$others.entry.addedBy",
                        lastUpdateDate: "$others.entry.lastUpdateDate",
                        date_added: "$others.entry.date_added"
                    }
                },
                {
                    $sort: { pid: -1 }
                }
            ], (err, getAllInventory)=>{
                if( err ) return cb({ sucess:false, message: err.message });
                
                if(getAllInventory.length){
                    return cb({ success:true, data:getAllInventory });
                }else{
                    return cb({ success:false, message: "No warehouse found!", toaster: 'off' });
                }
            });
        });

        //Medrep Inventory
        ///#/medrep/pricelist
        fn('getMedrepAllInventory', (data, cb) => {
            Product.aggregate([
                {
                    $match: {
                        status: 1
                    }
                },
                {
                    $lookup:
                    {
                        from: "warehouses",
                        localField: "*",
                        foreignField: "*",
                        as: "warehouses"
                    }
                },
                { $unwind: "$warehouses"},
                { $unwind : { path: "$warehouses", preserveNullAndEmptyArrays: true } },
                { $match: { "warehouses.status": 1 } },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "*",
                        foreignField: "*",
                        as: "lastUpdateBy"
                    }
                },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "*",
                        foreignField: "*",
                        as: "addedBy"
                    }
                },
                {
                    $project: {
                        _id: 1,
                        others: {
                            $let: {
                                vars: {
                                   theProduct: { 
                                    $arrayElemAt: [ "$warehouses.inventory",  {
                                        $indexOfArray: ["$warehouses.inventory.pid",  "$id" ]
                                    } ] 
                                   }
                                },
                                in: {
                                    $cond:
                                    {
                                        if: { $gte: [{ $indexOfArray: ["$warehouses.inventory.pid",  "$id" ]}, 0] },
                                        then: {
                                            total: "$$theProduct.total",
                                        },
                                        else: {
                                            total: 0
                                        }
                                    }
                                },
                             }
                        },
                        pname: "$name",
                        unit: "$unit",
                        stocklevel: "$stocklevel",
                        price: "$price",
                    }
                },
                {
                    $project:{
                        _id: 1,
                        pname: "$pname",
                        unit: "$unit",
                        stocklevel: "$stocklevel",
                        quantity: "$others.total",
                        price: "$price",
                    }
                }
            ], (err, getMedrepAllInventory)=>{
                console.log(getMedrepAllInventory);
                if( err ) return cb({ sucess:false, message: err.message });
                if(getMedrepAllInventory.length){
                    return cb({ success:true, data:getMedrepAllInventory });
                }else{
                    return cb({ success:false, message: "No products found!" });
                }
            });
        });


        //Sales Order
        fn('getAllProductsSO', (data, callback) => {
            Product.aggregate([
                { $match: { status: 1 } },
                {
                    $project: {
                        _id: 1,
                        id: "$id",
                        name: "$name",
                        price: "$price",
                        unit: "$unit",
                        quantity: "$quantity",
                        genericname: "$genericname",
                        brandname: "$brandname",
                        manufacturer: "$manufacturer",

                        distributor: "$distributor",
                        supplier_id: "$supplier_id",
                        barcode: "$barcode",
                        qrcode: "$qrcode",
                        description: "$description",
                        image: "$image",
                    }
                }
            ], (err, getAllProductsResult)=>{
                if( err ) return callback({ sucess:false, message: err.message });
                if(getAllProductsResult.length){
                    return callback({ success:true, data:getAllProductsResult });
                }else{
                    return callback({ success:false, message: "No products found!", toaster: 'off' });
                }
            });
        });
        

        //#/sales/sales-order/add-sales-order
        fn('getAllProductsWithPrice', (data, callback) => {
            Product.aggregate([
                {
                    $match: {
                        $and: [
                            { status: 1 },
                            { price: { $gt: 0 } },
                        ]
                    }
                },
                {
                    $project: {
                        _id: 1,
                        id: "$id",
                        name: "$name",
                        price: "$price",
                        unit: "$unit",
                        image: "$image",
                    }
                }
            ], (err, getAllProductsResult)=>{
                if( err ) return callback({ sucess:false, message: err.message });
                if(getAllProductsResult.length){
                    return callback({ success:true, data:getAllProductsResult });
                }else{
                    return callback({ success:false, message: "No products found!", toaster: 'off' });
                }
            });
        });

        /** UPDATE */
        fn('updateProductById', (data, cb) => {
            if( data.id ){
                Product.updateOne(
                    {
                        $and: [
                            { id: data.id },
                            { status: 1 },
                        ]
                    },
                    {
                        $set:{
                            name: data.name,
                            price: data.price || 0,
                            genericname: data.genericname,
                            brandname: data.brandname,
                            manufacturer : data.manufacturer,
                            unit : data.unit,
                            barcode : data.barcode,
                            sku : data.sku,
                            description : data.description,
                            image : data.image,
                            stocklevel: data.stocklevel,
                            lastUpdateBy: data.auth.id,
                            lastUpdateDate: new Date
                        }
                    },
                    (err, updateProductById) => {
                    if( err ) return cb({ success:false, message:err.message });
                    if(updateProductById.nModified){
                        // lib.createEmit('products', data.id);
                        lib.createEmit('products', data.id);
                        return cb({ success:true, message:updateProductById });
                    }else{
                        return cb({ success:false, message: "Unable to update product" });
                    }
                });
            }else{
                return cb({ success:false, message: "Product not found", toaster: 'off'  });
            }
        });

        fn('disableProduct', (data, cb) => {
            if( data.id ){
                Product.updateOne(
                    {
                        $and: [
                            { id: data.id },
                            { status: 1 },
                        ]
                    },
                    {
                        $set:{
                            status: 2,
                            lastUpdateBy: data.auth.id,
                            lastUpdateDate: new Date
                        }
                    },
                    (err, disableProduct) => {
                    if( err ) return cb({ success:false, message:err.message });
                    if(disableProduct.nModified){
                        // lib.createEmit('products');
                        lib.createEmit('products', data.id);
                        return cb({ success:true, message:disableProduct });
                    }else{
                        return cb({ success:false, message: "Unable to delete product" });
                    }
                });
            }else{
                return cb({ success:false, message: "Product not found", toaster: 'off'  });
            }
        });        

    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

    /**
     * @param {Object} data object and values to check
     * @return {Object} User object data
     */

    lib.createSocket( (client, io) => {
        return client.on('products', (data) => {
            console.log(`'products' emit received at`, __filename, `with data "${data}"`);
            io.emit('products', { success:true, data:data })
        });
    });

    // lib.createSocket( (client, io) => {
    //     return client.on('product', (data) => {
    //         console.log(`'product' emit received at`, __filename, `with data "${data}"`);
    //         io.emit('product', { success:true, data:data })
    //     });
    // });

})();