'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    Id = require('./id'),
    lib = require('../library'),

    /**
     * @see Function/method/class relied on
     * @link https://mongoosejs.com/docs/guide.html
     */
    YourModelName = module.exports = mongoose.model(base, mongoose.Schema({
        id:                     { type:Number, maxlength:7, required:true },
    }, { strict: true })),
    
    fn = lib.invocation().init(module.exports).fn;

        fn('YourFunctionName', async (data, callback) => {
            /** @description please note that {data} is an object */
            if( data === 'mycondition' ){

                /** @description if data passed your condition
                 *  be sure to generate a Unique ID for every create query
                 *  using the library function Id.generate(base)
                 * 
                 *  Also, it's important to return a callback;
                 */
                Id.generate(base, response => {
                    if( response.success){

                        /** @description if data passed your condition
                         *  inject response.id to your data
                         *  
                         *  Only use this function for create method / query
                         */
                        data.id = response.id;

                        YourModelName.create(data, (err, createResult) => {
                            if(err) return callback({ success:false, message:err.message });
                            return callback({
                                success:true,
                                message: 'success',
                                data: createResult
                            });
                        });

                    }else{
                        return callback({ success:false, message: 'Cannot generate user id' });
                    }
                });
            }else{
                return callback({ success:false, message: `incorrect ${data}` });
            }
        });
 
        fn('YourFunctionName2', async (data, callback) => {
            /** @description please note that {data} is an object */
            if( data === 'mycondition' ){
                return callback({ success:true, message: `correct ${data}` });
            }else{
                return callback({ success:false, message: `incorrect ${data}` });
            }
        });

    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

})();