'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    Id = require('./id'),
    lib = require('../library'),

    /**
     * @see Function/method/class relied on
     * @link https://mongoosejs.com/docs/guide.html
     */
    Animal = module.exports = mongoose.model(base, mongoose.Schema({
        id:                 { type:Number, maxlength:7, required:true },
        name:               { type:String, maxlength:100, required:true },
        
        date_added:         { type:Date, required:true, required:true },
        status:             { type:Number, maxlength:1, required:true, default:1 },
        added_by:           { type:Number, maxlength:7, required:true },
        lastUpdateBy:       { type:Number, maxlength:7, required:true },
        lastUpdateDate:     { type:Date, required:true },
    }, { strict: true })),
    
    fn = lib.invocation().init(module.exports).fn;
    
        fn('addAnimal', async (data, cb) => {
            data.date_added = new Date;
            data.status = 1;
            data.added_by = data.auth.id;
            data.lastUpdateBy = data.auth.id;
            data.lastUpdateDate = new Date;
            lib.create(data, (response) => {
                if(response.success){
                    lib.createEmit('animal'); 
                    return cb(response);
                }else{
                    return cb(response);
                }
            }, base, 1);
        });

    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

    lib.createSocket( (client, io) => {
        return client.on('animal', (data) => {
            console.log(`'animal' emit received at`, __filename, `with data "${data}"`);
            io.emit('animal', { success:true, data:data })
        });
    });

})();