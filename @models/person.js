'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    Notify = require('./notification'),
    lib = require('../library'),
    Id = require('./id'),

    Person = module.exports = mongoose.model(base, mongoose.Schema({
        id:                 { type:Number, maxlength:7, required:true },
        fname:              { type:String, maxlength:50, required:true },
        mname:              { type:String, maxlength:50, required:true },
        lname:              { type:String, maxlength:50, required:true },
        gender:             { type:String, maxlength:6, required:true },
        age:                { type:Number, maxlength:3, required:true },

        children: [
            {
                id:                 { type:Number, maxlength:7, required:true },
                fname:              { type:String, maxlength:50, required:true },
                mname:              { type:String, maxlength:50, required:true },
                lname:              { type:String, maxlength:50, required:true },
            }
        ],

        date_added:         { type:Date, required:true, required:true },
        status:             { type:Number, maxlength:1, required:true, default:1 },
        added_by:           { type:Number, maxlength:7, required:true },
        lastUpdateBy:       { type:Number, maxlength:7, required:true },
        lastUpdateDate:     { type:Date, required:true },
    }, { strict: true })),

    fn = lib.invocation().init(module.exports).fn;

        fn('getAllPeople', (data, cb) => {
            Person.aggregate([
                { $match: { status: 1 } },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "added_by",
                        foreignField: "id",
                        as: "addedBy"
                    }
                },
                {
                    $lookup:
                    {
                        from: "users",
                        localField: "lastUpdateBy",
                        foreignField: "id",
                        as: "lastUpdateBy"
                    }
                },
                { $unwind: "$addedBy" },
                { $unwind: "$lastUpdateBy" },
                {
                    $project: {
                        _id: 1,
                        id: "$id",
                        fname: "$fname",
                        mname: "$mname",
                        lname: "$lname",
                        age: "$age",
                        gender: "$gender",

                        date_added: "$date_added",
                        status: "$status",
                        added_by: { $concat: [
                            { $ifNull: [ "$addedBy.lname", "" ] }, ", ",
                            { $ifNull: [ "$addedBy.fname", "" ] }, " ",
                            { $ifNull: [ "$addedBy.mname", "" ] } ]
                        },
                        lastUpdateDate: "$lastUpdateDate",
                        lastUpdateBy: { $concat: [
                            { $ifNull: [ "$lastUpdateBy.lname", "" ] }, ", ",
                            { $ifNull: [ "$lastUpdateBy.fname", "" ] }, " ",
                            { $ifNull: [ "$lastUpdateBy.mname", "" ] } ]
                        },
                    }
                }
            ], (err, getAllPeople)=>{
                if( err ) return cb({ sucess:false, message: err.message });
                if(getAllPeople.length){
                    return cb({ success:true, data:getAllPeople });
                }else{
                    return cb({ success:false, message: "No products found!" });
                }
            });
        });

        fn('getAllPeopleNameId', (data, cb) => {
            Person.aggregate([
                { $match: { status: 1 } },
                {
                    $project: {
                        _id: 1,
                        id: "$id",
                        name: { $concat: [
                            { $ifNull: [ "$lname", "" ] }, ", ",
                            { $ifNull: [ "$fname", "" ] }, " ",
                            { $ifNull: [ "$mname", "" ] } ]
                        },
                    }
                }
            ], (err, getAllPeopleNameId)=>{
                if( err ) return cb({ sucess:false, message: err.message });
                if(getAllPeopleNameId.length){
                    return cb({ success:true, data:getAllPeopleNameId });
                }else{
                    return cb({ success:false, message: "No people found!" });
                }
            });
        });
        

        fn('getOnePersonChildren', (data, cb) => {
            Person.aggregate([
                { $match: { status: 1, id:data.id } },
                {
                    $project: {
                        _id: 1,
                        children: "$children"
                    }
                }
            ], (err, getOnePersonChildren)=>{
                if( err ) return cb({ sucess:false, message: err.message });
                if(getOnePersonChildren.length){
                    return cb({ success:true, data:getOnePersonChildren[0] });
                }else{
                    return cb({ success:false, message: "No children found!" });
                }
            });
        });
        
        fn('addChild', (data, cb) => {
            Id.generate('person-children', async (response) => {
                if( data.id ){
                    let child = {
                        id: response.id,
                        fname: data.fname,
                        mname: data.mname,
                        lname: data.lname,
                    }

                    Person.updateOne(
                        {
                            $and: [
                                { id: data.id },
                                { status: 1 },
                            ]
                        },
                        {
                            $push:{
                                children: child
                            }
                        },
                        (err, addChild) => {
                        if( err ) return cb({ success:false, message:err.message });
                        if(addChild.nModified){
                            lib.createEmit('personchildren', { id:data.id, child:child });
                            return cb({ success:true, message: "Child successfully updated!" });
                        }else{
                            return cb({ success:false, message: "Unable to update person's children" });
                        }
                    });
                }else{
                    return cb({ success:false, message: "Person ID not found" });
                }
            }, 800);
        });

        fn('UpdateChild', (data, cb) => {
            console.log(data);
            if( data.id ){
                Person.updateOne(
                    {
                        $and: [
                            { id: data.id },
                            { "children.id": data.child.id },
                        ]
                    },
                    {
                        $set:{
                            "children.$": data.child
                        }
                    },
                    (err, UpdateChild) => {
                        console.log(UpdateChild)
                    if( err ) return cb({ success:false, message:err.message });
                    if(UpdateChild.nModified){
                        return cb({ success:true, message: `Child (${data.child.id}) successfully updated!` });
                    }else{
                        return cb({ success:false, message: "Unable to update the child" });
                    }
                });
            }else{
                return cb({ success:false, message: "Child ID not found" });
            }
        });

        fn('createperson', (data, cb) => {
            data.date_added = new Date;
            data.status = 1;
            data.added_by = data.auth.id;
            data.lastUpdateBy = data.auth.id;
            data.lastUpdateDate = new Date;
            lib.create(data, (response) => {
                if(response.success){
                        lib.createEmit('person');
                        Notify.newNotification({
                            message: `New Person #${response.data.id} has been added.`,
                            role_module: '',
                            document_id: response.data.id,
                            from: data.auth.role,
                            type: "notify",
                            url: "/demo/sgs-crud",
                            for: ['admin'],
                            added_by: data.auth.id,
                        }, () => {}); 
                    return cb(response);
                }else{
                    return cb(response);
                }
            }, base, 800);
        });

        fn('updatePerson', (data, cb) => {
            if( data.id ){
                Person.updateOne(
                    {
                        $and: [
                            { id: data.id },
                            { status: 1 },
                        ]
                    },
                    {
                        $set:{
                            fname: data.fname,
                            mname: data.mname,
                            lname: data.lname,
                            age: data.age,
                            gender: data.gender,
                            
                            lastUpdateBy: data.auth.id,
                            lastUpdateDate: new Date
                        }
                    },
                    (err, updatePerson) => {
                    if( err ) return cb({ success:false, message:err.message });
                    if(updatePerson.nModified){
                        lib.createEmit('person', data.id);
                        return cb({ success:true, message: "Person successfully updated!" });
                    }else{
                        return cb({ success:false, message: "Unable to update person" });
                    }
                });
            }else{
                return cb({ success:false, message: "Person ID not found" });
            }
        });

        fn('updatePersonStatus', (data, cb) => {
            if( data.id ){
                Person.updateOne(
                    {
                        $and: [
                            { id: data.id },
                            { status: 1 },
                        ]
                    },
                    {
                        $set:{
                            status: 0,
                            
                            lastUpdateBy: data.auth.id,
                            lastUpdateDate: new Date
                        }
                    },
                    (err, updatePerson) => {
                    if( err ) return cb({ success:false, message:err.message });
                    if(updatePerson.nModified){
                        lib.createEmit('person', data.id);
                        return cb({ success:true, message: "Person successfully updated!" });
                    }else{
                        return cb({ success:false, message: "Unable to update person" });
                    }
                });
            }else{
                return cb({ success:false, message: "Person ID not found" });
            }
        });
        

    lib.createSocket( (client, io) => {
        return client.on('person', (data) => {
            console.log(`'person' emit received at`, __filename, `with data "${data}"`);
            io.emit('person', { success:true, data:data })
        });
    });
    
    lib.createSocket( (client, io) => {
        return client.on('personchildren', (data) => {
            console.log(`'personchildren' emit received at`, __filename, `with data "${data}"`);
            io.emit('personchildren', { success:true, data:data })
        });
    });
    

})();