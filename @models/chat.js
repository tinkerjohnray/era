'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    lib = require('../library'),
    ObjectId = mongoose.Types.ObjectId,

    // @ts-ignore
    Chat = module.exports = mongoose.model(base, mongoose.Schema({
        id:                     { type: Number, maxlength:7, required:true },
        sender:                 { type: Number },
        reciever:               { type: Number },
        convoType:              { type: Number, default: 1},
        convoName:              { type: String, default: '' },
        group:      [
            {
                memberID:               { type: Number },
                lastSeenMessageDate:    { type: Date, default: Date.now()}
            }
        ],
        groupDescription:      { type: String },
        messages:   [
            {
                sender:         { type: Number },
                message:        { type: String },
                date:           { type: Date },
                status:         { type: Number, default: 1}
            }
        ],
        dateCreated:            { type: Date, default: Date.now()},
        status:                 { type: Boolean, default: true},
        settings: {
            sound: { type: Boolean, default: true }
        }
    }, { strict: true })),

    fn = lib.invocation().init(module.exports).fn;

     /**
     * @description;
     * ConvoType:
        1 - one is to one convo
        2 - group convo 
     */

    fn('checkThreadIfEsist', async (data, cb) => {
        Chat.findOne(
            {   
                $or:[
                    {$and:[           
                         {sender: +data.auth.id},  
                         {reciever: +data.id} 
                    ]},
                    {$and:[
                        {reciever: +data.auth.id},  
                        {sender: +data.id} 
                   ]},
                ]
            }, (err, res) => {
                if( err ){return cb({ sucess:false, message: err.message });}
                if(res){
                    return cb({ success:true, data: res });
                }
                else{
                    return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
                }
        });
    });

    fn('getMessages', (data, cb) => {
        Chat.aggregate([
            {
                $match: {
                    $and: [
                        { 
                            'reciever':  { $in: [+data.auth.id, +data.id]}
                        },
                        { 
                            'sender':  { $in: [+data.auth.id, +data.id]}
                        }
                    ]
                }
            },
            {
                $unwind: "$messages"
            },
            { $sort: { "messages.date": -1 }},
            { $skip: data.skip},  //0 + data.limit on another load
            { $limit: 10}, //10 + data.limit on another load
            {
                $lookup: {
                    from: "users",
                    localField: "messages.sender",
                    foreignField: "id",
                    as: "sender"
                }
            },
            {
                $unwind: "$sender"
            },
            {
                $project: {
                    _id: 1,
                    senderID: "$sender.id",
                    sender: { $concat: [ "$sender.fname", ' ', "$sender.lname" ] },
                    message: "$messages.message",
                    date: "$messages.date",
                }
            },
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res });
            }
        });
    });

    fn('getGroupMessages', (data, cb) => {
        Chat.aggregate([
            {
                $match: {
                    _id: ObjectId(data.id)
                }
            },
            {
                $unwind: "$messages"
            },
            { $sort: { "messages.date": -1 }},
            { $skip: data.skip},  //0 + data.limit on another load
            { $limit: 10}, //10 + data.limit on another load
            {
                $lookup: {
                    from: "users",
                    localField: "messages.sender",
                    foreignField: "id",
                    as: "sender"
                }
            },
            {
                $unwind: "$sender"
            },
            {
                $project: {
                    _id: 1,
                    senderID: "$sender.id",
                    sender: { $concat: [ "$sender.fname", ' ', "$sender.lname" ] },
                    message: "$messages.message",
                    date: "$messages.date",
                }
            },
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res });
            }
        });
    });

    fn('getGroupInfo', (data, cb) => {
        Chat.aggregate([
            {$match: {
                _id: ObjectId(data.id)
            }},{
                $project: {
                    _id: 1,
                    convoName: "$convoName",                    
                    group: "$group",
                }
            }
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res[0] });
            }
        });
    });

    fn('getGroupThread', (data, cb) => {
        Chat.aggregate([
            {
                $match: {convoType: 2}
            },
            {
                $unwind: "$group"
            },
            {
                $match: {
                    'group.memberID': +data.auth.id
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "group.memberID",
                    foreignField: 'id',
                    as: 'user'
                }
            },
            {
                $unwind: "$user"
            },
            {
                $project: {
                    lname: "$convoName",
                    convoType: "$convoType",
                    uid: "$user.id"
                 }
            }
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res });
            }
        });
    });

    fn('getThreadMembers', (data, cb) => {
        Chat.aggregate([
            {
                $match: {_id: ObjectId(data.threadID)}
            },
            {
                $unwind: "$group"
            },
            {
                $lookup: {
                    from: "users",
                    localField: "group.memberID",
                    foreignField: 'id',
                    as: 'user'
                }
            },
            {
                $unwind: "$user"
            },
            {
                $project: {
                    _id: 1,
                    memberID: "$group.memberID", 
                    sender: { $concat: [ "$user.fname", ' ', "$user.lname" ] }
                }
            }
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res });
            }
        });
    });
    
    fn('createMessageThread', (data, cb) => {
        data.sender = data.auth.id;
        data.reciever = data.reciever;
        data.messages = data.data;
        data.group = data.group;
        lib.create(data, (response) => {
            if (response.success) {
                return cb(response);
            } else {
                return cb(response);
            }
        }, base, 1);
    }); 

    fn('sendMessageUpdate', (data, cb) => {
        Chat.findOne({_id: ObjectId(data.threadID)}, (err, res) => {
            if(err){
                return cb({ success: false, message: err.message });
            }else{
                let obj = {
                    sender: data.auth.id,
                    senderID: data.auth.id,
                    message: data.message,
                    date: new Date(),
                    reciever: data.reciever,
                    convoType: data.convoType,
                    threadID: data.threadID,
                    senderName: data.auth.fname+ " " + data.auth.lname,
                }
                res.messages.push(obj);
                lib.createEmit('chat', obj);
            }
            res.save(cb({success: true, data: res}))
        });
    });

    fn('createMessageGroupThread', (data, cb) => {
        let newMessage = {
            message: 'F!R3W@!!404',
            date: new Date,
            sender: data.auth.id
        }
        data.sender = data.auth.id;
        data.convoType = 2;
        data.convoName = data.name;
        data.group = data.members;
        data.messages = newMessage;
        lib.create(data, (response) => {
            if (response.success) {
                lib.createEmit('newGroupCreated', data);
                return cb(response);
            } else {
                return cb(response);
            }
        }, base, 1);
    });

    fn('updateGroupMembers', (data, cb) => {
        Chat.updateOne(
            {
                _id: ObjectId(data.tid)
            },
            {
                "$set": {
                    "convoName": data.convoName,
                    "group": data.group
                }
            },
            (err, res) => {
                if( err ) return cb({ success:false, message:err.message });
                if(res.nModified){
                    // lib.createEmit('pcbFundRequest');
                    return cb({ success:true, data: data.form });
                }else{
                    return cb({ success:false, message: "No changes found on the request." });
                }
        });
    });
    
    fn('countUnreadGroupMessages', (data, cb) => {
        Chat.aggregate([
            {
                $match: {_id: ObjectId(data.id)}
            },
            {
                $unwind: "$group"
            },
            {
                $match: {"group.memberID": data.uid}
            },
            {
                $unwind: "$messages"
            },
            {
                //exclude self from counter
                $match: {
                    "messages.sender": {$ne: data.auth.id}
                }
            },
            { $sort: { "messages.date": -1 }},
            { $skip: 0},  //0 + data.limit on another load
            { $limit: 11}, //10 + data.limit on another load
            {
                $project: {
                    lastSeenMessageDate: "$group.lastSeenMessageDate",
                    messagesDate: "$messages.date" 
                }
            }
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res });
            }
        });
    });

    fn('countUnreadMessages', (data, cb) => {
        Chat.aggregate([
            {
                $match: {
                    $and: [
                        { 
                            'reciever':  { $in: [+data.auth.id, +data.id]}
                        },
                        { 
                            'sender':  { $in: [+data.auth.id, +data.id]}
                        }
                    ]
                }
            },
            {
                $unwind: "$group"
            },
            {
                $match: {"group.memberID": data.auth.id}
            },
            {
                $unwind: "$messages"
            },
            {
                //exclude self from counter
                $match: {
                    "messages.sender": {$ne: data.auth.id}
                }
            },
            { $sort: { "messages.date": -1 }},
            { $skip: 0},  //0 + data.limit on another load
            { $limit: 11}, //10 + data.limit on another load
            {
                $project: {
                    lastSeenMessageDate: "$group.lastSeenMessageDate",
                    messagesDate: "$messages.date" 
                }
            }
        ], (err, res) => {
            if( err ){return cb({ sucess:false, message: err.message });}
            if(res.length == 0){
                return cb({ success:false, message: 'No Data Found!', toaster: 'off'});
            }
            else{
                return cb({ success:true, data: res });
            }
        });
    });

    fn('updateLastSeen', (data, cb) => {
        Chat.updateOne(
            {
                _id: ObjectId(data.id),
                group: { $elemMatch: { memberID: +data.auth.id}},
            },
            {
                $set: {
                    "group.$.lastSeenMessageDate": new Date(),
                }
            },
        (err, findResult)=>{
            if( err ) return cb({ success:false, message:err.message });
            if(findResult.nModified){
                return cb({ success:true, data: data.form });
            }else{
                return cb({ success:false, message: 'No changes found', toaster: 'off' });
            }
        });
    });


    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

    /**
     * @param {Object} data object and values to check
     * @return {Object} User object data
     */

    
    lib.createSocket( (client, io) => {
        return client.on('chat', (data) => {
            io.emit('chat', { success:true, data:data}) 
        });
    });

    lib.createSocket( (client, io) => {
        return client.on('newGroupCreated', (data) => {
            io.emit('newGroupCreated', { success:true, data:data}) 
        });
    });


})();
