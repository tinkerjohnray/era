'use strict';

(async function init() {
    module.exports = (express, app) => {
        const   cors = require('cors'),
        bodyParser = require('body-parser'),
        path = require('path'),
        livePath = path.join(__dirname, '../app/dist'),
        uploads = path.join(__dirname, '../uploads/images'),
        receiptsUpload = path.join(__dirname, '../uploads/receipts'),
        avatarsUpload = path.join(__dirname, '../uploads/avatars'),
        brochuresUpload = path.join(__dirname, '../uploads/brochures'),
        appupdates = path.join(__dirname, '../updates'),
        images_hmed_attendance = path.join(__dirname, '../uploads/images_hmed_attendance'),
        imagegallery = path.join(__dirname, '../uploads/image-gallery'),
        maintenancePath = path.join(__dirname, '../app/maintenance');

        return (() => {
            app.use(cors());
            app.use(express.static(livePath));
            app.use('/images', express.static(uploads));
            app.use('/receipts', express.static(receiptsUpload));
            app.use('/avatars/', express.static(avatarsUpload));
            app.use('/brochures/', express.static(brochuresUpload));
            app.use('/updates', express.static(appupdates));
            app.use('/images_hmed_attendance/', express.static(images_hmed_attendance));
            app.use('/image-gallery/', express.static(imagegallery));
            app.use(bodyParser.json({limit: '4mb'}));
            app.use(bodyParser.urlencoded({limit:'4mb', extended: false}));
            app.use((err, req, res, next) => {
                if(err) res.json({ success:false, message: err.message });
                next(); 
            }); 
            console.log('Options Initialized');
        });
    }
})();

                
