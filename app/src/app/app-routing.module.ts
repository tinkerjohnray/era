import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'login',        loadChildren: 'app/login/login.module#LoginModule' },
  { path: 'admin',        loadChildren: 'app/admin/admin.module#AdminModule', canActivate: [AuthGuard] },
  { path: '',         redirectTo: 'login', pathMatch: 'full' },
  { path: '**',       redirectTo: 'login' },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
