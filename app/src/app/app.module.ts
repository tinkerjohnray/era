/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF, DatePipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from './@core/services/auth.service';
import { UserGlobalService } from './@core/services/user.global.service';
import { SharedGlobalService, GlobalModal } from './@core/services/shared.global.service';
import { ConnectionService } from './@core/services/connection.service';
import { ChatComponent } from './@theme/components/header/chat/chat.component';

import { HttpModule } from '@angular/http';

import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/notAuth.guard';

// MY MODULES
import { AdminModule } from './admin/admin.module';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  declarations: [
    AppComponent, 
    GlobalModal,
    ChatComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    
    ToasterModule.forRoot(),
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    AdminModule,
  ],
  entryComponents:[
    GlobalModal,
    ChatComponent,
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    AuthGuard,
    NotAuthGuard,
    AuthService,
    UserGlobalService,
    SharedGlobalService,
    ConnectionService,
    DatePipe,
  ],
})
export class AppModule {
}

