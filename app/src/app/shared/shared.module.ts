import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { dataFilterPipe, NumberWithCommas, IsRead, SumPipe, DateScore, SafePipe, multiFilterPipe, ImageGallery } from '../@core/pipes/dataFilter';
import { TruncatePipe, TruncateTextPipe } from '../@core/pipes/truncate';
import { DataTableModule } from 'angular2-datatable';
import { NbStepperModule, NbSpinnerModule, NbListModule, NbCalendarModule,  NbAccordionModule } from '@nebular/theme';
import { MomentModule } from 'ngx-moment';
// import { MomentModule } from 'angular2-moment';
import { QRCodeModule } from 'angularx-qrcode';

// from valor components
import { TooltipModule, BsDatepickerModule, TimepickerModule  } from 'ngx-bootstrap';
// import { TruncatePipesModule } from 'angular-truncate-pipes';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

import { NgxEchartsModule } from 'ngx-echarts';
import { AccordionModule } from 'ngx-bootstrap';

//inifite scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PrintOrderTemplate } from './printing/print-order-template/print-order-template';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    NbStepperModule,
    NbSpinnerModule,
    NbCalendarModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    // TruncatePipesModule,
    TypeaheadModule.forRoot(),

    NgxEchartsModule,

    InfiniteScrollModule
  ],
  declarations: [
    dataFilterPipe,
    NumberWithCommas,
    IsRead,
    TruncateTextPipe,
    TruncatePipe,
    SumPipe,
    DateScore,
    SafePipe,
    multiFilterPipe,
    ImageGallery,
    PrintOrderTemplate,
  ],
  entryComponents: [],
  exports: [
    dataFilterPipe,
    NumberWithCommas,
    IsRead,
    SumPipe,
    DateScore,
    SafePipe,
    multiFilterPipe,
    ImageGallery,
    PrintOrderTemplate,
    TruncateTextPipe,
    TruncatePipe,
    DataTableModule,
    NbStepperModule,
    NbSpinnerModule,
    BsDatepickerModule,
    TimepickerModule,
    NbListModule,
    MomentModule,
    NbAccordionModule,
    TooltipModule,
    TypeaheadModule,
    AccordionModule,
    NgxEchartsModule,
    InfiniteScrollModule
  ],
  providers: []
})
export class SharedModule {}
