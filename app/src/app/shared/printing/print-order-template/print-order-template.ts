import { Component, OnInit, Input } from '@angular/core';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'print-order-template',
  templateUrl: './print-order-template.html',
  styleUrls: ['./print-order-template.scss']
})
export class PrintOrderTemplate implements OnInit {

  @Input() id;
  @Input() title;
  data: any = []; 

  address = {
   "address1": "76-6 MM Building, B.S Aquino Drive, Bacolod City", 
   "address2": "MARVIN Q. HERMOSURA - Proprietor",
   "address3": "VAT REG No. 936-595-907-0000",
   "address4": "Telefax (034) 707-0844"
  }

  constructor(
    public sgs: SharedGlobalService,
    private route: ActivatedRoute,
  ) { }
  
  ngOnInit() {
    this.sgs.loading = false;
    this.getOneOrderForPrint(this.id);
  }

  getOneOrderForPrint(id){
    switch(this.title){
      case "SALES ORDER":
        this.sgs.request('get', 'salesorder/getOneSalesOrderForPrint', { id:id, title:this.title }, (res) => {
          if(res.success){
            this.data = res.data;
            setTimeout(() => {
              this.sgs.printerEmitter.emit({ success:true });
            }, 20);
          }else{
            setTimeout(() => {
              this.sgs.printable.ready = false;
              this.sgs.printerEmitter.emit({ success:false });
            }, 20);
          }
        }, { socketLoader:false }); break;
      
      case "PURCHASE ORDER":
        this.sgs.request('get', 'purchaseorder/getOnePurchaseOrderForPrint', { id:id, title:this.title }, (res) => {
          if(res.success){
            this.data = res.data;
            setTimeout(() => {
              this.sgs.printerEmitter.emit({ success:true });
            }, 20);
          }else{
            setTimeout(() => {
              this.sgs.printable.ready = false;
              this.sgs.printerEmitter.emit({ success:false });
            }, 20);
          }
        }, { socketLoader:false }); break;
    }
  }

}
