import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animation';
import { AuthService } from '../@core/services/auth.service';
import { SharedGlobalService } from '../@core/services/shared.global.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    username: String;
    password: String;
    msg: String;
    loader = false;
    disableSubmit;
    conn;
    userID;

    constructor(
        public sgs: SharedGlobalService,
        public authService: AuthService,
        public router: Router
    ) {
        sgs.setBrowserTitle.emit('Login');
    }

    ngAfterViewInit(){
        // this.ugs.Toaster('success', 'Welcome!');
    }


    ngOnInit() {
        // this.ugs.Toaster('success', 'Welcome!');
    }

    Submit() {
        const user = {
          username: this.username,
          password: this.password,
        };
        
        this.sgs.request('post', 'user/authenticate', user, async (response) => {
            if( response.success ){
                if( await this.authService.setToken(response.data) ){
                    this.authService.login();
                    // this.changeStatus(true, response.data.user.id);
                }else{
                    this.msg = 'LocalStorage not supported. Token not set!';
                }
            }else{
                this.msg = response.message;
            }
        });
    }

    // changeStatus(status, id){
    //     this.sgs.request('put', 'user/changeStatus', {status: status, id: id}, async (res) => {});
    // }
}
