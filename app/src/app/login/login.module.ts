import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.modules';
import { LoginComponent } from './login.component';
import { ThemeModule } from '../@theme/theme.module';
import { NbSpinnerModule } from '@nebular/theme';

@NgModule({
    imports: [
        CommonModule, 
        ThemeModule, 
        LoginRoutingModule,
        NbSpinnerModule
    ],
    declarations: [LoginComponent],
})
export class LoginModule {}
