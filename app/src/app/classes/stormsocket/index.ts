import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Rx';

export class StormSocket{
    private col;
    private observable: any;
    private socket: any;
    constructor(data = null){
        this.connect(data['ip']);
        this.col = data['collection'];
        this.collection(data['collection']);
       // return this.observable;
    }

    public end(){
        // console.log('ended');
        //this.observable.unsubscribe();
    }

    // public subscribe(callback){
    //     return callback()
    // }

    protected collection(name){
        // this.socket.emit(name, {
        //     isCollection: name
        // });
        // this._on(name).subscribe(e => {
        //     console.log(e);
        // });
        this._on(name);
    }

    protected _emit(name) {
        this.socket.emit(name, name);
    }

    protected _on(name) {
       this.observable = new Observable(observer => {
            this.socket.on(name, (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        });
        return this.observable;
    }

    public read(){
        return this.observable;
    }

    public create(data){
        this.socket.emit(this.col, {
            function: 'CREATE',
            content: data
        });
    }

    protected connect(ip){
        this.socket = io.connect(ip);
    }
}
