import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';
import { FillComponent } from './modal/fill/fill.component';

@Component({
  selector: 'ngx-sss',
  templateUrl: './sss.component.html',
  styleUrls: ['./sss.component.scss']
})
export class SssComponent implements OnInit {
public  name: any;


  constructor(
    public sgs: SharedGlobalService
  ) { }

  ngOnInit() {
  }
  sssClick(data) {
    const viewData = data;
    this.sgs.Modal(
      { viewData },
      {
        component: FillComponent,
        size: 'lg',
      });
  }

  addForm(f){
    const Forms = {
      name: this.name
    }
  console.log(Forms)
    this.sgs.request('post', 'benefit/addForm', Forms, response => { console.log(response)
      console.log(response)
      if(response.success){
        this.sgs.goto('/admin/sss');
        this.sgs.Toaster('success', 'Success', 'New Card has been added!');
      }
    });
  }

}
