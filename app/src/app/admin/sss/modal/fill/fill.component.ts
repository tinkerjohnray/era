import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';

@Component({
  selector: 'ngx-fill',
  templateUrl: './fill.component.html',
  styleUrls: ['./fill.component.scss']
})
export class FillComponent implements OnInit {
public fname: any;
public lname: any;
public mname: any;
public age: Number;
public basic: any = {};
public address: any;
public company: any;
public Forms: any = {}

  constructor(
    public sgs: SharedGlobalService, 
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }
  closeModal() {
    this.activeModal.close();
  }
  submitForm(f){
    const Forms = {
      fname: this.fname,
      lname: this.lname,
      mname: this.mname,
      sex: this.Forms.sex,
      basic: {
        age: this.age,
        address: this.address,
        company: this.company,
      

      }
    }

    console.log(Forms);
    
    this.sgs.request('post', 'sss/createForm', Forms, response => {
      console.log(response)
      if(response.success){
        this.closeModal();
        this.sgs.goto('/admin/applicants');
        this.sgs.Toaster('success', 'Success', 'New applicant has been added!');
      }
    });
  }

  selectSex() {
    this.sgs.Modal({
      header: `Select Sex`,
      content: {
        type: 'array-object',
        data: [
          { id: 1, name: 'Male' },
          { id: 2, name: 'Female' },
        ],
        display: [{ property: 'name', nicename: "Sex" }],
        return: ['name'],
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

      .selected.subscribe(response => {
        if (response.success) {
          this.Forms.sex = response.data[0];
        }
      });
  }
}
