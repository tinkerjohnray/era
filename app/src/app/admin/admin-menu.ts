import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'Mobile Brands',
    icon: 'nb-home',
    children: [
  // { title: 'Create Brand',
  //   link: '/admin/mobile-brands/create-brand'
  // },
  { title: 'Brands List',
  link: '/admin/mobile-brands/brands-list'
  }
    ]
  },
  {
    title: 'Demo',
    icon: 'lnr lnr-laptop-phone',
    link: '/admin/demo',
    children: [
      {
        title: 'SGS CRUD',
        link: '/admin/demo/sgs-crud',
      },
      {
        title: 'SGS Array Update',
        link: '/admin/demo/sgs-array-update',
      },
      {
        title: 'angular2-datatable',
        link: '/admin/demo/dt',
      },
      {
        title: 'angular2-datatable (multifilter support)',
        link: '/admin/demo/dtmultifilter',
      },
      {
        title: 'Image Gallery',
        link: '/admin/demo/image-gallery',
      },
      {
        title: 'Select Modal (single)',
        link: '/admin/demo/select-modal-single',
      },
      {
        title: 'Select Modal (multiple)',
        link: '/admin/demo/select-modal-multiple',
      },
      {
        title: 'Modal (basic)',
        link: '/admin/demo/modals/multiple',
      },
      {
        title: 'Modal (as component)',
        link: '/admin/demo/modals/component',
      },
      {
        title: 'Modalb (confirmation)',
        link: '/admin/demo/modals/confirmation',
      },
    ]
  },
  {
    title: 'Users',
    icon: 'lnr lnr-users',
    link: '/admin/users',
    home: true,
  },
  {
    title: 'Boards',
    home: true,
    children: [
      { 
        title: 'Boards List',
     icon: 'lnr lnr-users',
     link: '/admin/boards',
   },
    { 
       title: 'SSS Applicants',
      icon: 'lnr lnr-users',
      link: '/admin/applicants',
    },
   
    ]
  },
  {
    title: 'Selected',
    icon: 'lnr lnr-users',
    link: '/admin/selected',
    home: true,
  },
  {
    title: 'Clients',
    icon: 'lnr lnr-users',
    children: [
      {
        title: 'Clients List',
        icon: 'lnr lnr-users',
        link: '/admin/client/clients-list',
        home: true,
      },
    ]
  },

  
];
