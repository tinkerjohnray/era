import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';
import { ViewBrandComponent } from './modal/view-brand/view-brand.component';
import { EditBrandComponent } from './modal/edit-brand/edit-brand.component';
import { CreatebraComponent } from './modal/createbra/createbra.component';




@Component({
  selector: 'ngx-brands-list',
  templateUrl: './brands-list.component.html',
  styleUrls: ['./brands-list.component.scss']
})
export class BrandsListComponent implements OnInit {
  data: any;
  selectQueryString = 'Brand';
  selectQuery = 'brand';
  filterQuery = '';
  sortBy = 'id -1';
  sortOrder = 'asc';

  constructor(
    public sgs: SharedGlobalService
  ) { }

  ngOnInit() {
    this.getMobile();
  }


  getMobile() {
    this.sgs.request('get', 'brand/getMobile', '', response => {
      if (response.success) {
        this.data = response.data;

      }

    })
  }

  view(data) {
    const viewData = data;
    this.sgs.Modal(
      { viewData },
      {
        component: ViewBrandComponent,
        size: 'md',
      });
  }

  
  onBrandSubmit(data) {
    const createData = data;
    this.sgs.Modal(
      { createData },
      {
        component: CreatebraComponent,
        size: 'md',
      });
  }

  onEdit(data) {
    const editData = data;
    this.sgs.Modal(
      { editData },
      {
        component: EditBrandComponent,
        size: 'md',
      });
  }
  deleteBtn(data) { 
      this.sgs.request('put', 'brand/deleteBtn', data, response => {
        if (response.success) {
          this.sgs.Toaster('danger', `${data.brand}`, ` has been deleted!`);
          this.getMobile();
        } 
      })
  }

  selectFilter(name, value){
    this.selectQuery = name;
    this.selectQueryString = value;
  }
}



