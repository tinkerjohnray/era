import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'ngx-edit-brand',
  templateUrl: './edit-brand.component.html',
  styleUrls: ['./edit-brand.component.scss']
})
export class EditBrandComponent implements OnInit {
  editData: any = {
    specs: {},
    model: []
  };
  brand: String;
  operatingsystem: String;
  processor: String;
  chipset: String;
  touch: Boolean = false;
  model: any = [];
  url: any;

  data: any;
  modelName: any;
  modelId: any;
  constructor(
    public sgs: SharedGlobalService,
    public activeModal: NgbActiveModal



  ) { }

  ngOnInit() {
    console.log(this.editData)
    this.getMobile();

  }


  getMobile() {
    this.sgs.request('get', 'brand/getMobile', '', response => {
      if (response.success) {
        this.data = response.data;

      }

    })
  }

  edited() {
    const brand = {
      id: this.editData.id,
      brand: this.editData.brand,
      model: this.editData.model,
      specs: {
        operatingsystem: this.editData.specs.operatingsystem,
        processor: this.editData.specs.processor,
        chipset: this.editData.specs.chipset,
        touch: this.editData.specs.touch,
      },
    }
    this.sgs.request('put', 'brand/edited', brand, response => {
      if (response.success || !response.untouched) {
        this.sgs.Toaster('success', `${brand.brand} has been`, 'edited Successfully!');
        this.closeModal();
      } else {

      }
    })
  }

  addModel(data) {
    let same = this.editData.model.map(e => e.name === data);
    if (!same.includes(true)) {
      this.editData.model.push({ name: data })
      this.model = '';
    } else {
      this.sgs.Toaster('', 'Error',
        `${data} already in list!`);
      this.model = '';
    }
  }




  deleteModel(name) {
    let index = this.editData.model.findIndex(e => e.name === name);
    if (index > -1) {
      this.editData.model.splice(index, 1);
    }
  }
  closeModal() {
    this.activeModal.close();
  }
}
