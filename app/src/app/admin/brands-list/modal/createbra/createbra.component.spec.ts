import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatebraComponent } from './createbra.component';

describe('CreatebraComponent', () => {
  let component: CreatebraComponent;
  let fixture: ComponentFixture<CreatebraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatebraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatebraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
