import { Component } from '@angular/core';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';

@Component({
  selector: 'tinker-sgs-array-update-component',
  templateUrl: './sgs-array-update.component.html',
  styleUrls: ['./sgs-array-update.component.scss']
})
export class SgsArrayUpdate {

  public Child: any;
  public selectedPerson: any = { name: '', children:[] };
  public socketInstancePersonChildren: any;

  constructor(
    public sgs: SharedGlobalService
  ){
      



    this.initChild();
    this.SocketPersonChildren();
  }

  initChild(){
    this.Child = {
      fname: '',
      mname: '',
      lname: '',
    }
  }

  SocketPersonChildren(){
    this.socketInstancePersonChildren = this.sgs.ResponseSocket('personchildren').subscribe(emitted => {
      console.log(emitted);
      console.log(this.selectedPerson);

      this.selectedPerson.id === emitted.data.id && this.selectedPerson.children.push(emitted.data.child);
    });
  }

  Submit(){
    this.sgs.request('put', 'person/addChild', { ...this.Child, id:this.selectedPerson.id }, res => {
      if(res.success){
        this.sgs.Toaster('success', 'Success', res.message)
      }
    }, { });
  }

  SelectPerson(){

    this.sgs.request('get', 'person/getAllPeopleNameId', {}, res => {
      if(res.success){
        console.log(res.data);
        selector(res.data);
      }else{
        selector([]);
      }
    }, { selectorLoader:true });
    
    let selector = (data) => {
      this.sgs.Modal({
        header: `Select Person`,
        content: {
          type: 'array-object',
          data: data,
          display: [
            { property:'id', nicename: "Person ID" },
            { property:'name', nicename: "Name" },
          ],
          return: ['id', 'name'],
          search: true,
          searchTo: ['id', 'name'],
          sortTo: 'name',
        },
        accept: 'single',
        type: 'selector',
      }, { size: 'lg' })
  
      .selected.subscribe( response => {
        if( response.success ){
          this.selectedPerson = {
            id: response.data[0],
            name: response.data[1]
          }

          this.sgs.request('get', 'person/getOnePersonChildren', { id:this.selectedPerson.id }, res => {
              this.selectedPerson.children = res.data.children || [];
          }, { });

        }
      });
    }
  }

  UpdateChild(child){
    console.log(child);
    let data = {
      id: this.selectedPerson.id,
      child: {
        id: child.id,
        fname: child.fname,
        mname: child.mname,
        lname: child.lname
      }
    }
    this.sgs.request('put', 'person/UpdateChild', data, res => {
      if(res.success){
        this.sgs.Toaster('success', 'Success', res.message)
      }
    }, { });
  }

  ngOnDestroy(){
    this.socketInstancePersonChildren.unsubscribe();
  }
  
}
