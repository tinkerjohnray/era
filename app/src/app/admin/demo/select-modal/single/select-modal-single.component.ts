import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';


@Component({
  selector: 'tinker-select-modal-single-component',
  templateUrl: './select-modal-single.component.html',
  styleUrls: ['./select-modal-single.component.scss']
})
export class SelectModalSingle implements OnInit {

  public data: any = [];
  public Result: any = {};

  constructor(
    public sgs: SharedGlobalService
  ){
    sgs.setBrowserTitle.emit('Select Modal - Single / Demo');
    this.initResult();
  }

  ngOnDestroy(){}

  ngOnInit() {}

  initResult(){
    this.Result = {
      name: "",

      city: {},
      _displaycity: "",

      smartphonebrand: {},
      _displaysmartphonebrand: "",
    }
  }
  

  SelectSmartPhoneBrand(){
    this.sgs.Modal({
      header: `Select Terms`,
      content: {
        type: 'array-object',
        data: [
          {name: 'S10', brand: 'Samsung', id: 1},
          {name: 'S9', brand: 'Samsung', id: 2},
          {name: 'P20', brand: 'Huawei', id: 3},
          {name: 'P30', brand: 'Huawei', id: 4},
          {name: 'Redmi 2s', brand: 'Xiaomi', id: 5},
          {name: 'Redmi 3s', Probrandvince: 'Xiaomi', id: 6},
          {name: 'Redmi', brand: 'Xiaomi', id: 7},
          {name: 'Mix 2', brand: 'Xiaomi', id: 8}
        ],
        display: [{ property:'name', nicename: "Name" }, { property:'brand', nicename: "Brand" }],
        return: ['id', 'name', 'brand'],
        search: true,
        searchTo: ['id', 'name'],
        sortTo: 'name',
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

    .selected.subscribe( response => {
      if( response.success ){
        this.Result._displaysmartphonebrand = response.data[1];
        this.Result.smartphonebrand.id = response.data[0];
        this.Result.smartphonebrand.name = response.data[1];
        this.Result.smartphonebrand.province = response.data[2];
      }
    });
  }
  

  selectName(){
    this.sgs.Modal({
      header: `Select Name`,
      content: {
        type: 'array-object',
        data: [
          { id: 1, name: 'Karl' },
          { id: 2, name: 'John Paul' },
          { id: 3, name: 'Kenneth' },
          { id: 4, name: 'Jimmy' },
          { id: 5, name: 'Akeem' },
          { id: 6, name: 'Matt' },
        ],
        display: [{ property:'name', nicename: "name" }],
        return: ['name'],
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

    .selected.subscribe( response => {
      if( response.success ){
        this.Result.name = response.data[0];
      }
    });
  }

  selectCity(){
    this.sgs.Modal({
      header: `Select City`,
      content: {
        type: 'array-object',
        data: [
          {name: 'Bacolod City', province: 'Negros Occidental', id: 1},
          {name: 'Bago City', province: 'Negros Occidental', id: 2},
          {name: 'Himamaylan City', province: 'Negros Occidental', id: 3},
          {name: 'Victorias City', province: 'Negros Occidental', id: 4},
          {name: 'Bais City', province: 'Negros Oriental', id: 5},
          {name: 'San Carlos City', province: 'Negros Occidental', id: 6},
          {name: 'Dumaguete City', province: 'Negros Oriental', id: 7},
          {name: 'Kabankalan City', province: 'Negros Occidental', id: 8}
        ],
        display: [{ property:'name', nicename: "Terms" }],
        return: ['id', 'name', 'province'],
        search: true,
        searchTo: ['id', 'name'],
        sortTo: 'name',
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

    .selected.subscribe( response => {
      if( response.success ){
        this.Result._displaycity = response.data[1];
        this.Result.city.id = response.data[0];
        this.Result.city.name = response.data[1];
        this.Result.city.province = response.data[2];
      }
    });
  }
  
}
