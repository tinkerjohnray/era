import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';
import { ImageGallery } from '../../../@core/pipes/dataFilter';

@Component({
  selector: 'tinker-image-gallery-component',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss'],
  providers: [ImageGallery]
})
export class ImageGalleryComponent implements OnInit {

  public data: any = [];
  public Result: any = {};

  constructor(
    public sgs: SharedGlobalService,
    public img: ImageGallery
  ){
    sgs.setBrowserTitle.emit('Angular2 Datatable / Demo');

    this.data = [
      { id:1, company: "TinkerTech", fname: "Johny", lname: "Deep" },
      { id:2, company: "Microsoft", fname: "Carlo", lname: "Yap" },
      { id:3, company: "Google", fname: "Papa", lname: "Deep" },
      { id:4, company: "TinkerTech", fname: "John", lname: "Volta" },
      { id:5, company: "Apple", fname: "Gora", lname: "Ma" },
      { id:6, company: "TinkerTech", fname: "Gusion", lname: "Deep" },
      { id:7, company: "Amazon", fname: "Johny", lname: "Boltron" },
      { id:8, company: "Amazon", fname: "Papa", lname: "Ball" },
      { id:9, company: "SpaceX", fname: "Habon", lname: "James" },
      { id:10, company: "Microsoft", fname: "Kawit", lname: "Leonard" },
      { id:11, company: "TinkerTech", fname: "Perdi", lname: "Gsw" },
      { id:12, company: "TinkerTech", fname: "Johny", lname: "Deep" },
      { id:13, company: "Google", fname: "Mayer", lname: "Smith" },
      { id:14, company: "Google", fname: "Yummy", lname: "Me" },
      { id:15, company: "Apple", fname: "Rosana", lname: "Boses" },
    ]
  }

  ngOnDestroy(){}

  ngOnInit() {}

  
  selectImage(){
    this.sgs.Modal({
      header: `Image Gallery`,
      showFooter: false,
      content: {
        return: ['source', 'id'],
      },
      type: 'image-gallery',
    }, { size: 'xl' })

    .selectedImage.subscribe( response => {
      // console.log(response);
      if( response.success ){
        this.Result.name = response.data[0];
      }
    });
  }

  Preview(src){
    let extension = src.split('.').pop();
    if(['gif', 'jpg', 'png', 'jpeg'].includes(extension)){
      this.sgs.Modal({
        header: `Preview`,
        showFooter: false,
        content: `<img src="${this.img.transform(src, 'image-gallery', 'html')}" width="100%">`,
        buttonName: 'close'
      }, { size: 'sm' });
    }else{
      this.sgs.Modal({
        header: `Preview`,
        showFooter: false,
        content: `You can only view image formats.`,
        buttonName: 'close'
      }, { size: 'sm' });
    }

  }
}
