import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';

@Component({
  selector: 'tinker-sgs-crud-component',
  templateUrl: './sgs-crud.component.html',
  styleUrls: ['./sgs-crud.component.scss']
})
export class SgsCrudComponent implements OnInit {

  public data: any = [];
  public Person: any = {};
  public socketInstance;
  public selectedPerson: any = {};

  public Animal: any = {}

  // addanimal(){
  //   this.sgs.request('post', 'animal/addAnimal', this.Animal, response => {
  //     if(response.success){
  //       this.sgs.Toaster('success', 'Success', 'New animal has been created');
  //     }
  //   });
  // }

  constructor(
    public sgs: SharedGlobalService
  ){
    sgs.setBrowserTitle.emit('SGS CRUD / Demo');
    this.getData();
    this.initPerson();
    this.initSelectedPerson();

    this.socketInstance = sgs.ResponseSocket('person').subscribe( emitted => {
      if(emitted.success){
        this.getData(false);
      }
    });
  }

  ngOnDestroy(){
    this.socketInstance.unsubscribe();
  }

  Submit(f){
    this.sgs.request('post', 'person/createPerson', this.Person, response => {
      if(response.success){
        this.sgs.Toaster('success', 'Success', 'New person has been created');
        f.reset();
        this.initPerson();
      }
    });
  }

  updatePerson(){
    this.sgs.request('put', 'person/updatePerson', this.selectedPerson, response => {
      if(response.success){
        this.sgs.Toaster('success', 'Success', `Person ID [${this.selectedPerson.id}] has been updated`);
        this.selectedPerson.selected = "";
        // this.initSelectedPerson();
      }
    });
  }

  updatePersonStatus(){
    this.sgs.request('put', 'person/updatePersonStatus', { id:this.selectedPerson.id }, response => {
      if(response.success){
        this.sgs.Toaster('success', 'Success', `Person ID [${this.selectedPerson.id}] has been deleted`);
        this.initSelectedPerson();
      }
    });
  }

  getData(socketLoader = true){
    this.sgs.request('get', 'person/getAllPeople', '', res => {
      if(res.success){
        this.data = res.data;
      }else{
        this.data = [];
      }
    }, {toaster:false, socketLoader: socketLoader});
  }

  

  initSelectedPerson(){
    this.selectedPerson = {
      id: 0,
      fname: "",
      mname: "",
      lname: "",
      age: 0,
      gender: "",
      selected: "",
    }
  }

  initPerson(){
    this.Person = {
      fname: "",
      mname: "",
      lname: "",
      age: 0,
      gender: "",
    }
  }

  NumberLimit(a){
    if( a.target.value && a.target.value.toString().length > a.target.maxLength ){
      a.target.value = a.target.value.slice(0, a.target.maxLength);
      return a.target.value;
    }
  }

  selectPerson(){
    console.log(this.data);

    this.sgs.Modal({

      header: `Select Person`,
      content: {
        type: 'array-object',
        data: this.data,
        display: [
          { property:'id', nicename: "Person ID" },
          { property:'fname', nicename: "First Name" },
          { property:'mname', nicename: "Middle Name" },
          { property:'lname', nicename: "Last Name" },
          { property:'age', nicename: "Age" },
          { property:'gender', nicename: "Gender" },
        ],
        return: ['id', 'fname', 'mname', 'lname', 'gender', 'age', 'date_added'],
        search: true,
        searchTo: ['id', 'fname', 'mname', 'lname', 'gender', 'age'],
        sortTo: 'fname',
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'lg' })

    .selected.subscribe( response => {
      if( response.success ){
        console.log(response.data)
        this.selectedPerson = {
          id: response.data[0],
          fname: response.data[1],
          mname: response.data[2],
          lname: response.data[3],
          gender: response.data[4],
          age: response.data[5],
          selected: [response.data[1], response.data[2], response.data[3]].join(' '),
        }
      }
    });    
  }

  selectGender(){
    this.sgs.Modal({
      header: `Select Gender`,
      content: {
        type: 'array-object',
        data: [
          { id: 1, name: 'Male' },
          { id: 2, name: 'Female' },
        ],
        display: [{ property:'name', nicename: "Gender" }],
        return: ['name'],
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

    .selected.subscribe( response => {
      if( response.success ){
        this.Person.gender = response.data[0];
      }
    });
  }

  selectUpdateGender(){
    this.sgs.Modal({
      header: `Select Gender`,
      content: {
        type: 'array-object',
        data: [
          { id: 1, name: 'Male' },
          { id: 2, name: 'Female' },
        ],
        display: [{ property:'name', nicename: "Gender" }],
        return: ['name'],
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

    .selected.subscribe( response => {
      if( response.success ){
        this.selectedPerson.gender = response.data[0];
      }
    });
  }

  ngOnInit() {}
  
}
