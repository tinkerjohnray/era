import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';


@Component({
  selector: 'tinker-dtmultifilter-component',
  templateUrl: './dtmultifilter.component.html',
  styleUrls: ['./dtmultifilter.component.scss']
})
export class TinkerDTMultifilterComponent implements OnInit {

  public data: any = [];

  constructor(
    public sgs: SharedGlobalService
  ){
    
    sgs.setBrowserTitle.emit('Angular2 Datatable - Multifilter Support / Demo');

    this.data = [
      { id:1, company: "TinkerTech", fname: "Johny", lname: "Deep" },
      { id:2, company: "Microsoft", fname: "Carlo", lname: "Yap" },
      { id:3, company: "Google", fname: "Papa", lname: "Deep" },
      { id:4, company: "TinkerTech", fname: "John", lname: "Volta" },
      { id:5, company: "Apple", fname: "Gora", lname: "Ma" },
      { id:6, company: "TinkerTech", fname: "Gusion", lname: "Deep" },
      { id:7, company: "Amazon", fname: "Johny", lname: "Boltron" },
      { id:8, company: "Amazon", fname: "Papa", lname: "Ball" },
      { id:9, company: "SpaceX", fname: "Habon", lname: "James" },
      { id:10, company: "Microsoft", fname: "Kawit", lname: "Leonard" },
      { id:11, company: "TinkerTech", fname: "Perdi", lname: "Gsw" },
      { id:12, company: "TinkerTech", fname: "Johny", lname: "Deep" },
      { id:13, company: "Google", fname: "Mayer", lname: "Smith" },
      { id:14, company: "Google", fname: "Yummy", lname: "Me" },
      { id:15, company: "Apple", fname: "Rosana", lname: "Boses" },
    ]
  }

  ngOnDestroy(){}

  ngOnInit() {}
  
}
