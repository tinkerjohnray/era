import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ThemeModule } from '../@theme/theme.module';
import { SharedModule } from '../shared/shared.module';
import { QRCodeModule } from 'angularx-qrcode';
import { AddUserComponent } from './add-user/add-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UsersComponent } from './users/users.component';

import {
  SgsCrudComponent,
  SelectModalSingle,
  TinkerDTComponent,
  TinkerDTMultifilterComponent,
  ImageGalleryComponent,
  SgsArrayUpdate,
} from './index';
import { CreateBrandComponent } from './create-brand/create-brand.component';
import { BrandsListComponent } from './brands-list/brands-list.component';
import { ViewBrandComponent } from './brands-list/modal/view-brand/view-brand.component';
import { EditBrandComponent } from './brands-list/modal/edit-brand/edit-brand.component';
import { CreatebraComponent } from './brands-list/modal/createbra/createbra.component';
import { BoardsComponent } from './boards/boards.component';
import { BirComponent } from './bir/bir.component';
import { SssComponent } from './sss/sss.component';
import { PagibigComponent } from './pagibig/pagibig.component';
import { FillComponent } from './sss/modal/fill/fill.component';
import { SeletedComponent } from './seleted/seleted.component';
import { ApplicantsComponent } from './applicants/applicants.component';
import { ViewApplicantsComponent } from './applicants/modal/view-applicants/view-applicants.component';
import { EditApplicantsComponent } from './applicants/modal/edit-applicants/edit-applicants.component';
import { ClientsComponent } from './clients/clients.component';
import { CreateClientComponent } from './clients/modal/create-client/create-client.component';
import { TaskComponent } from './clients/modal/task/task.component';
import { EditClientComponent } from './clients/modal/edit-client/edit-client.component';
import { CreateTaskComponent } from './clients/modal/task/modal/create-task/create-task.component';
import { EditTaskComponent } from './clients/modal/task/modal/edit-task/edit-task.component';

const ADMIN_COMPONENTS = [
  DashboardComponent,
  AdminComponent,
  AddUserComponent,
  UpdateUserComponent,
  UsersComponent,
  CreateBrandComponent,

  SgsCrudComponent,
  SelectModalSingle,
  TinkerDTComponent,
  TinkerDTMultifilterComponent,

  ImageGalleryComponent,
  SgsArrayUpdate,
];

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    AdminRoutingModule,
    SharedModule,
    QRCodeModule,
  ],
  entryComponents: [
    AddUserComponent,
    UpdateUserComponent,
  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    BrandsListComponent,
    ViewBrandComponent,
    EditBrandComponent,
    CreatebraComponent,
    BoardsComponent,
    BirComponent,
    SssComponent,
    PagibigComponent,
    FillComponent,
    SeletedComponent,
    ApplicantsComponent,
    ViewApplicantsComponent,
    EditApplicantsComponent,
    ClientsComponent,
    CreateClientComponent,
    TaskComponent,
    EditClientComponent,
    CreateTaskComponent,
    EditTaskComponent,
    
  ],
  providers: [],
})
export class AdminModule {
  
 }
