import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';

@Component({
  selector: 'ngx-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {

  constructor(
    public sgs: SharedGlobalService
  ) { }

  ngOnInit() {
  }
 c1Submit(){
   this.sgs.goto('/admin/sss')
 }
 c2Submit(){
  this.sgs.goto('/admin/bir')
}
c3Submit(){
  this.sgs.goto('/admin/pagibig')
}
}
