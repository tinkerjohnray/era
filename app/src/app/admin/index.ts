export * from './admin.component';

export * from './demo/sgs-crud/sgs-crud.component';
export * from './demo/select-modal/single/select-modal-single.component';
export * from './demo/dt/dt.component';
export * from './demo/dtmultifilter/dtmultifilter.component';

export * from './demo/image-gallery/image-gallery.component';

export * from './demo/sgs-array-update/sgs-array-update.component';