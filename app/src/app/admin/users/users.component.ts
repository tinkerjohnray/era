import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddUserComponent } from '../add-user/add-user.component';
import { UpdateUserComponent } from '../update-user/update-user.component';

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  bsValue = new Date();
  filterQuery = '';
  sortBy = 'id';
  sortOrder = 'asc';
  selectQueryString = 'Last Name';
  selectQuery = 'lname';
  data;
  loading = true;
  date = new Date();
  todate;
  public socketInstance;
  me;

  constructor(
    public sgs: SharedGlobalService,
    public ngbModal: NgbModal
  ) {
    this.socketInstance = sgs.ResponseSocket('users').subscribe( emitted => {
      this.getAllUsers();
    });
   }

  ngOnInit() {
    this.getAllUsers();
    // this.getUserAccount();
  }

  ngOnDestroy(){
    this.socketInstance.unsubscribe();
  }

  getUserAccount(){
    this.sgs.request('get', 'user/getMyAccount', null, async (res) => {
      if(res.success){
        this.me = res.data;
      }
    });
  }

  getAllUsers(){
    this.sgs.request('get', 'user/getAllUsers', null, async (response) => {
      if( response.success ){
       this.data = response.data;
       this.loading = false;
      }else{
        console.log( response );
      }
    },{cache:true, describe: "Error getting users!" });
  }


  selectFilter(name, value){
    this.selectQuery = name;
    this.selectQueryString = value;
  }


  addUser(){
    const activeModal = this.ngbModal.open(AddUserComponent, { size: 'lg', container: 'nb-layout', windowClass: 'min_height' });
  }

  updateUser(id){
    const activeModal = this.ngbModal.open(UpdateUserComponent, { size: 'lg', container: 'nb-layout', windowClass: 'min_height' });
    activeModal.componentInstance.uid = id;
  }

  disableAccount(id){
    this.sgs.Modal({
      header: `System Message`,
      content: `
        Are you sure you want to disable this user?
      `,
      type: 'confirmation',
      buttonName: 'close'
    }, { size: 'sm'})
    .confirm.subscribe( response => {
      if( response ){
        this.sgs.request('post', 'user/deactivateUser', {id: id}, async (res) => {
          if(res.success){
            this.getAllUsers();
            this.sgs.Toaster('success', 'Success', 'The user has been disabled.');
          }
        });
      }
    });
  }

  enableAccount(id){
    this.sgs.Modal({
      header: `System Message`,
      content: `
        Are you sure you want to enabable this user?
      `,
      type: 'confirmation',
      buttonName: 'close'
    }, { size: 'sm'})
    .confirm.subscribe( response => {
      if( response ){
        this.sgs.request('post', 'user/enableUser', {id: id}, async (res) => {
          if(res.success){
            this.getAllUsers();
            this.sgs.Toaster('success', 'Success', 'The user has been enabled.');
          }
        });
      }
    });
  }

}
