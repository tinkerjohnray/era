import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-edit-applicants',
  templateUrl: './edit-applicants.component.html',
  styleUrls: ['./edit-applicants.component.scss']
})
export class EditApplicantsComponent implements OnInit {
public editData: any = {
  basic: {}
};
public fname: any;
public Forms: any = {};
public sex: any = false;
  constructor(
    public sgs: SharedGlobalService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }



  editApplicants() {
    const Forms = {
      id: this.editData.id,
      fname: this.editData.fname,
      lname: this.editData.lname,
      mname: this.editData.mname,
      sex: this.editData.sex,
      basic: {
        age: this.editData.basic.age,
        address: this.editData.basic.address,
        company: this.editData.basic.company,
      },
    }
    this.sgs.request('put', 'sss/editApplicants', Forms, response => { 
      console.log(response.data)
      if (response.success || response.untouched || response.pristine) {
        this.sgs.Toaster('success', `${Forms.fname} has been`, 'edited Successfully!');
        this.closeModal();
      } 
    })
  }

  selectSex() {
    this.sgs.Modal({
      header: `Select Sex`,
      content: {
        type: 'array-object',
        data: [
          { id: 1, name: 'Male' },
          { id: 2, name: 'Female' },
        ],
        display: [{ property: 'name', nicename: "Sex" }],
        return: ['name'],
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

      .selected.subscribe(response => {
        if (response.success) {
          this.sex = response.data[0];
        }
      });
  }

  closeModal() {
    this.activeModal.close();
  }

}
