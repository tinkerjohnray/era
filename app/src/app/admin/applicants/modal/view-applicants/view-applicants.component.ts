import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';




@Component({
  selector: 'ngx-view-applicants',
  templateUrl: './view-applicants.component.html',
  styleUrls: ['./view-applicants.component.scss']
})
export class ViewApplicantsComponent implements OnInit {
public viewData;
  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }
  closeModal() {
    this.activeModal.close();
  }
}
