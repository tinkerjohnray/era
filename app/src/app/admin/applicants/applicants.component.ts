import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';
import { EditApplicantsComponent } from './modal/edit-applicants/edit-applicants.component';
import { ViewApplicantsComponent } from './modal/view-applicants/view-applicants.component';


@Component({
  selector: 'ngx-applicants',
  templateUrl: './applicants.component.html',
  styleUrls: ['./applicants.component.scss']
})
export class ApplicantsComponent implements OnInit {
  public data: any;
  constructor(
    public sgs: SharedGlobalService
  ) { }

  ngOnInit() {
    this.getApplicants();
  }

  getApplicants() {
    this.sgs.request('get', 'sss/getApplicants', '', response => {
      if (response.success) {
        this.data = response.data;
      }
    })
  }

  editApplicants(data) {
    const editData = data;
    this.sgs.Modal(
      { editData },
      {
        component: EditApplicantsComponent,
        size: 'lg',
      });
  }
  deleteApplicants(data) { 
    this.sgs.request('put', 'sss/deleteApplicants', data, response => {
      if (response.success) {
        this.sgs.Toaster('danger', `ID of ${data.id}`,` has been deleted!`);
        this.getApplicants();
      } 
    })
}


viewApplicants(data) {
  const viewData = data;
  this.sgs.Modal(
    { viewData },
    {
      component: ViewApplicantsComponent,
      size: 'lg',
    });
}
}
