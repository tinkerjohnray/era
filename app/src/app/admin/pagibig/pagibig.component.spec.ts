import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagibigComponent } from './pagibig.component';

describe('PagibigComponent', () => {
  let component: PagibigComponent;
  let fixture: ComponentFixture<PagibigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagibigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagibigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
