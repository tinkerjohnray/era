import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';



@Component({
  selector: 'ngx-create-brand',
  templateUrl: './create-brand.component.html',
  styleUrls: ['./create-brand.component.scss']
})
export class CreateBrandComponent implements OnInit {
  brand: String;
  operatingsystem: String;
  processor: String;
  chipset: String;
  touch: Boolean;
  model: String;
  val: any = {
    specs: {},
    model: []
  };

  activatedRoute: any;
  url: any;
  brandname: any = [];


  constructor(
    public sgs: SharedGlobalService,
  ) {

  }

  ngOnInit() {
  }

  onBrandSubmit() {
    const brand = {
      brand: this.brand,
      model: this.val.model,
      specs: {
        operatingsystem: this.operatingsystem,
        processor: this.processor,
        chipset: this.chipset,
        touch: this.touch,
      },
    }
    this.sgs.request('post', 'brand/addMobile', brand, response => {
      if (response.success) {
        this.sgs.goto('/admin/mobile-brands/brands-list');
        this.sgs.Toaster('success', 'Success', 'New Brand has been created');
 

      }

    })
  }



  addModel(data) {
    let same = this.val.model.map(e => e.name === data);
    if (!same.includes(true)) {
      this.val.model.push({ name: data })
      this.model = '';
    } else {
      this.sgs.Toaster('', 'Error',
        `${data} already in list!`);
      this.model = '';
    }
  }




  deleteModel(name) {
    let index = this.val.model.findIndex(e => e.name === name);
    if (index > -1) {
      this.val.model.splice(index, 1);
    }
  }

}
