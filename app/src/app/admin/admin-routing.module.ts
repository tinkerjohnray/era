import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AdminComponent } from './admin.component';
import { DashboardComponent} from './dashboard/dashboard.component';


import {
  SgsCrudComponent,
  SelectModalSingle,
  TinkerDTComponent,
  TinkerDTMultifilterComponent,
  ImageGalleryComponent,
  SgsArrayUpdate,
} from './index';

import { UsersComponent } from './users/users.component';
import { CreateBrandComponent } from './create-brand/create-brand.component';
import { BrandsListComponent } from './brands-list/brands-list.component';
import { EditBrandComponent } from './brands-list/modal/edit-brand/edit-brand.component';
import { ViewBrandComponent } from './brands-list/modal/view-brand/view-brand.component';
import { CreatebraComponent } from './brands-list/modal/createbra/createbra.component';
import { BoardsComponent } from './boards/boards.component';
import { SssComponent } from './sss/sss.component';
import { BirComponent } from './bir/bir.component';
import { PagibigComponent } from './pagibig/pagibig.component';
import { FillComponent } from './sss/modal/fill/fill.component';
import { SeletedComponent } from './seleted/seleted.component';
import { ApplicantsComponent } from './applicants/applicants.component';
import { ViewApplicantsComponent } from './applicants/modal/view-applicants/view-applicants.component';
import { EditApplicantsComponent } from './applicants/modal/edit-applicants/edit-applicants.component';
import { ClientsComponent } from './clients/clients.component';
import { CreateClientComponent } from './clients/modal/create-client/create-client.component';
import { EditClientComponent } from './clients/modal/edit-client/edit-client.component';
import { TaskComponent } from './clients/modal/task/task.component';
import { CreateTaskComponent } from './clients/modal/task/modal/create-task/create-task.component';
import { EditTaskComponent } from './clients/modal/task/modal/edit-task/edit-task.component';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
      children: [
        {
          path: 'dashboard',
          component: DashboardComponent,
        },
        {
          path: 'dashboard',
          component: DashboardComponent,
        }
      ]
    },
    {
      path: 'mobile-brands',
      children: [
     { 
         path: 'create-brand',
         component: CreateBrandComponent
        },
        { 
          path: 'brands-list',
          component: BrandsListComponent
         },
         { 
          path: 'edit-brand',
          component: EditBrandComponent
         },
         { 
          path: 'view-brand',
          component: ViewBrandComponent
         }, 
          { 
          path: 'createbra',
          component: CreatebraComponent
         },
         
         

      ]
      
    },
 
    {
      path: 'demo',
      children: [
        {
          path: 'sgs-crud',
          component: SgsCrudComponent
        },
        {
          path: 'sgs-array-update',
          component: SgsArrayUpdate
        },
        {
          path: 'dt',
          component: TinkerDTComponent
        },
        {
          path: 'image-gallery',
          component: ImageGalleryComponent
        },
        {
          path: 'dtmultifilter',
          component: TinkerDTMultifilterComponent
        },
        {
          path: 'select-modal-single',
          component: SelectModalSingle
        }
      ]
    },
    {
      path: 'selected',
      component: SeletedComponent
    },
    {
      path: 'boards',
      component: BoardsComponent
    },
    {
      path: 'sss',
      component: SssComponent
    },
    {
      path: 'fill',
      component: FillComponent
    },
    {
      path: 'bir',
      component: BirComponent
    },
    {
      path: 'pagibig',
      component: PagibigComponent
    },
    {
      path: 'users',
      component: UsersComponent
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'applicants',
      component: ApplicantsComponent
    },
    {
      path: 'viewapplicants',
      component: ViewApplicantsComponent
    },
    {
      path: 'editapplicants',
      component: EditApplicantsComponent
    },
    {
      path: 'client',
      children: [
        {
          path: 'clients-list',
          component: ClientsComponent
        },
        {
          path: 'create-client',
          component: CreateClientComponent
        },
        {
          path: 'edit-client',
          component: EditClientComponent
        },
        {
          path: 'client-task',
          component: TaskComponent,
          children: [
            {
              path: 'create-task',
              component: CreateTaskComponent
            },
            {
              path: 'edit-task',
              component: EditTaskComponent
            },
          ]
        }
      ]
    }

 
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
