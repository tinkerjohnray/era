import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';
import { CreateClientComponent } from './modal/create-client/create-client.component';
import { TaskComponent } from './modal/task/task.component';
import { EditClientComponent } from './modal/edit-client/edit-client.component';
import { CreateTaskComponent } from './modal/task/modal/create-task/create-task.component';

@Component({
  selector: 'ngx-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
public data: any;
  constructor(
    public sgs: SharedGlobalService
  ) { }

  ngOnInit() {
    this.getClient();
  }
  createClient(data) {
    const createData = data;
    this.sgs.Modal(
      { createData },
      {
        component: CreateClientComponent,
        size: 'md',
      });
  }
  task() {
    this.sgs.goto('admin/client/client-task')
  }

  onEdit(data) {
    const editClient = data;
    this.sgs.Modal(
      { editClient },
      {
        component: EditClientComponent,
        size: 'md',
      });
  }

  deleteClient(data) { 
    this.sgs.request('put', 'client/deleteClient', data, response => {
      if (response.success) {
        this.sgs.Toaster('danger', `${data.name}`, ` has been deleted!`);
        this.getClient();
      } 
    })
}
  
  getClient() {
  this.sgs.request('get', 'client/getClient', '', response => {
    if (response.success) {
      this.data = response.data;

    }

  })
}
}
