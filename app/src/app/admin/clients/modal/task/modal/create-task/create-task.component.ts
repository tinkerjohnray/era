import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedGlobalService } from '../../../../../../@core/services/shared.global.service';


@Component({
  selector: 'ngx-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {
 public name: any;

  constructor(
    public activeModal: NgbActiveModal,
    public sgs: SharedGlobalService

  ) { }

  ngOnInit() {
  }
  tasks() {
    const task = {
     name: this.name,
    }
    this.sgs.request('post', 'task/tasks', task, response => {
      if (response.success) {
        this.closeModal();
        this.sgs.goto('/admin/client/client-task');
        this.sgs.Toaster('success', 'Success', 'New Task has been added');

      } 
    })
  }

  closeModal() {
    this.activeModal.close();
  }
}
