import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'ngx-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss']
})
export class EditTaskComponent implements OnInit {
public editData: any = {};
 public data: any;
// public Task: any ={};
// public name: any;

  constructor(
    public sgs: SharedGlobalService,
    public activeModal: NgbActiveModal

  ) { }

  ngOnInit() {   
  }


  editedTask() {
    const Task = {
      name: this.editData.name
    }
    this.sgs.request('put', 'task/editedTask', Task, response => {  
      console.log(response)
      if (response.success || response.untouched) {
        this.sgs.Toaster('success', `has been`, 'edited Successfully!');
        this.closeModal();
      } 
    })
  }



  closeModal() {
    this.activeModal.close();
  }
}
