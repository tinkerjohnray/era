import { Component, OnInit } from '@angular/core';
import { CreateTaskComponent } from './modal/create-task/create-task.component';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';
import { EditTaskComponent } from './modal/edit-task/edit-task.component';

@Component({
  selector: 'ngx-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
 public data: any;

  constructor(
public sgs: SharedGlobalService
  ) { }

  ngOnInit() {
    this.getTasks();
  }
  createTask(data) {
    const createTask = data;
    this.sgs.Modal(
      { createTask },
      {
        component: CreateTaskComponent,
        size: 'md',
      });
  } 

  getTasks() {
    this.sgs.request('get', 'task/getTasks', '', response => {
      console.log(response)
      if (response.success) {
        this.data = response.data;

      }

    })
  }

  deleteTask(data) { 
    this.sgs.request('put', 'task/deleteTask', data, response => {
      if (response.success) {
        this.sgs.Toaster('danger', `${data.name}`, ` has been deleted!`);
        this.getTasks();
      } 
    })
}
editTask(data){ 
  const editData = data;
  console.log(editData)
  this.sgs.Modal(
    { editData },
    {
      component: EditTaskComponent,
      size: 'md',
    });
}
}
