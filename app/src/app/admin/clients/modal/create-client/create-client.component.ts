import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'ngx-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.scss']
})
export class CreateClientComponent implements OnInit {
  public address: any;
  public name: any;

  constructor(
    public sgs: SharedGlobalService,
    public activeModal: NgbActiveModal,
 
  ) { }

  ngOnInit() {
  }
  createClient() {
    const client = {
     name: this.name,
     address: this.address
    }
    this.sgs.request('post', 'client/createClient', client, response => {
      if (response.success) {
        this.closeModal();
        this.sgs.goto('/admin/client/clients-list');
        this.sgs.Toaster('success', 'Success', 'New Client has been added');

      } 
    })
  }

  closeModal() {
    this.activeModal.close();
  }
}
