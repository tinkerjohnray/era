import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'ngx-seleted',
  templateUrl: './seleted.component.html',
  styleUrls: ['./seleted.component.scss'],
  providers: [NgbActiveModal]
})
export class SeletedComponent implements OnInit {
  // public selectedBrand: any = {};
  public data: any = [];
  public Brand: any = {};
  public val: any = {
    specs: {},
    model: []
  };
  model: string;
  operatingsystem: String;
  processor: String;
  chipset: String;
  touch: Boolean = false;
  brand: String;
  constructor(
    public sgs: SharedGlobalService,
    public activeModal: NgbActiveModal
  ) {
    // this.initBrand();
    // this.selectedBrand();
   }

  ngOnInit() {
  }

  onBrandSubmit() {
    const brand = {
      brand: this.brand,
      model: this.val.model,
      specs: {
        operatingsystem: this.operatingsystem,
        processor: this.processor,
        chipset: this.chipset,
        touch: this.touch,
      },
    }

    this.sgs.request('post', 'brand/addMobile', brand, response => { console.log(response)
      console.log(this.Brand)
      if (response.success) {
        this.sgs.Toaster('success', 'Success', 'New Brand has been created');
      }
    })
  }

  addModel(data) {
    let same = this.val.model.map(e => e.name === data);
    if (!same.includes(true)) {
      this.val.model.push({ name: data })
      this.model = '';
    } else {
      this.sgs.Toaster('', 'Error',
        `${data} already in list!`);
      this.model = '';
    }
  }

  deleteModel(name) {
    let index = this.val.model.findIndex(e => e.name === name);
    if (index > -1) {
      this.val.model.splice(index, 1);
    }
  }

  closeModal() {
    this.activeModal.close();
  }

  selectTouch() {
    this.sgs.Modal({
      header: `SelectTouch`,
      content: {
        type: 'array-object',
        data: [
          { id: 1, name: 'True' },
          { id: 2, name: 'False' },
        ],
        display: [{ property: 'name', nicename: "Touch" }],
        return: ['name'],
      },
      accept: 'single',
      type: 'selector',
    }, { size: 'md' })

      .selected.subscribe(response => {
        if (response.success) {
          this.Brand.touch = response.data[0];
        }
      });
  }


}
