import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { SharedGlobalService } from '../../@core/services/shared.global.service';

@Component({
  selector: 'ngx-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  loading = true;
  public form: any;
  data;
  showpassword = false;
  eyeIcon = "fas fa-eye";

  constructor(
    public activeModal: NgbActiveModal,
    public formBuilder: FormBuilder,
    public sgs: SharedGlobalService
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      role:         ['', [Validators.required]],
      username:     ['', [Validators.required]],
      password:     ['', [Validators.required]],
      lname:        ['', [Validators.required]],
      fname:        ['', [Validators.required]],
      mname:        [''],
    })
  }

  ngOnInit() {
  }

  addUser(data){
    this.sgs.request('post', 'user/createUser', data.value, async (response) => {
      if(response.success){
        this.sgs.Toaster('success', 'Success', 'New user has been added.');
        this.closeModal();
      }
    })
  }

  showPassword(){
    if(this.showpassword == true){
      this.showpassword = false;
      this.eyeIcon = "fas fa-eye";
    }else{
      this.showpassword = true;
      this.eyeIcon = "fas fa-eye-slash";
    }
  }


  closeModal() {
    this.activeModal.close();
  }

}
