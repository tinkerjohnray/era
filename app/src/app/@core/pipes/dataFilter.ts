import * as _ from 'lodash';
import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ConnectionService } from '../services/connection.service' 

@Pipe({
  name: 'dataFilter'
})
@Injectable()
export class dataFilterPipe implements PipeTransform {
transform(array: any[], query: string, selector: string): any {
    if (query) {
        return _.filter(array, row => {


            if(selector.includes('.')){
                let x = selector.split('.'),
                    a = x[0],
                    b = x[1];
                if( (row[a][b] || '').toString().toLowerCase().indexOf(query) > -1 ){
                    return array
                }
            }


            if(Object.prototype.toString.call(query) === '[object Date]'){
                let qd = new Date(query);
                let qday = qd.getDate();
                let qmonth = qd.getMonth();
                let qyear = qd.getFullYear();

                let rd = new Date(row[selector]);
                let rday = rd.getDate();
                let rmonth = rd.getMonth();
                let ryear = rd.getFullYear();

                let queryDate = [qday, qmonth, qyear].join('');
                let rowDate = [rday, rmonth, ryear].join('');

                if(queryDate === rowDate){
                    return array;
                }

            }else if((row[selector] || '').toString().toLowerCase().indexOf(query) > -1){
                return array;
            } 
        });



    }
        return array;

    }
}

//Sample Usage => (1234.9876 | numberWithCommas : 2)
@Pipe({
    name: 'numberWithCommas',
})

export class NumberWithCommas implements PipeTransform {
    transform(num, places) {
        try {
            if (num > -1) {
                let decimalStr;
                if (String(num).split('.').length == 2 ) {
                    //console.log(String(num).split('.')[1].length);
                    if ( String(num).split('.')[1].length == 1) {
                        decimalStr = num.toString() + '0';
                    }else {
                        decimalStr = num.toString().split('.')[0] + num.toString().substr(num.toString().indexOf('.'), places + 1);
                    }
                } else {
                    decimalStr = num.toString() + '.00';
                }
                const parts = decimalStr.split('.');
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return parts.join('.');
            }
        }catch (e) {
            console.info('Math Error: ', e.message);
        }
    }
}

@Pipe({
    name: 'sum',
})

export class SumPipe implements PipeTransform {
    transform(items: any[], prop?): any {
        if(prop){
            return items.reduce((a, b) => a + b[prop], 0);
        }
        else{
            return items.reduce((a, b) => a + b, 0);
        }
    }
}

@Pipe({
    name: 'isRead',
})

export class IsRead implements PipeTransform {
    transform(items: any[], id): any {
        return items.filter(e => e.uid === id).length > 0 ? 'read' : 'unread';
    }
}


@Pipe({
    name: 'dscore',
})

export class DateScore implements PipeTransform {
    transform(item, b): any {
        return item.replace(/\_/g, b);
    }
}

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Pipe({ name: 'imageGallery' })
export class ImageGallery implements PipeTransform {
  constructor(private cs: ConnectionService) {}
  transform(src:String, path:String, type:String) {
    switch( type ){
        case 'html': return `${this.cs.connection}/${path}/${src}`
        case 'css': return `url('${this.cs.connection}/${path}/${src}')`;
        case 'local-html': return `${src}`
        case 'local-css': return `url('${src}')`;
    }
  }
}

@Pipe({
    name: 'multiFilter'
})

export class multiFilterPipe implements PipeTransform {
    transform(array: any[], query: string, mf: any, def:string): any {
        let temp = [];
        query = query.toString().toLowerCase();

        if( mf.length ){
            if(query){
                mf.forEach( prop => {
                    array.forEach( result => {
                        if( prop.name.includes('.') ){
                            //second level searching #support
                            let x = prop.name.split('.')[0];
                            let y = prop.name.split('.')[1];
                            if( (result[x][y] || '').toString().toLowerCase().includes(query) )
                                temp.push(result)
                        }else{
                            if( (result[prop.name] || '').toString().toLowerCase().includes(query) )
                                temp.push(result)
                        }
                    })
                });
                return temp;
            }else{
                return array;
            }
        }else{
            if(query){
                if( def.includes('.') ){
                    //second level searching #support
                    let x = def.split('.')[0];
                    let y = def.split('.')[1];
                    return array.filter( e => (e[x][y] || '').toString().toLowerCase().includes(query))
                }
                return array.filter( e => (e[def] || '').toString().toLowerCase().includes(query))
            }
            return array;
        }
    }
}