export class accountGroupModel{
    code:                string;
    name:                string;
    description:         string;
    status:              string;
    created:             string;
    dateCreated:         string;
    level:               number;
    category1:           firstLevelModel[];  //model for category1
}

export class firstLevelModel{
    code:                string;
    name:                string;
    description:         string;
    status:              string;
    created:             string;
    dateCreated:         string;
    level:               number;
    category2:           secondLevelModel[];  //model for category2
}

export class secondLevelModel{
    code:                string;
    name:                string;
    description:         string;
    status:              string;
    created:             string;
    dateCreated:         string;
    level:               number;
    category3:           thirdLevelModel[];  //model for category3
}

export class thirdLevelModel{
    code:                string;
    name:                string;
    description:         string;
    status:              string;
    created:             string;
    dateCreated:         string;
    level:               number;
}