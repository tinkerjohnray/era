import { Component } from '@angular/core';

import { ToasterService } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { SharedGlobalService } from './@core/services/shared.global.service';


@Component({
  selector: 'ngx-app',
  styles: [`
    .server-offline{
      background:#ff6b83;
      padding:12px 10px;
      color:#fff;
      text-align:center;
    }
  `],
  template: `
    <div class="server-offline" *ngIf="!sgs.server.status && sgs.Lan().status">
      <i class="fas fa-circle-notch fa-spin" style="opacity:.75; margin-right:10px"></i>Reconnecting &nbsp;&mdash;&nbsp; Server is Offline
    </div>
    <div class="server-offline" *ngIf="!sgs.Lan().status">
      <i class="fas fa-exclamation-circle"  style="opacity:.75; margin-right:10px"></i>No Internet Connection
    </div>
    <toaster-container [toasterconfig]="sgs.ToasterInit(toast)"></toaster-container>
    <div style="position:fixed; background:rgba(0,0,0,.5); width:100%; height:100%; z-index:999999999" *ngIf="sgs.selectorLoader">
      <div style="color:#fff; position:absolute; top:50%; left:50%; text-align:center; width:200px; height:30px; z-index:99999999; margin-top:-15px; margin-left:-100px">
      <i class="fas fa-circle-notch fa-spin" style="opacity:.75; margin-right:10px"></i>Loading data..
      </div>
    </div>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  
  public socketInstance;

  constructor(
    public sgs: SharedGlobalService,
    public toast: ToasterService
  ){
    this.socketInstance = this.sgs.ResponseSocket('serverStatus').subscribe(data => {
    });
  }

  ngOnDestroy(){ this.socketInstance.unsubscribe(); }
}
