import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Developed with ♥ by <b><a href="https://tinkertech.biz" target="_blank">Tinkertech Inc.</a></b> 2019</span>
  `,
})
export class FooterComponent {
}
