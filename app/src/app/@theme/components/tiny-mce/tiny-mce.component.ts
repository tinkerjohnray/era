import { Component, OnDestroy, AfterViewInit, Output, EventEmitter, ElementRef } from '@angular/core';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';

@Component({
  selector: 'ngx-tiny-mce',
  template: '',
})
export class TinyMCEComponent implements OnDestroy, AfterViewInit {

  @Output() editorKeyup = new EventEmitter<any>();

  editor: any;
  clearTinymceEmitter;
  hasSubscribed;

  constructor(
    private host: ElementRef,
    private sgs: SharedGlobalService
  ) { }

  ngAfterViewInit() {
    tinymce.init({
      target: this.host.nativeElement,
      plugins: ['link', 'paste', 'table'],
      skin_url: 'assets/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        this.editor.on('keyup', () => {
          this.editorKeyup.emit(editor.getContent());
        });

        this.editor.on('blur', () => {
          this.editorKeyup.emit(editor.getContent());
        });

        if(!this.hasSubscribed){
          this.clearTinymceEmitter = this.sgs.clearTinymce.subscribe( e=>{
            console.log('emit received');
            this.editor.setContent('');
          });
          this.hasSubscribed = true;
        }
      },
      height: '320',
    });
  }

  ngOnDestroy() {
    try{
      this.clearTinymceEmitter.unsubscribe();
      tinymce.remove(this.editor);
    }catch(e){
      console.info(e.name, e.message);
    }
  }
}
