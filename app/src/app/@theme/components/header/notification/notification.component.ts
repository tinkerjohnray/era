import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';
import { AuthService } from '../../../../@core/services/auth.service';

@Component({
    selector: 'tt-header-notification',
    styleUrls: ['./notification.component.scss'],
    templateUrl: './notification.component.html',  
})
export class NotificationComponent implements OnInit{
    user: any;
    cache: any = [];
    notifications: any = [];
    notifLoader:boolean = false;
    socketInstance;
    emitterInstance;
    tabTitle: String = "";

    constructor(
        private sgs: SharedGlobalService,
        private auth: AuthService
    ){
        this.loggedDefault();
        this.getData();

        this.socketInstance = sgs.ResponseSocket('notifications').subscribe( emitted => {
          if(emitted.success && emitted.data.readonly && emitted.data.uid === this.auth.getTokenData().id ){
            this.getData(false);
          }else{
            if(emitted.data.for !== undefined){
                if(emitted.data.for.filter(e => e.role === this.auth.getTokenData().role).length > 0){
                    this.getData(false);
                }
            }
          }
        });

        this.emitterInstance = sgs.cacheEmitter.subscribe( () => {
          this.loggedDefault();
        });
    }

    onChangeTab(event){
        this.tabTitle = event.tabTitle;
    }

    loggedDefault(){
        this.user = this.auth.getTokenData();
        this.cache = this.sgs.getCachedData();
    }

    getData(socketLoader = true){
        this.notifLoader = socketLoader;
        this.sgs.request('get', 'notification/getNotification', 'empty', res => {
            if(res.success){
                this.notifications = res.data;
            }else{
                this.notifications = [];
            }
            
            this.notifLoader = false;
        }, {toaster:false, socketLoader: false });
    }

    goto(url, nid, read?, role_module?){
        /**
         * @description It goes here once the notification has been read
         */
        if(read.filter(e => e.uid === this.user.id).length > 0){
            this.sgs.notificationStatus = false;

            if( this.user.role === 'admin' ){
                if( role_module ){
                    this.sgs.goto(`${this.user.role}/${role_module}/${url}`);
                }else{
                    this.sgs.goto(`${this.user.role}/${url}`);
                }
            }else{
                if(this.user.role === 'inventory' && url.includes('all-sales-order') ){
                    this.sgs.notificationStatus = false;
                }else{
                    this.sgs.goto(`${this.user.role}/${url}`);
                }
            }
            // this.user.role === 'admin'?
            // this.sgs.goto(`${this.user.role}/${role_module}/${url}`):
            // this.sgs.goto(`${this.user.role}/${url}`);
        }else{
            /**
             * @description unread
             */

            this.sgs.request('put', 'notification/updateNotificationRead', {nid:nid}, res => {
                this.sgs.notificationStatus = false;
                this.sgs.getNotificationLength();

                if( this.user.role === 'admin' ){
                    if( role_module ){
                        this.sgs.goto(`${this.user.role}/${role_module}/${url}`);
                    }else{
                        this.sgs.goto(`${this.user.role}/${url}`);
                    }
                }else{
                    if(this.user.role === 'inventory' && url.includes('all-sales-order') ){
                        this.sgs.notificationStatus = false;
                    }else{
                        this.sgs.goto(`${this.user.role}/${url}`);
                    }
                }
            });
        }
    }

    ngOnInit(){}
    ngOnChanges(){}
    ngOnDestroy(){
        this.emitterInstance.unsubscribe();
        this.socketInstance.unsubscribe();
    }
}