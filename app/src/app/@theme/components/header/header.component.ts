import { Component, Input, OnInit, HostListener } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/data/layout.service';
import { AuthService } from '../../../@core/services/auth.service';
import { Router } from '@angular/router';
import { NotificationComponent } from './notification/notification.component';
import { SharedGlobalService } from '../../../@core/services/shared.global.service';
import { ChatComponent } from './chat/chat.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateProfileComponent } from '../update-profile/update-profile.component';
import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})

export class HeaderComponent implements OnInit {


  
  @Input() position = 'normal';
  
  user: any;
  cache: any;
  token: any;
  socketInstance;

  public notifTab = NotificationComponent; 

  userMenu = [
    { 
      title: 'Profile',
      slug: 'profile'
    }, 
    { 
      title: 'Log out', 
      slug: 'logout'
    }
  ];

  constructor(
    public sidebarService: NbSidebarService,
    public auth: AuthService,
    public layoutService: LayoutService,
    public router: Router,
    public sgs: SharedGlobalService,
    private ngbModal: NgbModal
  ) {
    this.token = this.auth.getToken();
    
    sgs.getNotificationLength();
        
    this.loggedDefault();

    sgs.cacheEmitter.subscribe( () => {
      this.loggedDefault();
    });

    window.addEventListener('storage', (event) => {
      if (event.storageArea === localStorage) {
        this.loggedDefault();
        if(this.token !== this.auth.getToken()){
          if(this.auth.getToken() !== null)
            this.auth.login();
        }
      }
    }, false);

    this.socketInstance = sgs.ResponseSocket('notifications').subscribe( emitted => {
      if(emitted.success && emitted.data.readonly && emitted.data.uid === this.auth.getTokenData().id ){
      }else{
        if(emitted.data.for !== undefined && emitted.data.uid !== this.auth.getTokenData().id){
          if(emitted.data.for.filter(e => e.role === this.auth.getTokenData().role).length > 0){
            this.PlayNotification();
            sgs.notifLength++;
          }
        }
      }
    });
  }

  PlayNotification(){
    let notif = new Audio('/assets/mp3/notify.mp3');
        notif.loop = false;
        notif.play();
  }

  ngOnInit() {
    this.changeStatus(true);
  }

  loggedDefault(){
    this.user = this.auth.getTokenData();
    this.cache = this.sgs.getCachedData();
  }


  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {}

  ngOnDestory(){
    this.socketInstance.unsubscribe();
  }

  // turn offline status if browser or tabs is close
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.changeStatus(false)
    // event.returnValue = false;
  }

  logout(){
    this.changeStatus(false);
    this.auth.logout();
  }

  changeStatus(status){
    this.sgs.request('put', 'user/changeStatus', {status: status, id: this.auth.getTokenData().id}, async (res) => {});
  }


  openChat(){
    this.ngbModal
      const activeModal = this.ngbModal.open(ChatComponent, { size: 'lg', container: 'nb-layout', windowClass: 'min_height' });
  }

  updateUser(){
    const activeModal = this.ngbModal.open(UpdateProfileComponent, { size: 'lg', container: 'nb-layout', windowClass: 'min_height' });
    activeModal.componentInstance.uid = this.user.id;
  }

}