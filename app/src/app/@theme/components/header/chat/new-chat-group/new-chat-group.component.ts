import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../../../@core/services/auth.service';

@Component({
  selector: 'ngx-new-chat-group',
  templateUrl: './new-chat-group.component.html',
  styleUrls: ['./new-chat-group.component.scss']
})
export class NewChatGroupComponent implements OnInit {

  data;
  filterQuery = '';
  sortBy = 'lname';
  sortOrder = 'asc';
  selectQuery = 'lname';
  selectedCount = 0;
  selectedPersons = [];
  groupName;
  user;

  constructor(
    public sgs: SharedGlobalService,
    private activeModal: NgbActiveModal,
    public auth: AuthService,
  ) { }

  ngOnInit() {
    this.getChatableusers();
    this.user = this.auth.getTokenData();
  }

  getChatableusers(){
    this.sgs.request('get', 'user/getChatableUser', {}, async (res) => {
      if(res.success){
       res.data.map((a, i) => {
        res.data[i].selected = false;
       })
       this.data = res.data;
      }
    })
  }

  members = [];
  selectPerson(data){
    if(data.selected == false){
      this.selectedCount += 1;
      data.selected = true;
      this.selectedPersons.push(data);
      this.members.push({
        memberID: data.uid
      })
    }else{
      data.selected = false;
      this.removePerson(data);
    }

  }

  removePerson(data){
    const index: number = this.selectedPersons.indexOf(data);
    if (index !== -1) {
      this.selectedCount -= 1;
        this.selectedPersons.splice(index, 1);
        this.members.splice(index, 1);
        data.selected = false;
    }
  }

  create(){
    this.members.push({
      memberID: this.user.id
    })
    this.sgs.request('post', 'chat/createMessageGroupThread', {name: this.groupName, members: this.members}, async (res) => {
      if(res.success){
        this.sgs.Toaster('success', 'Success', 'New group has been created');
        this.closeModal();
      }
    });
  }

  closeModal(){
    this.activeModal.close();
   }
}

