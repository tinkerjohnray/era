import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../../@core/services/shared.global.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../../../@core/services/auth.service';

@Component({
  selector: 'ngx-group-settings',
  templateUrl: './group-settings.component.html',
  styleUrls: ['./group-settings.component.scss']
})
export class GroupSettingsComponent implements OnInit {

  data;
  filterQuery = '';
  sortBy = 'lname';
  sortOrder = 'asc';
  selectQuery = 'lname';
  selectedCount = 0;
  selectedPersons = [];
  groupName;
  user;
  tid; //threadID from chatComponent

  constructor(
    public sgs: SharedGlobalService,
    private activeModal: NgbActiveModal,
    public auth: AuthService,
  ) { }

  ngOnInit() {
    this.user = this.auth.getTokenData();
    this.getMessageThread();
  }

  getMessageThread(){
    this.sgs.request('get', 'chat/getGroupInfo', {id: this.tid}, async (res) => {
      if(res.success){
        this.groupName = res.data.convoName;
        this.getChatableusers(res.data);
      }
    })
  }

  getChatableusers(data){
    this.sgs.request('get', 'user/getAllChatableUser', {}, async (res) => {
      if(res.success){
       res.data.map((a, i) => {
        res.data[i].selected = false; 
        data.group.map((b, j) => {
          if(a.uid == b.memberID){
            this.selectPerson(res.data[i]);
          }
        })
       })
       this.data = res.data;
      }
    })
  }

  members = [];
  selectPerson(data){
    if(data.selected == false){
      this.selectedCount += 1;
      data.selected = true;
      this.selectedPersons.push(data);
      this.members.push({
        memberID: data.uid
      })
    }else{
      data.selected = false;
      this.removePerson(data);
    }
  }

  removePerson(data){
    const index: number = this.selectedPersons.indexOf(data);
    if (index !== -1) {
      this.selectedCount -= 1;
        this.selectedPersons.splice(index, 1);
        this.members.splice(index, 1);
        data.selected = false;
    }
  }

  save(){
    this.sgs.request('post', 'chat/updateGroupMembers', {convoName: this.groupName, group: this.members, tid: this.tid}, async (res) => {
      if(res.success){
        this.sgs.Toaster('success', 'Success', 'The group has been updated');
      }else{
        this.sgs.Toaster('danger', 'Warning', 'Error')
      }
    })
  }

  closeModal(){
    this.activeModal.close();
   }
}
