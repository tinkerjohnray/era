import { Component, OnInit } from '@angular/core';
import { SharedGlobalService } from '../../../../@core/services/shared.global.service';
import { AuthService } from '../../../../@core/services/auth.service';
import * as _ from 'lodash';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewChatGroupComponent } from './new-chat-group/new-chat-group.component';
import { GroupSettingsComponent } from './group-settings/group-settings.component';
import * as moment from 'moment';

@Component({
  selector: 'ngx-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  message;
  newDate = new Date();
  reciever;
  users = [];
  convoInfo;
  messages = [];
  messageID;
  user: any;;
  loading = true;
  socketInstance;
  socketInstanceGroup;
  socketInstanceUsers;
  openChatBox = false;
  groupThread: any;
  threadMembers: any;

  skip = 0;

  news = [];
  placeholders = [];
  pageSize = 10;
  pageToLoadNext = 1;
  loadinggg = true;
  TOTAL_PAGES = 1;

  data;
  filterQuery = '';
  sortBy = 'lname';
  sortOrder = 'asc';
  selectQuery = 'lname';

  constructor(
    public sgs: SharedGlobalService,
    public auth: AuthService,
    private activeModal: NgbActiveModal,
    private modalService: NgbModal
  ) {
    this.user = this.auth.getTokenData();
    this.socketInstance = sgs.ResponseSocket('chat').subscribe( emitted => {
      if(emitted.data.convoType == 1){
        // if user not equal to auth user and messageID == socket emitted ID, data will be pushed and displayed
        if(emitted.data.reciever != this.user.id && emitted.data.threadID != this.messageID){
        }else{
          if(emitted.data.threadID === this.messageID){
            emitted.data.sender = emitted.data.senderName;
            this.messages.push(emitted.data);
            setTimeout(a => {
              this.loadinggg = true;
              this.goDown();
            }, 10);
          }
          this.countOneThreadUnreadMessage(1, emitted.data.senderID);
          if(emitted.data.reciever === this.auth.getTokenData().id){
            this.PlayNotification();
          }else{
            this.PlaySendMessage();
          }
        }
      }else{
        if(this.threadMembers != undefined){
          this.threadMembers.map((a, i) => {         
            // push only once every specific user that the map found
            if(i === 0 && emitted.data.threadID === this.messageID){
              this.PlayNotification();
              emitted.data.sender = emitted.data.senderName;
              this.messages.push(emitted.data);
              setTimeout(a => {
                this.loadinggg = true;
                this.goDown();
              }, 10);
            }
          })
        }
        if(emitted.data.senderID != this.user.id){
          this.countOneThreadUnreadMessage(2, emitted.data.threadID);
        }
      }
    });
    this.socketInstanceGroup = sgs.ResponseSocket('newGroupCreated').subscribe( emitted => {
      this.users = [];
      this.getActiveUsers();
      this.getGroupThread();
    });
    this.socketInstanceUsers = sgs.ResponseSocket('userStatus').subscribe( emitted  => {
      if(this.users){
        this.users.map((a,i)=> {            
          if(a.uid == emitted.data.id){
            this.users[i].isOnline = emitted.data.isOnline;
          }
        })
      }
    })
  } 



  ngOnInit() {
    this.getActiveUsers();
    this.getGroupThread();

    for(var a = 15; a <= 18; a++){
      // console.log(moment().week(a).startOf('month').week(a).toString());
      // console.log("<br />");
      // console.log(moment().week(a).toString());

      console.log(moment().week(a).toString(), moment().week(a).day(-1).toString());

    }


  }

  ngOnDestroy(){
    this.socketInstance.unsubscribe();
    this.socketInstanceGroup.unsubscribe();
    this.socketInstanceUsers.unsubscribe();
  }  

  PlayNotification(){
    // let notif = new Audio('/assets/mp3/light.mp3');
    //     notif.loop = false;
    //     notif.play();
  }

  PlaySendMessage(){
    // let notif = new Audio('/assets/mp3/send.mp3');
    // notif.loop = false;
    // notif.play();
  }

  getActiveUsers(){
    var counter = 0;
    this.sgs.request('get', 'user/getChatableUser', null, async (res) => {
      if(res.success){
        res.data.map(a => {
          a.convoType = 1;
          this.sgs.request('get','chat/countUnreadMessages', {id: a.uid}, async (res) => {
            if(res.success){
              res.data.map(b => {
                var lastSeen = new Date(b.lastSeenMessageDate);
                var messageDate = new Date(b.messagesDate);
                 if(lastSeen.getTime() <= messageDate.getTime()){
                  counter += 1;
                 }
               })
            }
            a.counter = counter;
            counter = 0;
          });
          this.users.push(a);
        })
      }
    })
  }

  getGroupThread(){
    var counter = 0;
    this.sgs.request('get', 'chat/getGroupThread', {}, (res) => {
      if(res.success){
        res.data.map(a => {
          //count unread messages
          this.sgs.request('get', 'chat/countUnreadGroupMessages', {id: a._id, uid: a.uid}, async (res) => {
            if(res.success){
              res.data.map(a => {
               var lastSeen = new Date(a.lastSeenMessageDate);
               var messageDate = new Date(a.messagesDate);
                if(lastSeen.getTime() <= messageDate.getTime()){
                  counter += 1;
                }
              })
              a.counter = counter;
              // counter = 0;
            }
          })
          this.users.push(a);
        })
      }
    })
  }

  // if socket fired, the sender will emit a signal to call this function
  countOneThreadUnreadMessage(type, threadID){
    let i = _.find(this.users, (o) => {
      if(type == 1){
        return o.uid == threadID;
      }else{
        return o._id == threadID;
      }
    })
    if(type == 1 && i.uid != this.user.id){
      i.counter += 1;
    }else if(type == 2){
      i.counter += 1;
    }
  }

  getThreadMembers(threadID){
    this.sgs.request('get', 'chat/getThreadMembers', {threadID: threadID}, (res) => {
      if(res.success){  
        this.threadMembers = res.data;
      }
    })
  }

  convoType; // 1 = 1on1 | 2 = group
  openThread(type, user){
    user.counter = 0; // clear counter when thread is seen
    this.convoType = type;
    this.openChatBox = true;
    this.loading = true;
    this.loadinggg = true;
    if(type == 1){
      this.convoInfo = user;
      this.reciever = user;
      this.sgs.request('get', 'chat/checkThreadIfEsist', {id: user.uid}, async (res) => {
        if(res.success){
          this.messages = [];
          this.placeholders = [];
          this.loading = false;
          this.messageID = res.data._id;
          if(res.data.messages.length >= 2){ // >= 2 because the message code was added as index 0
            this.sgs.request('get', 'chat/getMessages', {id: user.uid, skip: this.skip}, async (response) => {
              if(response.success){
                this.placeholders = [];
                  this.messages = _.sortBy(response.data, o => o.date)
                  setTimeout(a => {
                    this.goDown();
                  }, 10);
              }
            })
          }
          this.sgs.request('post', 'chat/updateLastSeen', {id: res.data._id}, async (res) => {});
        }else{
          // initialized a new document of not exist inside chat collection
          this.messages = [
            {        
              // create a dummy set of message as first message 
              // to identify that this is the first message created from 
              // the thread and end the message loader | code: "FIREW@!!404" <- any messages with this code will be ignored
              sender: this.user.id,
              message: "F!R3W@!!404",
              date: new Date()
            }
          ];
          let group = [{
            memberID: this.reciever.uid,
          },{
            memberID: this.user.id
          }]
          this.sgs.request('post', 'chat/createMessageThread', {reciever: this.reciever.uid, data: this.messages, group: group}, async (res) => {
            this.loading = false;
            this.openMessage(this.convoInfo);
          });
        }
      });
    }else{
      this.convoInfo = user;
      this.messages = [];
      this.loading = false;
      this.sgs.request('get', 'chat/getGroupMessages', {id: user._id, skip: this.skip}, async (response) => {
        if(response.success){
          this.getThreadMembers(response.data[0]._id);
          this.messageID =  user._id; //threadID
          this.placeholders = [];
            this.messages = _.sortBy(response.data, o => o.date)
            setTimeout(a => {
              this.goDown();
            }, 10);
        }
      });
      //update seen and message counter
      this.sgs.request('post', 'chat/updateLastSeen', {id: this.convoInfo._id}, async (res) => {});
    }
  }

  openMessage(user){
    this.openChatBox = true;
    this.loading = true;
    this.loadinggg = true;
    this.convoInfo = user;
    this.reciever = user;
    this.sgs.request('get', 'chat/checkThreadIfEsist', {id: user.uid}, async (res) => {
      if(res.success){
        this.placeholders = [];
        this.messages = [];
        this.loading = false;
        this.messageID = res.data._id;
        if(res.data.messages.length >= 2){ // >= 2 because the message code was added as index 0
          this.sgs.request('get', 'chat/getMessages', {id: user.uid, skip: this.skip}, async (response) => {
            if(response.success){
              this.placeholders = [];
                this.messages = _.sortBy(response.data, o => o.date)
                setTimeout(a => {
                  this.goDown();
                }, 10);
            }
          })
        }
      }else{
        this.messages = [
          {        
            sender: this.user.id,
            message: "F!R3W@!!404",
            date: new Date()
          }
        ];
        this.sgs.request('post', 'chat/createMessageThread', {reciever: this.reciever.uid, data: this.messages}, async (res) => {
          this.loading = false;
          this.openMessage(this.convoInfo);
        });
      }
    });
  }


  senderID;
  onKeydown(event){
    if (event.key === "Enter") {
      let obj = {}
      if(this.isEmptyOrSpaces(this.message)){
       //if empty do nothing..
      }else{
        let message = this.message;
        this.senderID = this.user.id;
        if(this.convoType == 1){
          obj = {message: message, threadID: this.messageID, reciever: this.reciever.uid, convoType: this.convoType}
        }else{
          obj = {message: message, threadID: this.messageID, convoType: this.convoType}
        }
        this.sgs.request('post', 'chat/sendMessageUpdate', obj, async (res) => {
          setTimeout(() => {
            this.goDown();
          }, 10)
        });
        this.message = "";
      }
    }
  }  

  send(){
    if(this.isEmptyOrSpaces(this.message)){
      //if empty do nothing..
     }else{
      let obj = {}
      let message = this.message;
      this.senderID = this.user.id;
      if(this.convoType == 1){
        obj = {message: message, threadID: this.messageID, reciever: this.reciever.uid, convoType: this.convoType}
      }else{
        obj = {message: message, threadID: this.messageID, convoType: this.convoType}
      }
      this.sgs.request('post', 'chat/sendMessageUpdate', obj, async (res) => {
        setTimeout(() => {
          this.goDown();
        }, 10)
      });
      this.message = "";
     }
  }

  isEmptyOrSpaces(str){
    //check empty or whitespaces
    return str === null || str.match(/^ *$/) !== null;
  }

  nextSkip = 10;
  cnt = 0;
  loadNext() {
    if(this.loadinggg){
      if(this.convoInfo){
        if(this.convoType == 1){
          this.placeholders = new Array(this.pageSize);
          this.sgs.request('get', 'chat/getMessages', {id: this.convoInfo.uid, skip: this.nextSkip}, async (res) => {
            if(res.success){
              this.placeholders = [];
              this.loadinggg = true;
              this.nextSkip += 10;
              res.data.map((a, i) => {
                this.messages.splice(0, 0, a);
                // if message detect the message code 'F!R3W@!!404', it will stop loading messages  
                if(a.message == 'F!R3W@!!404'){
                  this.nextSkip = 10;
                  this.loadinggg = false;
                  this.placeholders = [];
                }
            }) 
          }
        });
      }else{
        this.placeholders = new Array(this.pageSize);
        this.sgs.request('get', 'chat/getGroupMessages', {id: this.convoInfo._id, skip: this.nextSkip}, async (res) => {
          if(res.success){
            this.placeholders = [];
            this.loadinggg = true;
            this.nextSkip += 10;
            res.data.map((a, i) => {
              this.messages.splice(0, 0, a);
              // if message detect the message code 'F!R3W@!!404', it will stop loading messages  
              if(a.message == 'F!R3W@!!404'){
                this.nextSkip = 10;
                this.loadinggg = false;
                this.placeholders = [];
              }
          }) 
          }
        });
      }
    }
   }
  }

 newGroup(){
  const activeModal = this.modalService.open(NewChatGroupComponent, { size: 'lg', container: 'nb-layout' });
 }

 groupSettings(data){
  const activeModal = this.modalService.open(GroupSettingsComponent, { size: 'lg', container: 'nb-layout' });
  activeModal.componentInstance.tid = data._id;
 }

 closeModal(){
  this.activeModal.close();
 }

 goDown(){
  document.querySelector("#chat").scrollTo(0,document.querySelector("#chat").scrollHeight);
 }

}
