# Official Documentation for TinkerTech SGS Starter
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]()
[![Build Status](https://img.shields.io/badge/analyze-passing-brightgreen)]()
[![Build Status](https://img.shields.io/badge/uptime-100%25-brightgreen)]()
[![Build Status](https://img.shields.io/badge/private%20documented%20frontend%20api%20density-18%25-red)]()
[![Build Status](https://img.shields.io/badge/private%20documented%20backend%20api%20density-0%25-red)]()
[![Build Status](https://img.shields.io/badge/vulnerabilities-0%25-brightgreen)]()
[![Build Status](https://img.shields.io/badge/Frontend%20Library%20Version-1.2-blue)]()
[![Build Status](https://img.shields.io/badge/Backend%20Framework%20Version-2.0-blue)]()


## Angular2+ ~ Frontend

### Global Modal

##### Modal Component
# 
```javascript
this.sgs.Modal(
  { id:id },
  { component: <COMPONENT>, size: 'full' }
);
```

**Best practice** `sgs.Modal(arg1, arg2)` must be inside of a function
```javascript
<FunctionName>(id){
    this.sgs.Modal(
      { id:id },
      { component: <COMPONENT>, size: 'full' }
    );
}  
```

##### Arguments / Parameters
* First Argument - `Object Data` *customizable*
    * `{ ... }`
* Second Argument - `Object Literals` *mostly defaults*
    * `component:` 
    * `size:`
        * `{ size: 'sm' }`
        * `{ size: 'md' }`
        * `{ size: 'lg' }`
        * `{ size: 'xl' }`
        * `{ size: 'full' }`
    * `backdrop:` (optional)
        * `{ backdrop: 'static' }` (default)
    * `container` (optional)
        * `{ container: 'nb-layout' }` (default)
        
##### Basic data
# 
```javascript
this.sgs.Modal(
  { id: 1 },
  { component: UpdateUserComponent, size: 'full' }
);
```

##### Complex and Mix data
# 
```javascript
this.sgs.Modal(
  {
    id: 1,
    name: 'Johnray Blance',
    contact: {
        mobile: [
            { smart: '0909 1234 567' },
            { sun: '0909 1234 568' },
            { globe: '0909 1234 569' },
        ]
    }
  },
  { component: UpdateUserComponent, size: 'full' }
);
```

##### How to trigger the modal?
# 
`COMPONENT.TS`

Calling inside the contructor
```javascript
constructor(){
    this.myModal(); //trigger myModal()    
}

myModal(){
    this.sgs.Modal(
      { id: 1 },
      { component: UpdateUserComponent, size: 'full' }
    );
}
```

`COMPONENT.HTML`

```html
    <a (click)="myModal()">Modal Button Link</a>
```

Or, you call the function using any angular events

`focus` `blur` `submit` `scroll` `cut` `copy` `paste` `keydown` `keypress` `keyup` `mouseenter` `mousedown` `mouseup` `click` `dblclick` `drag` `dragover` `drop`

##### Closing Modal

`.TS`
```javascript
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

constructor(private activeModal: NgbActiveModal){}
  
closeModal(){
    this.activeModal.close();
}
```
`.HTML`
```html
    <a (click)="closeModal()">Close</a>
```

#### Confirmation Modal

[![Confirmation Modal Image](http://serverkey.tinkertech.co/modal.confirmation.png)](http://serverkey.tinkertech.co/modal.confirmation.png)

```typescript
this.sgs.Modal({
    header: `Are you sure you want to delete?`,
    content: `Deleting notification #12345`,
    type: 'confirmation',
}, { size: 'sm' })

.confirm.subscribe( response => {
    if( response ){
        //if true; statement here
    }else{
        //if false; statement here
    }
});
```


#### Confirmation Modal with buttons option

[![Confirmation Modal Buttons Image](http://serverkey.tinkertech.co/modal.confirmation.buttons.png)](http://serverkey.tinkertech.co/modal.confirmation.buttons.png)

```typescript
this.sgs.Modal({
    header: `Are you sure to continue?`,
    content: `It looks like you've not completely set all the items in your purchase order. Are you sure to receive it partially? `,
    type: 'confirmation',
    buttons: {
    left: {
        class: 'btn-primary',
        label: 'No',
        value: false,
    },
    right: {
        class: 'btn-danger',
        label: 'Yes, Partial Inventory',
        value: true,
    },
    }
}, { size: 'md' })

.confirm.subscribe( response => {
    if( response ){
        //if true; statement here
    }else{
        //if false; statement here
    }
});
```



## Server Library/Framework  ~ Backend
